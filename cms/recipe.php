<?php 
  $page = 'recipe';
  include('parts/header.inc.php');
?>
  <div class="main">
    <div class="row-fluid">
      <div class="listbox span12">
        <div class="listboxheader clearfix">
          <form id="searchfrm" class="form-search fleft" action="javascript:void(0);">
            <input type="text" class="input-large search-query" placeholder="Filtrer les résultats ..." autocomplete="off">
          </form>
          <div id="status" class="btn-group offset1" data-toggle="buttons-radio">
            <button class="btn btn-inverse active" data-value="1">Actifs</button>
            <button class="btn btn-inverse" data-value="0">Inactifs</button>
            <button class="btn btn-inverse" data-value="">Tous</button>
          </div>
          <div class="addnew fright">
            <a class="btn btn-inverse"><i class="icon-plus icon-white"></i> Nouvelle recette</a>
          </div>
        </div>
        <div class="newentry"></div>
        <ul class="entryheader">
          <li class="clearfix">
            <div class="fleft">
              <span class="id"><span class="badge">id</span></span>
              <span class="title"><span class="badge">titre</span></span>
              <span class="keywords"><span class="badge">mots-clés</span></span>
              <span class="productlinks"><span class="badge">produits associés</span></span>
            </div>
            <div class="fright">
              <span class="statusbtn"></span>
              <span class="editbtn"></span>
            </div>
          </li>
        </ul>
        <ul class="itemslist boxcontent">
          <li class="templ productitem">
            <div class="handle" title="Réordonner [glissez/déposez]"></div>
            <div class="itemrow clearfix">
              <div class="fleft">
                <span class="id autofill" data-fieldname="rcp_id"></span>
                <span class="title autofill" data-fieldname="rcd_title_fr"></span>
                <span class="keywords autofill" data-fieldname="rcd_keywords_fr"></span>
                <span class="productlinks autofill" data-fieldname="rcp_product_links"></span>
              </div>
              <div class="fright">
                <span class="statusbtn" title="Activer / Désactiver [cliquez]"><i class="icon-star"></i></span>
                <span class="editbtn" title="Modifier / Voir le détail [cliquez]"><i class="icon-edit"></i></span>
              </div>
            </div>
            <div class="editentry"></div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="templ detailbox">
  <div class="editform">
    <div class="detailboxheader clearfix">
      <h4 class="itemTitle span9 fleft"></h4>
      <div class="span3 aright">
        <button class="btn btn-danger cancelBtn" type="button">Annuler</button>
        <button class="btn btn-success saveBtn" type="button">Sauvegarder</button>
      </div>
    </div>
    <div class="boxcontent">
      <form>
        <input type="hidden" name="rcp_id" value="0"/>
        <fieldset>
          <p class="lead">Information générale</p>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label><span class="label">PRODUITS LIÉS (SKU)</span></label>
              <input type="text" class="span12 typeaheadinput_multiple" name="rcp_product_links" maxlength="255" />
            </div>
            <div class="controls-group span3">
              <label title="Date d'ajout de la recette."><span class="label">DATE</span></label>
              <input type="text" readonly class="span12 datepicker" name="rcp_date" required pattern="\d{4}-\d{2}-\d{2}"/>
            </div>
            <div class="controls-group span3">
              <label><span class="label">AFFICHAGE</span></label>
              <select class="span12" name="rcp_active" id="active" required>
                <option value="1">Activé</option>
                <option value="0">Désactivé</option>
              </select>
            </div>
          </div>
          <p class="lead">Images</p>
          <div class="controls controls-row">
            <div class="controls-group span6 imgupcontainer" data-imgsize="small">
              <label><span class="label">IMAGE - PETIT FORMAT</span></label>
              <div class="imgupbutton">
                <div class="imgupmask btn btn-medium">Choisir un fichier</div>
                <input class="imgupinput" type="file" name="rcp_img_small" title="" />
              </div>
              <div class="imguppreviewbox">
                <a class="imgupremovebtn" href="javascript:void(0);" title="Supprimer l'image"><i class="icon-remove icon-white"></i></a>
                <div class="imguppreview" data-fieldname="rcp_img_small"></div>
                <div class="imgupmessage imgupdefaultmsg">
                  Aucune image associée.
                </div>
                <div class="imgupmessage imgupformatmsg">
                  Formats acceptés : JPG, GIF et PNG uniquement. Réessayez avec un fichier image adéquat.
                </div>
                <div class="imgupmessage imgupsizemsg">
                  Taille de fichier maximale acceptée : 1 Mo. Réessayez avec un fichier de plus petite taille.
                </div>
              </div>
            </div>
            <div class="controls-group span6 imgupcontainer" data-imgsize="large">
              <label><span class="label">IMAGE - GRAND FORMAT</span></label>
              <div class="imgupbutton">
                <div class="imgupmask btn btn-medium">Choisir un fichier</div>
                <input class="imgupinput" type="file" name="rcp_img_large" title=""/>
              </div>
              <div class="imguppreviewbox">
                <a class="imgupremovebtn" href="javascript:void(0);" title="Supprimer l'image"><i class="icon-remove icon-white"></i></a>
                <div class="imguppreview" data-fieldname="rcp_img_large"></div>
                <div class="imgupmessage imgupdefaultmsg">
                  Aucune image associée.
                </div>
                <div class="imgupmessage imgupformatmsg">
                  Formats acceptés : JPG, GIF et PNG uniquement. Réessayez avec un fichier image adéquat.
                </div>
                <div class="imgupmessage imgupsizemsg">
                  Taille de fichier maximale acceptée : 1 Mo. Réessayez avec un fichier de plus petite taille.
                </div>
              </div>
            </div>
          </div>
          <p class="lead">Textes</p>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label><span class="label label-info">TITRE DE LA RECETTE</span></label>
              <input title="Titre de la recette en français" type="text" name="rcd_title_fr" class="span12 use_as_frmtitle" required pattern=".{3,255}" maxlength="255" autocomplete="off" />
            </div>
            <div class="controls-group span6">
              <label><span class="label label-important">RECIPE TITLE</span></label>
              <input title="Titre de la recette en anglais" type="text" name="rcd_title_en" class="span12" required pattern=".{3,255}" maxlength="255" autocomplete="off" />
            </div>
          </div>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label><span class="label label-info">Mots-clés</span></label>
              <input title="Mots-clés français séparés par des virgules" autocomplete="off" type="text" name="rcd_keywords_fr" class="span12 typeaheadinput_multiple" placeholder="Ex. : Entrée, Crevettes, Grèce" />
            </div>
            <div class="controls-group span6">
              <label><span class="label label-important">Keywords</span></label>
              <input title="Mots-clés anglais séparés par des virgules" autocomplete="off" type="text" name="rcd_keywords_en" class="span12 typeaheadinput_multiple" placeholder="Ex.: Main dish, Chicken, Italian" />
            </div>
          </div>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label><span class="label label-info">URL RÉFÉRENCEMENT</span></label>
              <input type="text" name="rcd_slug_fr" class="span12" disabled="disabled" value="Générée automatiquement" />
            </div>
            <div class="controls-group span6">
              <label><span class="label label-important">SEO-FRIENDLY URL</span></label>
              <input type="text" name="rcd_slug_en" class="span12" disabled="disabled" value="Générée automatiquement" />
            </div>
          </div>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label title="Liste des ingrédients en français"><span class="label label-info">Ingrédients</span></label>
              <textarea title="Un ingrédient par ligne" rows="10" name="rcd_ingredients_fr" class="span12"></textarea>
            </div>
            <div class="controls-group span6">
              <label title="Liste des ingrédients en anglais"><span class="label label-info">Ingredients</span></label>
              <textarea title="Un ingrédient par ligne" rows="10" name="rcd_ingredients_en" class="span12"></textarea>
            </div>
          </div>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label title="Étapes de la préparation en français"><span class="label label-info">Préparation</span></label>
              <textarea title="Une étape par ligne." rows="10" name="rcd_preparation_fr" class="span12"></textarea>
            </div>
            <div class="controls-group span6">
              <label title="Étapes de la préparation en anglais"><span class="label label-info">Preparation</span></label>
              <textarea title="Une étape par ligne." rows="10" name="rcd_preparation_en" class="span12"></textarea>
            </div>
          </div>
        </fieldset>
      </form>
    </div>
    <div class="detailboxfooter clearfix">
      <div class="span12 aright">
        <button class="btn btn-danger cancelBtn" type="button">Annuler</button>
        <button class="btn btn-success saveBtn" type="button">Sauvegarder</button>
      </div>
    </div>
  </div>
</div>
<?php include('parts/footer.inc.php') ?>