<?php 
  $page = 'class';
  include('parts/header.inc.php');
?>
  <div class="main">
    <div class="row-fluid">
      <div class="listbox span12">
        <div class="listboxheader clearfix">
          <form id="searchfrm" class="form-search fleft" action="javascript:void(0);">
            <input type="text" class="input-large search-query" placeholder="Filtrer les résultats ..." autocomplete="off">
          </form>
          <div id="status" class="btn-group offset1" data-toggle="buttons-radio">
            <button class="btn btn-inverse active" data-value="1">Actifs</button>
            <button class="btn btn-inverse" data-value="0">Inactifs</button>
            <button class="btn btn-inverse" data-value="">Tous</button>
          </div>
          <div class="addnew fright">
            <a class="btn btn-inverse"><i class="icon-plus icon-white"></i> Nouveau cours/atelier</a>
          </div>
        </div>
        <div class="newentry"></div>
        <ul class="entryheader">
          <li class="clearfix">
            <div class="fleft">
              <span class="clsId"><span class="badge">id</span></span>
              <span class="clsTitle"><span class="badge">titre</span></span>
              <span class="clsInstructor"><span class="badge">instructeur</span></span>
              <span class="clsUpcoming"><span class="badge">seances à venir</span></span>
              <span class="clsPrice"><span class="badge">prix</span></span>
            </div>
            <div class="fright">
              <span class="statusbtn"></span>
              <span class="editbtn"></span>
            </div>
          </li>
        </ul>
        <ul class="itemslist boxcontent">
          <li class="templ productitem">
            <div class="handle" title="Réordonner [glissez/déposez]"></div>
            <div class="itemrow clearfix">
              <div class="fleft">
                <span class="clsId autofill" data-fieldname="cls_id"></span>
                <span class="clsTitle autofill" data-fieldname="cls_title"></span>
                <span class="clsInstructor autofill" data-fieldname="cls_instructor"></span>
                <span class="clsUpcoming autofill" data-fieldname="sch_upcoming"></span>
                <span class="clsPrice autofill" data-fieldname="cls_price"></span>
              </div>
              <div class="fright">
                <span class="statusbtn" title="Activer / Désactiver [cliquez]"><i class="icon-star"></i></span>
                <span class="editbtn" title="Modifier / Voir le détail [cliquez]"><i class="icon-edit"></i></span>
              </div>
            </div>
            <div class="editentry"></div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="templ detailbox">
  <div class="editform">
    <div class="detailboxheader clearfix">
      <h4 class="itemTitle span9 fleft"></h4>
      <div class="span3 aright">
        <button class="btn btn-danger cancelBtn" type="button">Annuler</button>
        <button class="btn btn-success saveBtn" type="button">Sauvegarder</button>
      </div>
    </div>
    <div class="boxcontent">
      <form>
        <input type="hidden" name="cls_id" value="0"/>
        <fieldset>
          <p class="lead">Information générale</p>
          <div class="controls controls-row">
            <div class="controls-group span3">
              <label><span class="label">LANGUE</span></label>
              <select name="cls_lang" class="span12" required>
                <option value="fr">Français</option>
                <option value="en">Anglais</option>
              </select>
            </div>
            <div class="controls-group span3">
              <label><span class="label">PRIX</span></label>
              <input type="text" class="span12" name="cls_price" required pattern="\d{1,4}((\.|,)\d{1,2})?" maxlength="7" />
            </div>
            <div class="controls-group span3">
              <label><span class="label">TAXABLE</span></label>
              <select class="span12" name="cls_taxable" required>
                <option value="1">Oui</option>
                <option value="0">Non</option>
              </select>
            </div>
            <div class="controls-group span3">
              <label><span class="label">AFFICHAGE</span></label>
              <select class="span12" name="cls_active" id="active" required>
                <option value="1">Activé</option>
                <option value="0">Désactivé</option>
              </select>
            </div>
          </div>
          <p class="lead">Textes et image</p>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <div>
                <label><span class="label">TITRE DU COURS</span></label>
                <input title="Titre du cours dans la langue du cours" type="text" name="cls_title" class="span12 use_as_frmtitle typeaheadinput" required pattern=".{3,255}" maxlength="255" autocomplete="off" />
              </div>
              <div>
                <label><span class="label">SOUS-TITRE À AFFICHER</span></label>
                <input title="Phrase à afficher sous le titre du cours" type="text" name="cls_subtitle" class="span12 typeaheadinput" required pattern=".{3,255}" maxlength="255" autocomplete="off" placeholder="Avec le chef Monsieur Untel" />
              </div>
              <div>
                <label><span class="label">DESCRIPTION</span></label>
                <textarea title="Description du cours dans la langue choisie." rows="8" name="cls_desc" class="span12"></textarea>
              </div>
            </div>
            <div class="controls-group span6">
              <div>
                <label><span class="label">INSTRUCTEUR</span></label>
                <input title="Nom complet de l'instructeur" type="text" name="cls_instructor" class="span12 typeaheadinput" required pattern=".{3,255}" maxlength="255" autocomplete="off" />
              </div>
              <div>
                <label><span class="label">ENTÊTE INSTRUCTEUR</span></label>
                <input title="Phrase de l'entête des cours de cet instructeur" type="text" name="cls_instructor_header" class="span12 typeaheadinput" required pattern=".{3,255}" maxlength="255" autocomplete="off" />
              </div>
            </div>
          </div>
          <div class="controls controls-row">
            <div class="controls-group span6 imgupcontainer">
              <div>
                <label><span class="label">IMAGE - COURS</span></label>
                <div class="imgupbutton">
                  <div class="imgupmask btn btn-medium">Choisir un fichier</div>
                  <input title="Cliquez pour choisir un fichier d'image sur votre poste." class="imgupinput" type="file" name="cls_img_course" />
                </div>
                <div class="imguppreviewbox">
                  <a class="imgupremovebtn" href="javascript:void(0);" title="Supprimer l'image"><i class="icon-remove icon-white"></i></a>
                  <div class="imguppreview" data-fieldname="cls_img_course"></div>
                  <div class="imgupmessage imgupdefaultmsg">
                    Ajouter une image associée à ce cours/atelier.
                  </div>
                  <div class="imgupmessage imgupformatmsg">
                    Formats acceptés : JPG, GIF et PNG uniquement. Réessayez avec un fichier image adéquat.
                  </div>
                  <div class="imgupmessage imgupsizemsg">
                    Taille de fichier maximale acceptée : 1 Mo. Réessayez avec un fichier de plus petite taille.
                  </div>
                </div>
              </div>  
            </div>
            <div class="controls-group span6 imgupcontainer">
              <div>
                <label><span class="label">IMAGE - INSTRUCTEUR</span></label>
                <div class="imgupbutton">
                  <div class="imgupmask btn btn-medium">Choisir un fichier</div>
                  <input title="Cliquez pour choisir un fichier d'image sur votre poste." class="imgupinput" type="file" name="cls_img_instructor" />
                </div>
                <div class="imguppreviewbox">
                  <a class="imgupremovebtn" href="javascript:void(0);" title="Supprimer l'image"><i class="icon-remove icon-white"></i></a>
                  <div class="imguppreview" data-fieldname="cls_img_instructor"></div>
                  <div class="imgupmessage imgupdefaultmsg">
                    Ajouter une image associée à ce  chef/instructeur.
                  </div>
                  <div class="imgupmessage imgupformatmsg">
                    Formats acceptés : JPG, GIF et PNG uniquement. Réessayez avec un fichier image adéquat.
                  </div>
                  <div class="imgupmessage imgupsizemsg">
                    Taille de fichier maximale acceptée : 1 Mo. Réessayez avec un fichier de plus petite taille.
                  </div>
                </div>
              </div>
            </div>
          </div>
          <p class="lead">Séances</p>
          <div class='classScheduleGroup'>
            <div class="controls controls-row classScheduleLabelRow">
              <div class="controls-group span3 largerSpan3">
                <label title="Lieu où aura lieu la séance de cours"><span class="label">LIEU</span><span class='starrequired'>*</span></label>
              </div>
              <div class="controls-group span2">
                <label title="Date de la séance"><span class="label">DATE</span><span class='starrequired'>*</span></label>
              </div>
              <div class="controls-group span1">
                <label title="Heure à laquelle débute le cours"><span class="label">HR</span><span class='starrequired'>*</span></label>
              </div>
              <div class="controls-group span1">
                <label title="Durée en minutes du cours"><span class="label">DUR.</span><span class='starrequired'>*</span></label>
              </div>
              <div class="controls-group span1">
                <label title="Nombre de personnes par groupe"><span class="label"># PL.</span><span class='starrequired'>*</span></label>
              </div>
              <div class="controls-group span1">
                <label title="Nombre de places encore disponibles"><span class="label">DISP.</span><span class='starrequired'>*</span></label>
              </div>
              <div class="controls-group span2">
                <label title="Contrôle l'affichage de cette séance sur le site Web"><span class="label">AFFICHAGE</span><span class='starrequired'>*</span></label>
              </div>
            </div>
          </div>
          <a class="btn btn-inverse addClassBtn" href="javascript:void(0);" title='Ajouter une séance de ce cours'><i class="icon-plus icon-white"></i> Ajouter une séance</a>
        </fieldset>
      </form>
    </div>
    <div class="detailboxfooter clearfix">
      <div class="span12 aright">
        <button class="btn btn-danger cancelBtn" type="button">Annuler</button>
        <button class="btn btn-success saveBtn" type="button">Sauvegarder</button>
      </div>
    </div>
  </div>
</div>
<div class="controls controls-row classScheduleRow templ">
  <div class="controls-group span3 largerSpan3">
    <input type="hidden" class='sch_id' name="sch_id[]" value="0" />
    <input title="Lieu où aura lieu la séance de cours" type="text" name="sch_location[]" class="span12 typeaheadinput" required pattern=".{3,255}" maxlength="255" autocomplete="off" />
  </div>
  <div class="controls-group span2">
    <input title="Date de la séance" type="text" readonly class="span12 datepicker_multiple" name="sch_date[]" required pattern="\d{4}-\d{2}-\d{2}"/>
  </div>
  <div class="controls-group span1">
    <input title="Heure à laquelle débute le cours" type="text" class="span12" name="sch_time[]" pattern="\d{2}:\d{2}" maxlength="5" placeholder='00:00' required />
  </div>
  <div class="controls-group span1">
    <input title="Durée en minutes du cours" type="text" class="span12" name="sch_duration[]" pattern="\d{1,3}" maxlength="3" placeholder='120' required />
  </div>
  <div class="controls-group span1">
    <input title="Nombre de personnes par groupe" type="text" class="span12" name="sch_groupsize[]" pattern="\d{1,2}" maxlength="2" required />
  </div>
  <div class="controls-group span1">
    <input title="Nombre de places encore disponibles" type="text" class="span12" name="sch_stock[]" pattern="\d{1,2}" maxlength="2" required />
  </div>
  <div class="controls-group span2">
    <select title="Contrôle l'affichage de cette séance sur le site Web" class="span12" name="sch_active[]" id="active" required>
      <option value="1">Activé</option>
      <option value="0">Désactivé</option>
    </select>
  </div>
  <div class="controls-group span1 smallerSpan1">
    <a class="fright removeClassBtn" href="javascript:void(0);" title='Supprimer cette séance'><i class="icon-remove"></i></a>
  </div>
</div>
<?php include('parts/footer.inc.php') ?>