if(!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g,'');
  };
}

(function() {
	var maxImageFileSizeInMB = 1;
	var acceptedFileUploadTypes = {
		'image/png': true,
		'image/jpeg': true,
		'image/gif': true
	};

	var addNewFormTitles = {
		'slideshow':"Nouvelle diapositive",
		'product':"Nouveau produit",
		'recipe':"Nouvelle recette",
		'class':"Nouveau cours"
	};

	var catalogueIdPrefixes = {
		'slideshow':"sli",
		'product':"prd",
		'recipe':"rcp",
		'class':"cls"
	};	

	var responsePayload = new Array();
	var itemsListSortOrder = new Array();
	var requestFilters = new Object();
	var availableProductTypes = new Object();
	var typeAheadSources = new Object();

	$(function() {
		$(document).ajaxStart(loading);
		$(document).ajaxComplete(finished);
		mainCmsFunctions();
		if($('body').data('page')=='index') {
			login();
			$(".loginfrm .oocms_cnx").on("click",login);
		}
		$(".logout").on("click",logout);
	})

	function mainCmsFunctions() {
		setRequestFilters();
		loadTypeList();
		getInitialSortOrder().then(loadItemsList);
		
		// Status filter
		$("#status button").on("click",function() {
								requestFilters.f_status = $(this).data("value");
								loadItemsList();
							});

		// Type filter
		$("#type").on("click","li",function() {
								requestFilters.f_type = $(this).data("id");
								$("#type li").removeClass("active");
								$(this).addClass("active");
								$("a span",$(this).closest(".dropdown"))
														.text($(this).text());
								loadItemsList();
							});
		
		// Keywords filter 
		$("#searchfrm input").on("keyup", refreshSearch).on("keydown",function(){
									clearTimeout(window.instantSearchTimoutId);
								});

		// Category filter 
		$("#category li").on("click",changeCategory);

		$(".addnew .btn").on("click",showNewEntryForm);
		$(".listbox").on("click",".editbtn",showEditForm);
		$(".listbox").on("click",".statusbtn",toggleStatus);
		$(".listbox").on("click",".cancelBtn",resetAndCloseForm);
		$(".listbox").on("click",".saveBtn",sendEntryForm);
		// Only for Class section
		$(".listbox").on("click",".addClassBtn",addClassFormRow);
		$(".listbox").on("click",".removeClassBtn",removeClassSchedule);
		
		$(".listbox").on("change",".imgupinput", handleImageUpload);
		$(".listbox").on("click",".imgupremovebtn",deleteUploadedImage);
		$(".itemslist").sortable({handle: '.handle'});
		$(".itemslist").on("sortupdate", updateSortOrder);
	}

	function resetPageState() {
		$("#status button").removeClass("active");
		$("#status button").eq(0).addClass("active");
		$("#type li").removeClass("active");
		$("#type li").eq(0).addClass("active");
		$("#type_label").text($("#type li").eq(0).text());
		$("#searchfrm input").val('');
		requestFilters = {};
		setRequestFilters();
		loadTypeList();
		getInitialSortOrder().then(loadItemsList);
	}

	function changeCategory() {
		$("#category li").removeClass("active");
		$(this).addClass("active");
		resetPageState();
	}

	function setRequestFilters() {
		requestFilters.f_lang = $("html").attr("lang");
		requestFilters.f_catalogue = $("#catalogue li.active").data("name");
		if($('#category').size()>0) {
			requestFilters.f_category = $("#category li.active").data("id");
		}
		if($('#type').size()>0) {
			requestFilters.f_type = $("#type li.active").data("id");
		}
		requestFilters.f_status = $("#status button.active").data("value");
		if($('#searchfrm').size()>0) {
			requestFilters.f_kw = $("#searchfrm input").val();
		}
	}

	function typeaheadExtractor(query) {
		var result = /([^,]+)$/.exec(query);
		if(result && result[1])
			return result[1].trim();
		return '';
	}

	function getTypeAheadSources($frm) {
		if(!$frm.data('typeahead')) {
			$.ajax("appcode/router.php?url="+requestFilters.f_catalogue+"/get_typeahead",
					{data: requestFilters, context:$frm, type: 'POST'}).done(setTypeAheadSources);
		}
	}

	function setTypeAheadSources(data,status,xhr,formElt) {
		typeAheadSources = data;
		var $form = ($(this).prop("tagName")=='FORM')?$(this):formElt;
		$(".typeaheadinput",$form).each(function() {
			$(this).typeahead({source:typeAheadSources[$(this).attr('name').replace(/\[\]/g,'')]});
			console.log($(this).attr('name').replace(/\[\]/g,''));
			console.log(typeAheadSources[$(this).attr('name').replace(/\[\]/g,'')]);
		});
		$(".typeaheadinput_multiple",$form).each(function() {
			$(this).typeahead({
				source:typeAheadSources[$(this).attr('name')],
				showAll:true,
				updater: function(item) {
							if(item.search(/\d ? \| ?/)) {
								item = item.replace(/\D*([\w-]+) ?\|.*/i,'$1');
								console.log(item);
							}
					        return this.$element.val().replace(/[^,]*$/,'')+item+',';
					    },
				matcher: function (item) {
							var tquery = typeaheadExtractor(this.query);
							if(!tquery) return false;
							return ~item.toLowerCase().indexOf(tquery.toLowerCase());
					    },
				highlighter: function (item) {
						    var query = typeaheadExtractor(this.query).
						    			replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&');
						    return item.replace(new RegExp('(' + query + ')', 'ig'), 
						    	function ($1, match) {
						        	return '<strong>' + match + '</strong>';
						    	});
						}
			});
		});
	}

	function appendRequestFilters(formData) {
		for(var filterName in requestFilters) {
			formData.append(filterName,requestFilters[filterName]);	
		}
		return formData;
	}

	function refreshSearch(event) {
		// Ignore space
		//if(event.which != 32) {
		//if($.trim($(this).val())!="") {
			$inputVal = $(this).val();
			// Time requests to a max of 3 per second.
			window.instantSearchTimoutId = 
					setTimeout(function() {
						requestFilters.f_kw = $inputVal;
						loadItemsList();
					},333);
		//}
	}

	function showNewEntryForm(obj) {
		//if($(".newentry .detailbox").size()==0) {
			$(".newentry").empty();
			$(".newentry").html($(".templ.detailbox").clone()
								.removeClass("templ").addClass("newitemform"));
			setDatePicker($(".newentry .datepicker"));
		//}
		$(".newentry .itemTitle")
			.text(addNewFormTitles[$("#catalogue li.active").data("name")] 
				+ (($("#category li.active").size()>0)?
							" - " + $("#category li.active").text():""));
		$(".newentry").slideDown();
		$(".newentry").addClass("oomodal");
		$(".newentry .datepicker").datepicker("setDate", new Date());
		markRequiredFields($(".newentry form"));
		// Call it again and again (instead of once above) to refresh strings.
		getTypeAheadSources($(".newentry form"));
		$("select[name='prd_cat_id_fk']").val(requestFilters["f_category"]);
	}

	function showEditForm(obj) {
		var $li = $(this).closest("li");
		$(".itemrow",$li).hide();
		$li.addClass("editing");
		if($(".detailbox",$li).size()==0) {
			$(".editentry",$li).append($(".templ.detailbox").clone()
							.removeClass("templ").addClass("newitemform"));
			setDatePicker($(".newitemform .datepicker",$li));
		}
		loadEditFormData($li);
		$(".editentry .itemTitle",$li).
				text($(".editentry form input.use_as_frmtitle",$li).val());
		$(".editentry",$li).slideDown();
		$(".editentry",$li).addClass("oomodal");
		markRequiredFields($(".editentry form",$li));
		// Call it again and again (instead of once above) to refresh strings.
		getTypeAheadSources($(".editentry form",$li));
		$("select[name='prd_cat_id_fk']").val(requestFilters["f_category"]);
	}

	function markRequiredFields($frm) {
		$("input[required],select[required]",$frm).each(function() {
			var $label = $(this).prevAll("label");
			if($(".starrequired",$label).size()==0) {
				$(this).prevAll("label").append("<span class='starrequired'>*</span>");
			}
			else {
				return;
			}
		});
	}

	function loadEditFormData(item) {
		var data = responsePayload[item.data("pos")];
		var frm = $("form",item);
		for(var p in data) {
			if(frm.get(0).elements[p]) {
				if(frm.get(0).elements[p].type != "file") {
					frm.get(0).elements[p].value = data[p].replace(/<br>/gi,'\n');
				}
			}
			else if(p=="schedule" && $.isArray(data[p]) && data[p].length>0) {
				loadScheduleData(data[p],frm);
			}
		} 

		// Set FormData
		if(!frm.data('formData')) {
			frm.data('formData',new Object());
		}
		var formData = frm.data("formData");

		// Display images.
		$("div.imguppreview",item).each(function() {
			$(this).show(); // Make sure the elt display is set.
			var fieldName = $(this).data("fieldname");
			if(data[fieldName] != "") {
				formData[fieldName+"_update"] = data[fieldName];
				$(this).html("<img src='../data/images/" + requestFilters.f_catalogue
											+ "/" + data[fieldName]+"?rnd="+Math.random()+"'/>");
				$(".imgupmessage",$(this).closest(".imgupcontainer")).hide();
				$(".imgupremovebtn",$(this).closest(".imgupcontainer")).show();
			}
		});
	}

	function resetAndCloseForm() {
		$("form",$(this).closest(".detailbox")).get(0).reset();
		$(".imgupremovebtn",$(this).closest(".detailbox")).click();
		if($(this).closest(".editentry").size()>0) {
			$(this).closest(".editentry").hide();
			$(this).closest(".editentry").prev(".itemrow").show();
			$(this).closest("li").removeClass("editing");
		}
		else {
			$(".newentry").hide();
		}
		//$(".itemslist").sortable("enable");
	}

	function setDatePicker($el) {
		$.datepicker.regional['fr-CA'] = {
			closeText: 'Fermer',
			prevText: 'Précédent',
			nextText: 'Suivant',
			currentText: 'Aujourd\'hui',
			monthNames: ['janvier', 'février', 'mars', 'avril', 'mai', 'juin',
				'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'],
			monthNamesShort: ['janv.', 'févr.', 'mars', 'avril', 'mai', 'juin',
				'juil.', 'août', 'sept.', 'oct.', 'nov.', 'déc.'],
			dayNames: ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'],
			dayNamesShort: ['dim.', 'lun.', 'mar.', 'mer.', 'jeu.', 'ven.', 'sam.'],
			dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
			weekHeader: 'Sem.',
			dateFormat: 'yy-mm-dd',
			firstDay: 0,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''
		};
		$.datepicker.setDefaults($.datepicker.regional['fr-CA']);
		$el.datepicker({dateFormat: "yy-mm-dd"});
	}

	function handleImageUpload() {
		if (this.files && this.files[0]) {
			readUploadedImage(this);
        }
	}

	function previewUploadedImage(imgFile,imgupContainer) {
		var reader = new FileReader();
		reader.onload = function (event) {
			var image = new Image();
			image.src = event.target.result;
			$(".imguppreview",imgupContainer).html(image);
		};
		reader.readAsDataURL(imgFile);
	}

	function readUploadedImage(input) {
		var imgupContainer = $(input).closest(".imgupcontainer");
		var frm = imgupContainer.closest('form');
		if(!frm.data('formData')) {
			frm.data('formData',new Object());
		}
		var formData = frm.data("formData");
		for (var i = 0; i < input.files.length; i++) {
			if (acceptedFileUploadTypes[input.files[i].type] === true) {
				if(input.files[i].size/1024/1024 < maxImageFileSizeInMB) {
					formData[input.name] = input.files[i];
					$(".imgupmessage",imgupContainer).hide();
					$(".imguppreview",imgupContainer).show();
					$(".imgupremovebtn",imgupContainer).show();
					previewUploadedImage(input.files[i],imgupContainer);
				}
				else {
					messageUploadedImage(imgupContainer,formData,".imgupsizemsg");
				}
			}
			else {
				messageUploadedImage(imgupContainer,formData,".imgupformatmsg");
			}
		}
	}

	function messageUploadedImage(imgupContainer,formData,msgElt) {
		$("img",imgupContainer).remove();
		delete formData[$("input[type='file']",imgupContainer).get(0).name];
		formData[$("input[type='file']",imgupContainer).get(0).name+"_update"] = "";
		$(".imgupremovebtn",imgupContainer).hide();
		$(".imguppreview",imgupContainer).hide();
		$(".imgupmessage",imgupContainer).hide();
		$(msgElt,imgupContainer).show();
	}

	function deleteUploadedImage(evt) {
		evt.preventDefault();
		var imgupContainer = $(this).closest(".imgupcontainer");
		/*
		if(imgupContainer.closest(".newentry").size()>0) {
			$('img',imgupContainer).remove();
		}
		*/
		var formData = imgupContainer.closest('form').data("formData");
		if(formData) {
			messageUploadedImage(imgupContainer,formData,".imgupdefaultmsg");
		}
	}
	
	function sendEntryForm() {
		var frm = $("form",$(this).closest(".detailbox"));
		if(frm.get(0).checkValidity()) {
			frm.removeClass("frmnotvalid");
			var formData = new FormData(frm.get(0));

			// Append Request Filters.
			formData = appendRequestFilters(formData);
			
			/*
			// Append image files to the form data
			for(var obj in frm.data("formData")) {
				formData.append(obj,frm.data("formData")[obj]);
			}
			*/

			$.ajax('appcode/router.php?url='+requestFilters.f_catalogue+'/add',{processData: false,
						contentType: false,data: formData, type: 'POST', 
						context:$(this).closest('.detailbox')}).done(addHandler);
		}
		else {
			frm.addClass("frmnotvalid");
		}
	}

	function addHandler(data) {
		// If this is a new record, add it to the top of the sortOrder table
		if($(this).closest(".newentry").size()>0) {
			itemsListSortOrder.unshift(data);
			saveSortOrder();
		}
		// Empty formData
		$.removeData($("form",$(this))[0],'formData');
		$(".cancelBtn",$(this)).click();
		loadItemsList();
	}

	function loadItemsList() {
		$.ajax("appcode/router.php?url="+requestFilters.f_catalogue+"/get",{data: requestFilters, 
										type: 'POST'}).done(loadListHandler);
	}

	function loadListHandler(data) {
		if(data && Object.prototype.toString.call(data) === '[object Array]') {
			responsePayload = cleanNull(data);
			displayItemList();
		}
	}

	function displayItemList() {
		var $itemListElt = $(".itemslist");
		$("li",$itemListElt).not(".templ").remove();
		if(responsePayload.length==0) {
			$itemListElt.append("<li class='msgemptyitems'>Aucun élément.</li>");
		}
		var isEmptySortOrder = (itemsListSortOrder.length==0)?true:false;
		for(var i=0; i<responsePayload.length; i++) {
			if(isEmptySortOrder) {
				itemsListSortOrder[i] = responsePayload[i][catalogueIdPrefixes[requestFilters.f_catalogue]+'_id'];
			}
			$itemListElt.append(fillOneItem($("li.templ",$itemListElt).clone()
									.removeClass("templ").attr("data-pos",i),
														responsePayload[i]));
		}
	}

	function fillOneItem(elt,data) {
		elt.data("id",data[catalogueIdPrefixes[requestFilters.f_catalogue]+'_id']);
		var autoFillElts = $('.autofill',elt);
		
		autoFillElts.each(function(i) {
			if($(this).data('fieldname')=="prd_typ_id_fk") {
				$(this).text(availableProductTypes[data[$(this).data('fieldname')]]);
			}
			else {
				$(this).text(data[$(this).data('fieldname')]);
			}
			if($(this).hasClass('linkify') && $(this).text()!="") {
				$(this).html("<a class='brand' title='[Cliquez pour tester l&apos;adresse du lien]\n"+$(this).text()+"' href='"+$(this).text()+"' target='_blank'>Lien</a>");
			}
		});
		if(data[catalogueIdPrefixes[requestFilters.f_catalogue]+'_active']=="0") {
			$('.itemrow',elt).addClass("inactive");
			$(".statusbtn .icon-star",elt).removeClass("icon-star")
												.addClass("icon-star-empty");
		}
		return elt;
	}

	function toggleStatus() {
		var item_id = catalogueIdPrefixes[requestFilters.f_catalogue] + "_id";
		$.ajax("appcode/router.php?url="+requestFilters.f_catalogue+"/toggle_status",{data: 
						{item_id:$(this).closest(".productitem").data("id")}, 
										type: 'POST'}).done(loadItemsList);
	}

	/*
		Set null values to empty string in resultset array returned from DB.
	*/
	function cleanNull(json) {
		for(var i=0; i<json.length; i++) {
			for(var p in json[i]) {
				if(json[i][p]===null) {
					json[i][p] = "";
				}
			}
		}
		return json;
	}

	function loadTypeList(){
		if($("#type").size()>0) {
			$.ajax("appcode/router.php?url=type/get",{data: requestFilters, 
										type: 'POST'}).done(handleTypeList);
		}
	}

	function handleTypeList(data) {
		availableProductTypes = new Object();
		$("#type").html('<li data-id="0" class="active"><a>Tous</a></li>');
		$(".type_select").html('<option value="">Choisir un type</option>');
		$(data).each(function($k,$v) {
			availableProductTypes[$v['typ_id']] = $v['typ_name_fr'];
			$("#type").append('<li data-id="'+$v['typ_id']+'"><a>'
												+$v['typ_name_fr']+'</a></li>');
			$(".type_select").append('<option value="'+$v['typ_id']+'">'
												+$v['typ_name_fr']+'</option>');
		});
	}

	function itemSort(arr, from, to) {
		arr.splice(to, 0, arr.splice(from, 1)[0]);
	};

	function getInitialSortOrder() {
		itemsListSortOrder = [];
		return $.ajax("appcode/router.php?url=sort/get",{data: requestFilters,
			type: 'POST'}).done(function(data) {
				if(data && data[0] && data[0]["srt_idseries"] != "") {
					itemsListSortOrder = data[0]["srt_idseries"].split(",");
					requestFilters["srt_idseries"] = itemsListSortOrder.join();
				}
			});
	}

	function saveSortOrder() {
		requestFilters["srt_idseries"] = itemsListSortOrder.join();				
		$.ajax("appcode/router.php?url=sort/save",{data: requestFilters,
														type: 'POST'});
	}

	function updateSortOrder(event, ui) {
		var startPos = $.inArray(ui.item.data("id"),itemsListSortOrder);
		var endPos = $.inArray(ui.item.nextAll().data("id"),itemsListSortOrder);
		if(endPos>startPos) {
			endPos--;
		}
		else if(endPos==-1) {
			endPos = itemsListSortOrder.length - 1;
		}
		
		itemSort(itemsListSortOrder,startPos,endPos);
		saveSortOrder();
	}

	/* 
	 * User Management
	 * Handles user login/logout
	 */
	function login() {
		$.ajax({url:"appcode/router.php?url=user/login",type:"POST",
		        data:{uname:$("input[name='oocms_uname']").val(),
		        pwd:$("input[name='oocms_pwd']").val()},success:h_login});
	}
	
	function h_login(data) {
		if(data && data!=="false" && data!=-1) {
			window.location='slideshow.php';
		}
		else {
			$(".loginfrm").slideDown(100);
			$(".logout,.alert").hide();
			if(data==-1 && window.location.search=="?nl") {
				$(".loginfrm .alert-warning").show();
				closeAlert($(".loginfrm .alert-warning"));
			}
			if(data==false) {
				$(".loginfrm .alert-error").show();
				$(".loginfrm input[autofocus]").select().focus();
				$(".loginfrm input[type='password']").val('');
				closeAlert($(".loginfrm .alert-error"));
			}
			if(window.location.search=="?lo") {
				$(".loginfrm .alert-warning").hide();
				$(".loginfrm .alert-success").show();
				closeAlert($(".loginfrm .alert-success"));
			}
		}
	}

	function closeAlert($alertObj,delay) {
		delay = (delay)?delay:3000;
		window.setTimeout(function() {
			$alertObj.slideUp();
		},delay);
	}
	
	function logout() {
		$.ajax({url:"appcode/router.php?url=user/logout",
		        type:"GET",success:h_logout});
	}
	
	function h_logout(data,status,xhr) {
		if(data) {
			$(".loginfrm").slideDown(100);
			$(".loginform input").eq(0).focus();
			$(".logout,.alert").hide();
			$(".loginfrm .alert-success").show();
			window.location='index.php?lo';
		}
	}

	function loading() {
		$(".ajaxloader").show();
	}

	function finished(event,request,settings) {
		$(".ajaxloader").hide();
	}

	function addClassFormRow(e,frm) {
		var $form = (frm)?frm:$(this).closest("form");
		$(".classScheduleLabelRow",$form).show();
		$(".classScheduleGroup .classScheduleRow",$form).removeClass('newClassRow');
		var $newRow = $(".templ.classScheduleRow").clone()
								.removeClass("templ").addClass('newClassRow');
		$(".classScheduleGroup",$form).append($newRow);
		setDatePicker($(".datepicker_multiple",".newClassRow"));
		$(".datepicker_multiple",".newClassRow").datepicker("setDate", new Date());
		markRequiredFields(".newClassRow");
		setTypeAheadSources(typeAheadSources,null,null,$('.newClassRow').closest('form'));
		return $newRow;
	}

	function removeClassFormRow(data,status,xhr,context) {
		var $context = (context)?context:$(this);
		console.log($context);
		$context.closest(".classScheduleRow").remove();
		if($(".classScheduleGroup .classScheduleRow",".newitemform").size()<1) {
			$(".classScheduleLabelRow").hide();
		}
		//console.log($(".classScheduleGroup .classScheduleRow",".newitemform").size());
	}

	function removeClassSchedule() {
		var $schRow = $(this).closest(".classScheduleRow");
		var $schId = $(".sch_id",$schRow).val();
		console.log($schId);
		if($schId>0) {
			$.ajax({url:"appcode/router.php?url=class/remove_schedule",
		        type:"POST",data:{'sch_id':$schId},context:$(this),success:removeClassFormRow});
		}
		else {
			removeClassFormRow(null, null, null, $(this));
		}
	}

	function loadScheduleData(data,frm) {
		$(".classScheduleGroup .classScheduleRow",frm).remove();
		var rowData,eltName;
		for(var i=0; i<data.length; i++) {
			rowData = data[i];
			$row = addClassFormRow(null,frm);
			console.log($row.html());
			for(p in rowData) {
				eltName = p+'[]';
				$('input[name="'+eltName+'"]',$row).val(rowData[p]);
				$('select[name="'+eltName+'"]',$row).val(rowData[p]);
			}
		}
	}
	
})();