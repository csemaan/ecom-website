<?php 
  $page = 'index';
  include('parts/header.inc.php');
?>
  <section class="loginfrm">
    <div class="span4 offset4">
      <div class="well">
        <legend>Gestionnaire de contenu</legend>
        <form action="javascript:void(0);">
          <div class="alert alert-error">
              <a class="close" data-dismiss="alert" href="#">x</a>Hummm, j'vous connais pas.
          </div>
          <div class="alert alert-success">
              <a class="close" data-dismiss="alert" href="#">x</a>Ciao !
          </div>
          <div class="alert alert-warning">
              <a class="close" data-dismiss="alert" href="#">x</a>Identifiez-vous ...
          </div>
          <label><span class="label">NOM D'UTILISATEUR</span></label>
          <input type="text" name="oocms_uname" maxlength="50" autofocus="autofocus" />
          <label><span class="label">MOT DE PASSE</span></label>
          <input type="password" name="oocms_pwd" maxlength="50" autocomplete="off" />
          <button class="btn-success btn oocms_cnx">Connexion</button>      
        </form>    
      </div>
    </div>
  </section>
</div>
<?php include('parts/footer.inc.php') ?>