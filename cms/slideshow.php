<?php 
  $page = 'slideshow';
  include('parts/header.inc.php');
?>
  <div class="main">
    <div class="row-fluid">
      <div class="listbox span12">
        <div class="listboxheader clearfix">
          <form id="searchfrm" class="form-search fleft" action="javascript:void(0);">
            <input type="text" class="input-large search-query" placeholder="Filtrer les résultats ..." autocomplete="off">
          </form>
          <div id="status" class="btn-group offset1" data-toggle="buttons-radio">
            <button class="btn btn-inverse active" data-value="1">Actifs</button>
            <button class="btn btn-inverse" data-value="0">Inactifs</button>
            <button class="btn btn-inverse" data-value="">Tous</button>
          </div>
          <div class="addnew fright">
            <a class="btn btn-inverse"><i class="icon-plus icon-white"></i> Nouvelle diapositive</a>
          </div>
        </div>
        <div class="newentry"></div>
        <ul class="entryheader">
          <li class="clearfix">
            <div class="fleft">
              <span class="sliId"><span class="badge">id</span></span>
              <span class="sliTitle"><span class="badge">titre</span></span>
              <span class="sliDesc"><span class="badge">description</span></span>
              <span class="sliLink"><span class="badge">url</span></span>
            </div>
            <div class="fright">
              <span class="statusbtn"></span>
              <span class="editbtn"></span>
            </div>
          </li>
        </ul>
        <ul class="itemslist boxcontent">
          <li class="templ productitem">
            <div class="handle" title="Réordonner [glissez/déposez]"></div>
            <div class="itemrow clearfix">
              <div class="fleft">
                <span class="sliId autofill" data-fieldname="sli_id"></span>
                <span class="sliTitle autofill" data-fieldname="sld_title_fr"></span>
                <span class="sliDesc autofill" data-fieldname="sld_desc_fr"></span>
                <span class="sliLink autofill linkify" data-fieldname="sld_link_fr"></span>
              </div>
              <div class="fright">
                <span class="statusbtn" title="Activer / Désactiver [cliquez]"><i class="icon-star"></i></span>
                <span class="editbtn" title="Modifier / Voir le détail [cliquez]"><i class="icon-edit"></i></span>
              </div>
            </div>
            <div class="editentry"></div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="templ detailbox">
  <div class="editform">
    <div class="detailboxheader clearfix">
      <h4 class="itemTitle span9 fleft"></h4>
      <div class="span3 aright">
        <button class="btn btn-danger cancelBtn" type="button">Annuler</button>
        <button class="btn btn-success saveBtn" type="button">Sauvegarder</button>
      </div>
    </div>
    <div class="boxcontent">
      <form data-typeahead="false">
        <input type="hidden" name="sli_id" value="0"/>
        <input type="hidden" name="sli_active" value="1"/>
        <fieldset>
          <p class="lead">Image</p>
          <div class="controls controls-row">
            <div class="controls-group span12 imgupcontainer">
              <label><span class="label">IMAGE</span><span class='starrequired'>*</span></label>
              <div class="imgupbutton">
                <div class="imgupmask btn btn-medium">Choisir un fichier</div>
                <input title="Cliquez pour choisir un fichier." class="imgupinput" type="file" name="sli_img" />
              </div>
              <div class="imguppreviewbox imguplarge">
                <a class="imgupremovebtn" href="javascript:void(0);" title="Supprimer l'image"><i class="icon-remove icon-white"></i></a>
                <div class="imguppreview" data-fieldname="sli_img"></div>
                <div class="imgupmessage imgupdefaultmsg">
                  Ajoutez une image pour cette dispositive.<br/>L'image doit avoir les dimensions exactes suivantes : 1380px par 500px.
                </div>
                <div class="imgupmessage imgupformatmsg">
                  Formats acceptés : JPG, GIF et PNG uniquement. Réessayez avec un fichier image adéquat.
                </div>
                <div class="imgupmessage imgupsizemsg">
                  Taille de fichier maximale acceptée : 200 Ko. Réessayez avec un fichier de plus petite taille.
                </div>
              </div>
            </div>
          </div>
          <p class="lead">Textes</p>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label><span class="label label-info">TITRE DE LA DIAPOSITIVE</span></label>
              <input title="Titre de la diapositive en français" type="text" name="sld_title_fr" class="span12 use_as_frmtitle" maxlength="255" autocomplete="off" />
            </div>
            <div class="controls-group span6">
              <label><span class="label label-important">SLIDE TITLE</span></label>
              <input title="Titre de la diapositive en anglais" type="text" name="sld_title_en" class="span12" maxlength="255" autocomplete="off" />
            </div>
          </div>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label><span class="label label-info">AMORCE</span></label>
              <input title="Court texte qui sera placé sous le titre - en français" autocomplete="off" type="text" name="sld_desc_fr" class="span12" />
            </div>
            <div class="controls-group span6">
              <label><span class="label label-important">LEAD</span></label>
              <input title="Court texte qui sera placé sous le titre - en anglais" autocomplete="off" type="text" name="sld_desc_en" class="span12" />
            </div>
          </div>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label><span class="label label-info">LIEN (URL COMPLÈTE)</span></label>
              <input title="Adresse du lien français associé s'il y a lieu" type="url" name="sld_link_fr" class="span12" />
            </div>
            <div class="controls-group span6">
              <label><span class="label label-important">LINK (COMPLETE URL)</span></label>
              <input title="Adresse du lien anglais associé s'il y a lieu" type="url" name="sld_link_en" class="span12" />
            </div>
          </div>
        </fieldset>
      </form>
    </div>
    <div class="detailboxfooter clearfix">
      <div class="span12 aright">
        <button class="btn btn-danger cancelBtn" type="button">Annuler</button>
        <button class="btn btn-success saveBtn" type="button">Sauvegarder</button>
      </div>
    </div>
  </div>
</div>
<?php include('parts/footer.inc.php') ?>