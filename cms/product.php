<?php 
  $page = 'product';
  include('parts/header.inc.php');
?>
  <div class="main">
    <div class="navbar navbar-inverse products-subnav">
      <div class="navbar-inner">
        <a class="brand">Catégorie</a>
        <ul class="nav" id="category">
          <li data-id="1" class="active"><a>Huiles d'olives</a></li>
          <li data-id="2"><a>Vinaigres</a></li>
          <li data-id="3"><a>Garde-manger</a></li>
          <li data-id="4"><a>Ensembles-cadeaux</a></li>
        </ul>
        <ul class="nav pull-right">
          <a class="brand pull-right">Type</a>
          <li class="dropdown active">
            <a class="dropdown-toggle" data-toggle="dropdown">
              <span id="type_label">Tous</span>
              <b class="caret"></b>
            </a>
            <ul id="type" class="dropdown-menu">
              <li data-id="0" class="active"><a>Tous</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
    <div class="row-fluid">
      <div class="listbox span12">
        <div class="listboxheader clearfix">
          <form id="searchfrm" class="form-search fleft" action="javascript:void(0);">
            <input type="text" class="input-large search-query" placeholder="Filtrer les résultats ..." autocomplete="off">
          </form>
          <div id="status" class="btn-group offset1" data-toggle="buttons-radio">
            <button class="btn btn-inverse active" data-value="1">Actifs</button>
            <button class="btn btn-inverse" data-value="0">Inactifs</button>
            <button class="btn btn-inverse" data-value="">Tous</button>
          </div>
          <div class="addnew fright">
            <a class="btn btn-inverse"><i class="icon-plus icon-white"></i> Nouveau produit</a>
          </div>
        </div>
        <div class="newentry"></div>
        <ul class="entryheader">
          <li class="clearfix">
            <div class="fleft">
              <span class="prdId"><span class="badge">id</span></span>
              <span class="prdName"><span class="badge">nom</span></span>
              <span class="prdSku"><span class="badge">sku</span></span>
              <span class="prdType"><span class="badge">type</span></span>
              <span class="prdStock"><span class="badge">stock</span></span>
              <span class="prdPrice"><span class="badge">prix</span></span>
              <span class="prdSaleprice"><span class="badge">prix soldé</span></span>
            </div>
            <div class="fright">
              <span class="statusbtn"></span>
              <span class="editbtn"></span>
            </div>
          </li>
        </ul>
        <ul class="itemslist boxcontent">
          <li class="templ productitem">
            <div class="handle" title="Réordonner [glissez/déposez]"></div>
            <div class="itemrow clearfix">
              <div class="fleft">
                <span class="prdId autofill" data-fieldname="prd_id"></span>
                <span class="prdName autofill" data-fieldname="pdd_name_fr"></span>
                <span class="prdSku autofill" data-fieldname="prd_sku"></span>
                <span class="prdType autofill" data-fieldname="prd_typ_id_fk"></span>
                <span class="prdStock autofill" data-fieldname="prd_stock"></span>
                <span class="prdPrice right autofill" data-fieldname="prd_price"></span>
                <span class="prdSaleprice right autofill" data-fieldname="prd_saleprice"></span>
              </div>
              <div class="fright">
                <span class="statusbtn" title="Activer / Désactiver [cliquez]"><i class="icon-star"></i></span>
                <span class="editbtn" title="Modifier / Voir le détail [cliquez]"><i class="icon-edit"></i></span>
              </div>
            </div>
            <div class="editentry"></div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="templ detailbox">
  <div class="editform">
    <div class="detailboxheader clearfix">
      <h4 class="itemTitle span9 fleft"></h4>
      <div class="span3 aright">
        <button class="btn btn-danger cancelBtn" type="button">Annuler</button>
        <button class="btn btn-success saveBtn" type="button">Sauvegarder</button>
      </div>
    </div>
    <div class="boxcontent">
      <form>
        <input type="hidden" name="prd_id" value="0"/>
        <input type="hidden" name="prd_sortorder" value="0"/>
        <fieldset>
          <p class="lead">Information générale</p>
          <div class="controls controls-row">
            <div class="controls-group span3">
              <label><span class="label">CATÉGORIE</span></label>
              <select name="prd_cat_id_fk" class="span12" required disabled="disabled">
                <option value="0">Choisir une catégorie</option>
                <option value="1">Huiles d'olives extra vierge</option>
                <option value="2">Vinaigres</option>
                <option value="3">Garde-manger</option>
                <option value="4">Ensembles-cadeaux</option>
              </select>
            </div>
            <div class="controls-group span3">
              <label><span class="label">TYPE</span></label>
              <select class="span12 type_select" name="prd_typ_id_fk" required pattern="[1-9][0-9]?">
                <option value="">Choisir un type</option>
              </select>
            </div>
            <div class="controls-group span3">
              <label><span class="label">TAXABLE</span></label>
              <select class="span12" name="prd_taxable" required>
                <option value="0">Non</option>
                <option value="1">Oui</option>
              </select>
            </div>
            <div class="controls-group span3">
              <label><span class="label">AFFICHAGE</span></label>
              <select class="span12" name="prd_active" id="active" required>
                <option value="1">Activé</option>
                <option value="0">Désactivé</option>
              </select>
            </div>
          </div>
          <div class="controls controls-row">
            <div class="controls-group span2">
              <label><span class="label">SKU</span></label>
              <input type="text" class="span12" name="prd_sku" required pattern="[\w-]{2,25}" maxlength="25" />
            </div>
            <div class="controls-group span1">
              <label><span class="label">PRIX</span></label>
              <input type="text" class="span12" name="prd_price" required pattern="\d{1,4}((\.|,)\d{1,2})?" maxlength="7" />
            </div>
            <div class="controls-group span2">
              <label><span class="label">PRIX SOLDÉ</span></label>
              <input type="text" class="span12" name="prd_saleprice" pattern="\d{1,4}((\.|,)\d{1,2})?" maxlength="7" />
            </div>
            <div class="controls-group span1">
              <label title="Unités en stock."><span class="label">QTE</span></label>
              <input type="text" class="span12" name="prd_stock" required pattern="\d{1,3}" maxlength="3" />
            </div>
            <div class="controls-group span2">
              <label title="Date d'ajout de l'article au catalogue."><span class="label">DATE</span></label>
              <input type="text" readonly class="span12 datepicker" name="prd_date" required pattern="\d{4}-\d{2}-\d{2}"/>
            </div>
            <div class="controls-group span1">
              <label title="Poids de l'article en 'grammes'."><span class="label">poids</span></label>
              <input type="text" class="span12" name="prd_weight" required pattern="\d{1,5}" maxlength="5" />
            </div>
            <div class="controls-group span1">
              <label title="Mesure en 'mm' du plus long côté."><span class="label">LONG.</span></label>
              <input type="text" class="span12" name="prd_length" pattern="\d{1,4}" maxlength="4" />
            </div>
            <div class="controls-group span1">
              <label title="Mesure en 'mm' du second plus long côté."><span class="label">LARG.</span></label>
              <input type="text" class="span12" name="prd_width" pattern="\d{1,4}" maxlength="4" />
            </div>
            <div class="controls-group span1">
              <label title="Mesure en 'mm' du plus petit côté."><span class="label">PROF.</span></label>
              <input type="text" class="span12" name="prd_height" pattern="\d{1,4}" maxlength="4" />
            </div>
          </div>
          <p class="lead">Images</p>
          <div class="controls controls-row">
            <div class="controls-group span6 imgupcontainer" data-imgsize="small">
              <label><span class="label">IMAGE - PETIT FORMAT</span></label>
              <div class="imgupbutton">
                <div class="imgupmask btn btn-medium">Choisir un fichier</div>
                <input class="imgupinput" type="file" name="prd_img_small" title="" />
              </div>
              <div class="imguppreviewbox">
                <a class="imgupremovebtn" href="javascript:void(0);" title="Supprimer l'image"><i class="icon-remove icon-white"></i></a>
                <div class="imguppreview" data-fieldname="prd_img_small"></div>
                <div class="imgupmessage imgupdefaultmsg">
                  Aucune image associée.
                </div>
                <div class="imgupmessage imgupformatmsg">
                  Formats acceptés : JPG, GIF et PNG uniquement. Réessayez avec un fichier image adéquat.
                </div>
                <div class="imgupmessage imgupsizemsg">
                  Taille de fichier maximale acceptée : 1 Mo. Réessayez avec un fichier de plus petite taille.
                </div>
              </div>
            </div>
            <div class="controls-group span6 imgupcontainer" data-imgsize="large">
              <label><span class="label">IMAGE - GRAND FORMAT</span></label>
              <div class="imgupbutton">
                <div class="imgupmask btn btn-medium">Choisir un fichier</div>
                <input class="imgupinput" type="file" name="prd_img_large" title=""/>
              </div>
              <div class="imguppreviewbox">
                <a class="imgupremovebtn" href="javascript:void(0);" title="Supprimer l'image"><i class="icon-remove icon-white"></i></a>
                <div class="imguppreview" data-fieldname="prd_img_large"></div>
                <div class="imgupmessage imgupdefaultmsg">
                  Aucune image associée.
                </div>
                <div class="imgupmessage imgupformatmsg">
                  Formats acceptés : JPG, GIF et PNG uniquement. Réessayez avec un fichier image adéquat.
                </div>
                <div class="imgupmessage imgupsizemsg">
                  Taille de fichier maximale acceptée : 1 Mo. Réessayez avec un fichier de plus petite taille.
                </div>
              </div>
            </div>
          </div>
          <p class="lead">Textes</p>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label><span class="label label-info">NOM DU PRODUIT</span></label>
              <input title="Nom du produit en français" type="text" name="pdd_name_fr" class="span12 use_as_frmtitle" required pattern=".{3,255}" maxlength="255" autocomplete="off" />
            </div>
            <div class="controls-group span6">
              <label><span class="label label-important">PRODUCT NAME</span></label>
              <input title="Nom du produit en anglais" type="text" name="pdd_name_en" class="span12" required pattern=".{3,255}" maxlength="255" autocomplete="off" />
            </div>
          </div>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label><span class="label label-info">PROVENANCE</span></label>
              <input autocomplete="off" type="text" name="pdd_origin_fr" class="span12 typeaheadinput" placeholder="Ex. : Cordoue, Espagne" />
            </div>
            <div class="controls-group span6">
              <label><span class="label label-important">ORIGIN</span></label>
              <input autocomplete="off" type="text" name="pdd_origin_en" class="span12 typeaheadinput" placeholder="Ex.: Córdoba, Spain" />
            </div>
          </div>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label><span class="label label-info">FORMAT</span></label>
              <input type="text" name="pdd_format_fr" class="span12" placeholder="Ex. : Bouteille de 1 litre" />
            </div>
            <div class="controls-group span6">
              <label><span class="label label-important">PACKAGING</span></label>
              <input type="text" name="pdd_format_en" class="span12" placeholder="Ex.: 1 liter bottle" />
            </div>
          </div>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label><span class="label label-info">URL RÉFÉRENCEMENT</span></label>
              <input type="text" name="pdd_slug_fr" class="span12" disabled="disabled" value="Générée automatiquement" />
            </div>
            <div class="controls-group span6">
              <label><span class="label label-important">SEO-FRIENDLY URL</span></label>
              <input type="text" name="pdd_slug_en" class="span12" disabled="disabled" value="Générée automatiquement" />
            </div>
          </div>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label title="Premier champ de description libre."><span class="label label-info">DESCRIPTION 1</span></label>
              <input autocomplete="off" title="Étiquette en français" type="text" name="pdd_desc1_title_fr" class="span12 typeaheadinput" placeholder="Ex. : Saveurs" /><br/>
              <textarea title="Texte en français." rows="5" name="pdd_desc1_text_fr" class="span12"></textarea>
            </div>
            <div class="controls-group span6">
              <label title="Premier champ de description libre."><span class="label label-important">DESCRIPTION 1</span></label>
              <input autocomplete="off" title="Étiquette en anglais" type="text" name="pdd_desc1_title_en" class="span12 typeaheadinput" placeholder="Ex. : Characteristics" /><br/>
              <textarea title="Texte en anglais" rows="5" name="pdd_desc1_text_en" class="span12"></textarea>
            </div>
          </div>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label title="Deuxième champ de description libre."><span class="label label-info">DESCRIPTION 2</span></label>
              <input autocomplete="off" title="Étiquette en français" type="text" name="pdd_desc2_title_fr" class="span12 typeaheadinput" placeholder="Ex. : En bouche" /><br/>
              <textarea title="Texte en français." rows="5" name="pdd_desc2_text_fr" class="span12"></textarea>
            </div>
            <div class="controls-group span6">
              <label title="Deuxième champ de description libre."><span class="label label-important">DESCRIPTION 2</span></label>
              <input autocomplete="off" title="Étiquette en anglais" type="text" name="pdd_desc2_title_en" class="span12 typeaheadinput" placeholder="Ex. : Taste" /><br/>
              <textarea title="Texte en anglais" rows="5" name="pdd_desc2_text_en" class="span12"></textarea>
            </div>
          </div>
          <div class="controls controls-row">
            <div class="controls-group span6">
              <label title="Troisième champ de description libre."><span class="label label-info">DESCRIPTION 3</span></label>
              <input autocomplete="off" title="Étiquette en français" type="text" name="pdd_desc3_title_fr" class="span12 typeaheadinput" placeholder="Ex. : Variétés d'olives" /><br/>
              <textarea title="Texte en français." rows="5" name="pdd_desc3_text_fr" class="span12"></textarea>
            </div>
            <div class="controls-group span6">
              <label title="Troisième champ de description libre."><span class="label label-important">DESCRIPTION 3</span></label>
              <input autocomplete="off" title="Étiquette en anglais" type="text" name="pdd_desc3_title_en" class="span12 typeaheadinput" placeholder="Ex. : Olives variety" /><br/>
              <textarea title="Texte en anglais" rows="5" name="pdd_desc3_text_en" class="span12"></textarea>
            </div>
          </div>
        </fieldset>
      </form>
    </div>
    <div class="detailboxfooter clearfix">
      <div class="span12 aright">
        <button class="btn btn-danger cancelBtn" type="button">Annuler</button>
        <button class="btn btn-success saveBtn" type="button">Sauvegarder</button>
      </div>
    </div>
  </div>
</div>
<?php include('parts/footer.inc.php') ?>