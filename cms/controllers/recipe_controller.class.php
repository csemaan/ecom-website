<?php 
class RecipeController extends Controller {
	function add() {
		return $this->model->add($_POST);
	}
	
	function get() {
		return $this->model->get($_POST);
	}
	
	function get_one() {
		return $this->model->getOne($_POST);
	}

	function remove() {
		return $this->model->remove($_POST);
	}

	function toggle_status() {
		return $this->model->toggleStatus($_POST);
	}
	
	function get_typeahead() {
		return $this->model->getTypeahead($_POST);
	}
}
?>