<?php 
/*
 * Everything here is obvious
 * 
 */
class UserController extends Controller {
    public function login() {
        if(!isset($_SESSION["loggedInUserID"])) {
            if($_POST["uname"]=="") {
                return -1;
            }
            $r = $this->getUser($_POST);
            if($r!==false) {
                // Delete last logged-in user
                $lastLoggedInUserSessionID = file_get_contents("logged-in-users.php");
                $lastLoggedInUserSessionFile = session_save_path()."sess_".$lastLoggedInUserSessionID;
                if(file_exists($lastLoggedInUserSessionFile)) {
                    unlink($lastLoggedInUserSessionFile);
                }
                file_put_contents(session_id());
                $_SESSION["loggedInUserID"] = $r[0]["usr_id"];
                return $_SESSION["loggedInUserID"];
            }
            return false;
        }
        return $_SESSION["loggedInUserID"];
    }
    
    public function getUser($p) {
        $r = $this->model->login($p);
		if($r!==false && count($r)>0) {
			return $r;
		}
		else {
			return false;
		}
	}
	
	public function logout() {
		unset($_SESSION["loggedInUserID"]);
		return true;
	}
}
?>