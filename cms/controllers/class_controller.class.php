<?php 
class ClassController extends Controller {
	function add() {
		return $this->model->add($_POST);
	}
	
	function get() {
		return $this->model->get($_POST);
	}

	function remove_schedule() {
		return $this->model->removeSchedule($_POST);
	}

	function toggle_status() {
		return $this->model->toggleStatus($_POST);
	}
	
	function get_typeahead() {
		return $this->model->getTypeahead($_POST);
	}
}
?>