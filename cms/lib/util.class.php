<?php 
class Util {
	public static function saveUploadedFiles($files,$catalogue,$id) {
		$fileNames = array();
		foreach($files as $key => $file) {
			if(isset($file['name']) && $file['name']!="") {
				$basefilename = Util::makeFileName($id,$key,pathinfo($file['name'], PATHINFO_EXTENSION));
				$dir = IMAGE_UPLOAD_DIR.$catalogue."/";
				if(!is_dir($dir)) mkdir($dir);
			    move_uploaded_file($file['tmp_name'], $dir.$basefilename);
			    $fileNames[$key] = $basefilename;
			}
		}
		return $fileNames;
	}
	
	public static function removeImageFiles($catalogue,$files) {
		foreach($files as $imgLabel => $imgFileName) {
			$dir = IMAGE_UPLOAD_DIR.$catalogue."/";
			if($imgFileName !== "") {
				unlink(realpath($dir.$imgFileName));
			}
		}
	}

	public static function makeFileName($id,$key,$ext) {
		$keyParts = explode("_",$key);
		$imgKind = (count($keyParts)>2)?'_'.$keyParts[2]:"";
		//if(stripos($key, "small")>0) $size = "_small";
		//else if(stripos($key, "large")>0) $size = "_large";
		return $id.$imgKind.'.'.$ext;
	}

	public static function slugify($text){
		setlocale(LC_CTYPE, 'en_US.utf8');
		$text = preg_replace('#[^\\pL\d]+#u', '-', $text);
		$text = trim($text, '-');
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		$text = strtolower($text);
		$text = preg_replace('#[^-\w]+#', '', $text);
		return $text;
	}

	public static function slugify2($text){
		setlocale(LC_CTYPE, 'en_US.utf8');
		return preg_replace('#[^-\w]+#', '', strtolower(iconv('utf-8', 'us-ascii//TRANSLIT', trim(preg_replace('#[^\\pL\d]+#u', '-', $text), '-'))));
	}
}
?>