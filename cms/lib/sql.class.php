<?php 
// MySQLi Abstraction Class
// TODO : error handling, refining of select queries result, etc.

class Sql {
	protected $db;
	protected $result;
	
	function connect() {
		$this->db = new mysqli(MY_HOST,MY_USER,MY_PASS,MY_DBNAME);
		$this->db->query("SET NAMES 'UTF8'");
	}
	
	function close() {
		$this->db->close();
	}
	
	function clean($data) {
		// Incompatible with PHP<5.4
		array_walk($data,
			function(&$v,$k) {
				if(is_numeric($v)) {
					$v = (double)$v;
				}
				else {
					// Ugly hack due to the use of HTML5 FormData on the client AJAX/side 
					// (which forces us to shut down jQuery's data processing, which hence causes loads of backslashes being sent)
					// Need to find a better way, but couldn't after 1 week trying!
					if(!is_array($v)) {
						$v = str_replace('\r\n', '<br>', $v);
						$v = stripslashes($v);
						$v = trim($this->db->real_escape_string($v));
					}
					else $v = $this->clean($v);
				}
			}
		);
		return $data;
	}
	
	function setNull($v) {
		if(!$v && $v!=="0" && $v!==0) return "NULL";
		return $v;
	}

	function doQuery($q) {
		$this->result = $this->db->query($q);
	}
	
	function select($q) {
		$this->doQuery($q);
		$resultArray = array();
		while($record = $this->result->fetch_assoc()) {
			$resultArray[] = $record;
		}
		return $resultArray;
	}
	
	function update($q) {
		$this->doQuery($q);
		return $this->db->affected_rows;
	}
	
	function insert($q) {
		$this->doQuery($q);
		return $this->db->insert_id;
	}
	
	function delete($q) {
		$this->doQuery($q);
		return $this->db->affected_rows;
	}
}
?>