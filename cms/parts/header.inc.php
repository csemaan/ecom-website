<?php 
  session_start();
  if(!isset($_SESSION["loggedInUserID"]) && $page!="index") {
    header("Location: index.php?nl");
  }
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>Olive &amp; Olives - Gestionnaire de contenu</title>
    <meta name="robots" content="noindex,nofollow">
    <meta charset="utf-8">
    <meta name="viewport" content="width=1050">
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <![endif]-->
    <link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css" rel="stylesheet" media="screen">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="assets/css/cms.css" rel="stylesheet">
  </head>
  <body data-page='<?=$page?>'>
    <div class="container">
      <header>
        <div class="controlbar">
          <div class="ajaxloader"><img src="assets/img/loader2.gif"/></div>
          <div class="logo fleft"><a href="index.php"><img alt="Olive &amp; Olives" src="assets/img/logo.png"/></a></div>
          <nav class="fleft">
            <ul id="catalogue">
              <li data-name="slideshow" <?=(($page=='slideshow')?' class="active"':'')?>><a href="slideshow.php">Diaporama</a></li>
              <li data-name="product" <?=(($page=='product')?' class="active"':'')?>><a href="product.php">Produits</a></li>
              <li data-name="recipe"<?=(($page=='recipe')?' class="active"':'')?>><a href="recipe.php">Recettes</a></li>
              <li data-name="class"<?=(($page=='class')?' class="active"':'')?>><a href="class.php">Cours</a></li>
            </ul>
          </nav>
          <div class="logout fright"><i class="icon-off icon-white"></i> Déconnexion</div>
        </div>
      </header>