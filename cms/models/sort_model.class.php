<?php 
class SortModel extends Sql {
    /*
	 * 
	 * Save sort order series for requested catalogue/category.
	 * 
	 */
	function save($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		if(!isset($f_category)) {
			$f_category=0;
		}
		$q = "REPLACE INTO sort (srt_catalogue,srt_prd_cat_id,srt_idseries) 
        				VALUES ('$f_catalogue', $f_category, '$srt_idseries')";
		$r = $this->update($q);
		$this->close();
		return $r;
	}

	function get($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		$q = "SELECT srt_idseries FROM sort WHERE srt_catalogue='$f_catalogue'";
		$q .= (isset($f_category))?" AND srt_prd_cat_id = $f_category":"";
		$r = $this->select($q);
		$this->close();
		return $r;
	}

}
?>