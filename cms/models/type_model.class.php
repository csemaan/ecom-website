<?php 
class TypeModel extends Sql {
    /*
	 * 
	 * Get data in specified language for all types satisfying filters: 
	 *					category, status, keywords.
	 * 
	 */
	function get($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		if(isset($f_kw) && $f_kw != "") {
			$re = str_replace(" ","|",$f_kw);
		}
        $q = "SELECT * FROM type WHERE typ_cat_id_fk = $f_category 
        		AND typ_active=1 ORDER BY typ_sortorder ASC,typ_id DESC";
		$r = $this->select($q);
		$this->close();
		return $r;
	}
}
?>