<?php 
class RecipeModel extends Sql {
    private $languages = array("fr","en");

    /*
     * Add a new recipe.
     * 
     */
	function add($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		
		if($rcp_id==0) {
			$q1 = "INSERT INTO recipe VALUES (
										$rcp_id,
										'".trim(str_replace(" ", "", $rcp_product_links),",")."',
										'',
										'',
										'$rcp_date',
										$rcp_active)";
			$r1 = $this->insert($q1);
			if($r1) {
				$multiInsert = "";
				foreach($this->languages as $lan) {
		        	$multiInsert .=
		        			"(0,'$lan','"
		        			.${'rcd_title_'.$lan}."','"
		        			.${'rcd_ingredients_'.$lan}."','"
		        			.${'rcd_preparation_'.$lan}."','"
		        			.${'rcd_keywords_'.$lan}."','"
		        			.Util::slugify(${'rcd_title_'.$lan})."',$r1),";
		        }
			    $q2 = "INSERT INTO recipe_detail VALUES ".rtrim($multiInsert,",");
			    $r2 = $this->insert($q2);
			}
		}

		if(isset($r1))
			$rcp_id = $r1;

		// Save uploaded image files
		$imgFileNames = array();
		if(isset($_FILES) && count($_FILES)>0) {
			$imgFileNames = Util::saveUploadedFiles($_FILES,$f_catalogue,$rcp_id);
		}
		// Get filenames
		$rcp_img_small = (isset($imgFileNames["rcp_img_small"]))?$imgFileNames["rcp_img_small"]:"";
		$rcp_img_large = (isset($imgFileNames["rcp_img_large"]))?$imgFileNames["rcp_img_large"]:"";
		
		// Update record with image filenames.
		$p["rcp_id"] = $rcp_id;
		$this->modify($p,$imgFileNames);

		// Return ID of inserted product if this is a new record.
		return (isset($r1)?$r1:0);
	}

	
	/*
	 * 
	 * Modify a recipe record, updating all fields.
	 * 
	 */
	private function modify($p,$imgFileNames) {
	    $this->connect();
		$p = $this->clean($p);
		extract($p);

		$updateSmallImage = "";
		if(isset($imgFileNames['rcp_img_small'])) {
			$updateSmallImage = "rcp_img_small	= '".$imgFileNames['rcp_img_small']."',";
		}
		else if(isset($rcp_img_small_update) && $rcp_img_small_update=='') {
			$updateSmallImage = "rcp_img_small	= '',";
		}

		$updateLargeImage = "";
		if(isset($imgFileNames['rcp_img_large'])) {
			$updateLargeImage = "rcp_img_large	= '".$imgFileNames['rcp_img_large']."',";
		}
		else if(isset($rcp_img_large_update) && $rcp_img_large_update=='') {
			$updateLargeImage = "rcp_img_large	= '',";
		}
		
	    $q1 = "UPDATE recipe SET $updateSmallImage $updateLargeImage
		    				rcp_product_links 	= '".trim(str_replace(" ", "", $rcp_product_links),",")."',
							rcp_date 			= '$rcp_date',
							rcp_active 			= $rcp_active
	    					WHERE rcp_id=$rcp_id";
	    $r1 = $this->update($q1);
        foreach($this->languages as $lan) {
        	$q2 = "UPDATE recipe_detail SET
        					rcd_title		= '".${'rcd_title_'.$lan}."',
        					rcd_ingredients	= '".${'rcd_ingredients_'.$lan}."',
        					rcd_preparation = '".${'rcd_preparation_'.$lan}."',
					    	rcd_keywords	= '".trim(preg_replace("/ *, */", ", ", ${'rcd_keywords_'.$lan}),", ")."',
					    	rcd_slug		= '".Util::slugify(${'rcd_title_'.$lan})."'
        				WHERE rcd_rcp_id_fk=$rcp_id AND rcd_lang='$lan'";
        	$r2 = $this->update($q2);
        }
	    $this->close();
	    if(count($_FILES)>0 && $r1) {
			$imgFileNames = Util::saveUploadedFiles($_FILES,$f_catalogue,$rcp_id);
		}
	    return $r1;
	}
	
	/*
	 * 
	 * Get data in specified language for all recipes satisfying : 
	 *					status and keywords.
	 * 
	 */
	function get($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		// We need to make keyword search separately as it is language dependant and will 
		// not always return the whole record with EN and FR details.
		if(isset($f_kw) && $f_kw != "") {
			$re = preg_replace("/\s+/", "|", trim($f_kw));
			if(isset($re) && $re != "") {
				$sq = "SELECT GROUP_CONCAT(DISTINCT CAST(rcp_id AS CHAR)) as rcp_id  
						FROM recipe JOIN recipe_detail 
							ON rcd_rcp_id_fk=rcp_id 
							WHERE (rcd_title REGEXP '$re' 
        						OR rcd_keywords REGEXP '$re'
        						OR rcp_product_links REGEXP '$re' 
        						OR rcd_ingredients REGEXP '$re')";
				$sr = $this->select($sq);
				if($sr && count($sr)>0 && $sr[0]["rcp_id"]!==NULL) {
					$matchingIds = $sr[0]["rcp_id"];
				}
				else {
					$matchingIds = '0';	
				}
			}
		}
		//return count($sr);
        $q = "SELECT * FROM recipe 
        				JOIN recipe_detail 
                		ON rcd_rcp_id_fk = rcp_id 
                		WHERE 1=1";
        $q .= (isset($f_status)&&$f_status!=="")?" AND rcp_active=$f_status":"";
        $q .= (isset($matchingIds))?" AND rcp_id IN ($matchingIds)":"";
        $q .= (isset($srt_idseries))?" ORDER BY FIELD(rcp_id,$srt_idseries)":"";
       	$r = $this->select($q);
		$this->close();
		return $this -> fixPivot($r);
	}

	/*
	 * 
	 * Get text detail for one recipe in specified language.
	 * 
	 */
	function getOne($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		$q = "SELECT * FROM recipe_detail 
                		WHERE rcd_lang = '$f_lang' 
                		AND rcd_rcp_id_fk = $rcp_id";
		$r = $this->select($q);
		$this->close();
		return $r;
	}

	
	/*
	 * 
	 * Remove one recipe.
	 * 
	 */
	function remove($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		$q = "DELETE FROM recipe WHERE rcp_id=$rcp_id";
		$r2 = $this->delete($q);
		$this->close();
		return $r2;
	}

	/*
	 * 
	 * Deactivate/Activate one recipe.
	 * 
	 */
	function toggleStatus($p) {
	    $this->connect();
		$p = $this->clean($p);
		extract($p);
	    $q = "UPDATE recipe SET rcp_active = MOD(rcp_active+1,2) 
	    											WHERE rcp_id=$item_id";
	    $r = $this->update($q);
	    $this->close();
	    return $r;
	}

	/*
	 * 
	 * 	Get typeahead strings in the specified language and category for the 
	 *	following fields: 
	 *					Keywords
	 * 
	 */
	function getTypeahead($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		$q = array();
		$r = array();
		foreach($this->languages as $lan) {
			$q[$lan] = "SELECT 
	        		GROUP_CONCAT(DISTINCT rcd_keywords SEPARATOR ',') 
	        			as rcd_keywords_$lan
					FROM recipe_detail JOIN recipe ON rcd_rcp_id_fk=rcp_id
					WHERE rcd_lang='$lan'";
			$r[$lan] = $this->select($q[$lan]);
			array_walk($r[$lan][0],function(&$v,$k){
							$v = array_values(
									array_unique(
										array_map('trim',
											array_filter(
												explode(",",trim($v,", "))
												,function($val) {
													if(trim($val)!="")
														return true; 
													else 
														return false;
												}
											)
										)
									)
								);
						}
			);
		}
		$qSku = "SELECT GROUP_CONCAT(DISTINCT CONCAT(prd_sku,' | ',pdd_name) SEPARATOR ',')
							AS rcp_product_links FROM product JOIN product_detail 
							ON prd_id=pdd_prd_id_fk WHERE pdd_lang='fr' AND prd_active=1
							ORDER BY prd_sku ASC";
		$rSku = $this->select($qSku);
		array_walk($rSku[0],function(&$v,$k){$v = explode(",",trim($v,","));});
		$this->close();
		return array_merge($r["fr"][0],$r["en"][0],$rSku[0]);
	}

	/*
		Fixes multiple rows resultsets for recipe and recipe details
		Should be extended to work for other tables too.
	*/
	private function fixPivot($rs) {
		$fixedRS = array();
		for($i=0; $i<count($rs)-1; $i = $i+2) {
			$tempRecord = array();
			foreach($rs[$i] as $k => $v) {
				if(stripos($k,'rcp_')===0) {
					$tempRecord[$k] = $v;
				}
				else {
					$tempRecord[$k."_".$rs[$i]['rcd_lang']] = $v;	
				}
			}
			foreach($rs[$i+1] as $k => $v) {
				if(stripos($k,'rcd_')===0) {
					$tempRecord[$k."_".$rs[$i+1]['rcd_lang']] = $v;
				}
			}
			$fixedRS[] = $tempRecord;
		}
		return $fixedRS;
	}

}
?>