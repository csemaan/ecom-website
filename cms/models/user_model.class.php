<?php 
class UserModel extends Sql {
    
    /*
     * Get the password salt associated with a username.
     * 
     */
    private function getPwdSalt($uname) {
        $q = "SELECT usr_salt FROM cmsuser WHERE usr_uname='$uname'"; 
        $r = $this->select($q);
        if($r) {
            return $r[0]["usr_salt"];
        }
        return false;
    }
    
    /*
     * Returns the user record corresponding to the given username and password
     * Compare username and password encrypting the following string 
     * "##"+clearPwd+salt+"!!"
     * with Sha512 (lowercase Hex) before sending it on the wire to MySQL
     * 
     */
    function login($p) {
        $this->connect();
        $p = $this->clean($p);
        extract($p);
        $encryptedPwd = hash("sha512",'##'.$pwd.'!!'.$this->getPwdSalt($uname));
        $q = "SELECT usr_id FROM cmsuser WHERE usr_uname='$uname' 
                AND usr_pwd='$encryptedPwd' AND usr_disabled=0";
        $r = $this->select($q);
        $this->close();
        return $r;
    }
}
?>