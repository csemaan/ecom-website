<?php 
class ProductModel extends Sql {
    private $languages = array("fr","en");

    /*
     * Add a new product.
     * 
     */
	function add($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		
		// Upload files first.
		$imgFileNames = array();
		if(isset($_FILES) && count($_FILES)>0) {
			$imgFileNames = Util::saveUploadedFiles($_FILES,$f_catalogue,$prd_sku);
		}

		// Now get filenames
		$prd_img_small = (isset($imgFileNames["prd_img_small"]))?$imgFileNames["prd_img_small"]:"";
		$prd_img_large = (isset($imgFileNames["prd_img_large"]))?$imgFileNames["prd_img_large"]:"";
		
		// Call update.
		if($prd_id>0) {
		    return $this->modify($p,$imgFileNames);
		}
		
		$q1 = "INSERT INTO product VALUES (
									$prd_id,
									'$prd_sku',
									".str_replace(',', '.', $prd_price).",
									".$this->setNull($prd_saleprice).",
									$prd_taxable,
									$prd_stock,
									$prd_weight,
									".$this->setNull($prd_length).",
									".$this->setNull($prd_width).",
									".$this->setNull($prd_height).",
									'$prd_img_small',
									'$prd_img_large',
									$prd_sortorder,
									'$prd_date',
									$prd_active,
									$f_category,
									$prd_typ_id_fk)";
		$r1 = $this->insert($q1);
		if($r1) {
			$multiInsert = "";
			foreach($this->languages as $lan) {
	        	$multiInsert .=
	        			"(0,'$lan','"
	        			.${'pdd_name_'.$lan}."','"
	        			.${'pdd_origin_'.$lan}."','"
	        			.${'pdd_format_'.$lan}."','"
	        			.${'pdd_desc1_title_'.$lan}."','"
	        			.${'pdd_desc1_text_'.$lan}."','"
	        			.${'pdd_desc2_title_'.$lan}."','"
	        			.${'pdd_desc2_text_'.$lan}."','"
	        			.${'pdd_desc3_title_'.$lan}."','"
	        			.${'pdd_desc3_text_'.$lan}."','"
	        			.Util::slugify(${'pdd_name_'.$lan})."',$r1),";
	        }
		    $q2 = "INSERT INTO product_detail VALUES ".rtrim($multiInsert,",");
		    $r2 = $this->insert($q2);
		}
		$this->close();
		return $r1;
	}

	
	/*
	 * 
	 * Modify a product record, updating all fields.
	 * 
	 */
	private function modify($p,$imgFileNames) {
	    $this->connect();
		$p = $this->clean($p);
		extract($p);

		$updateSmallImage = "";
		if(isset($imgFileNames['prd_img_small'])) {
			$updateSmallImage = "prd_img_small	= '".$imgFileNames['prd_img_small']."',";
		}
		else if(isset($prd_img_small_update) && $prd_img_small_update=='') {
			$updateSmallImage = "prd_img_small	= '',";
		}

		$updateLargeImage = "";
		if(isset($imgFileNames['prd_img_large'])) {
			$updateLargeImage = "prd_img_large	= '".$imgFileNames['prd_img_large']."',";
		}
		else if(isset($prd_img_large_update) && $prd_img_large_update=='') {
			$updateLargeImage = "prd_img_large	= '',";
		}
		
	    $q1 = "UPDATE product SET $updateSmallImage $updateLargeImage
		    				prd_sku 		= '$prd_sku',
		    				prd_price 		= ".str_replace(',', '.', $prd_price).",
		    				prd_saleprice 	= ".$this->setNull($prd_saleprice).",
		    				prd_taxable 	= $prd_taxable,
							prd_stock 		= $prd_stock,
							prd_weight 		= $prd_weight,
							prd_length 		= ".$this->setNull($prd_length).",
							prd_width 		= ".$this->setNull($prd_width).",
							prd_height 		= ".$this->setNull($prd_height).",
							prd_sortorder 	= $prd_sortorder,
							prd_date 		= '$prd_date',
							prd_active 		= $prd_active,
							prd_cat_id_fk 	= $f_category,
							prd_typ_id_fk 	= $prd_typ_id_fk
	    					WHERE prd_id=$prd_id";
	    $r1 = $this->update($q1);
        foreach($this->languages as $lan) {
        	$q2 = "UPDATE product_detail SET
        					pdd_name		= '".${'pdd_name_'.$lan}."',
        					pdd_origin		= '".${'pdd_origin_'.$lan}."',
        					pdd_format		= '".${'pdd_format_'.$lan}."',
					    	pdd_desc1_title	= '".${'pdd_desc1_title_'.$lan}."',
					    	pdd_desc1_text	= '".${'pdd_desc1_text_'.$lan}."',
					    	pdd_desc2_title	= '".${'pdd_desc2_title_'.$lan}."',
					    	pdd_desc2_text	= '".${'pdd_desc2_text_'.$lan}."',
					    	pdd_desc3_title	= '".${'pdd_desc3_title_'.$lan}."',
					    	pdd_desc3_text	= '".${'pdd_desc3_text_'.$lan}."',
					    	pdd_slug		= '".Util::slugify(${'pdd_name_'.$lan})."'
        				WHERE pdd_prd_id_fk=$prd_id AND pdd_lang='$lan'";
        	$r2 = $this->update($q2);
        }
	    $this->close();
	    if(count($_FILES)>0 && $r1) {
			$imgFileNames = Util::saveUploadedFiles($_FILES,$f_catalogue,$prd_sku);
		}
	    return $r1;
	}
	
	/*
	 * 
	 * Get data in specified language for all products satisfying filters: 
	 *					category, type, status, keywords.
	 * 
	 */
	function get($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		// We need to make keyword search separately as it is language dependant and will 
		// not always return the whole record with EN and FR details.
		if(isset($f_kw) && $f_kw != "") {
			$re = preg_replace("/\s+/", "|", trim($f_kw));
			if(isset($re) && $re != "") {
				$sq = "SELECT GROUP_CONCAT(DISTINCT CAST(prd_id AS CHAR)) as prd_id  
						FROM product JOIN product_detail 
							ON pdd_prd_id_fk=prd_id 
							WHERE (pdd_name REGEXP '$re' 
        						OR pdd_origin REGEXP '$re'
        						OR pdd_format REGEXP '$re' 
        						OR prd_sku REGEXP '$re')";
				$sr = $this->select($sq);
				if($sr && count($sr)>0 && $sr[0]["prd_id"]!==NULL) {
					$matchingIds = $sr[0]["prd_id"];
				}
				else {
					$matchingIds = '0';	
				}
			}
		}
        $q = "SELECT * FROM product 
        				JOIN product_detail 
                		ON pdd_prd_id_fk = prd_id 
                		WHERE prd_cat_id_fk = $f_category";
        $q .= (isset($f_type)&&$f_type!="0")?" AND prd_typ_id_fk=$f_type":"";
        $q .= (isset($f_status)&&$f_status!=="")?" AND prd_active=$f_status":"";
        $q .= (isset($matchingIds))?" AND prd_id IN ($matchingIds)":"";
        $q .= (isset($srt_idseries))?" ORDER BY FIELD(prd_id,$srt_idseries)":"";
       	$r = $this->select($q);
		$this->close();
		return $this -> fixPivot($r);
	}

	/*
	 * 
	 * Get text detail for one product in specified language.
	 * 
	 */
	function getOne($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		$q = "SELECT * FROM product_detail 
                		WHERE pdd_lang = '$f_lang' 
                		AND pdd_prd_id_fk = $prd_id";
		$r = $this->select($q);
		$this->close();
		return $r;
	}

	
	/*
	 * 
	 * Remove one product.
	 * 
	 */
	function remove($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		$q = "DELETE FROM product WHERE prd_id=$prd_id";
		$r2 = $this->delete($q);
		$this->close();
		return $r2;
	}

	/*
	 * 
	 * Deactivate/Activate one product.
	 * 
	 */
	function toggleStatus($p) {
	    $this->connect();
		$p = $this->clean($p);
		extract($p);
	    $q = "UPDATE product SET prd_active = MOD(prd_active+1,2) 
	    											WHERE prd_id=$item_id";
	    $r = $this->update($q);
	    $this->close();
	    return $r;
	}

	/*
	 * 
	 * 	Get typeahead strings in the specified language and category for the 
	 *	following fields: 
	 *					Origin, Desc 1 Title, Desc 2 Title, Desc 3 Title
	 * 
	 */
	function getTypeahead($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		$q = array();
		$r = array();
		foreach($this->languages as $lan) {
			$q[$lan] = "SELECT 
	        		GROUP_CONCAT(DISTINCT pdd_origin SEPARATOR '|') 
	        			as pdd_origin_$lan,
					GROUP_CONCAT(DISTINCT pdd_desc1_title SEPARATOR '|') 
						as pdd_desc1_title_$lan,
					GROUP_CONCAT(DISTINCT pdd_desc2_title SEPARATOR '|') 
						as pdd_desc2_title_$lan,
					GROUP_CONCAT(DISTINCT pdd_desc3_title SEPARATOR '|')
						as pdd_desc3_title_$lan
					FROM product_detail JOIN product ON pdd_prd_id_fk=prd_id
					WHERE pdd_lang='$lan' AND prd_cat_id_fk=$f_category";
			$r[$lan] = $this->select($q[$lan]);
			array_walk($r[$lan][0],function(&$v,$k){$v = explode("|",trim($v,"|"));});
		}
		$this->close();
		return array_merge($r["fr"][0],$r["en"][0]);
	}

	/*
		Fixes multiple rows resultsets for product and product details
		Should be extended to work for other tables too.
	*/
	private function fixPivot($rs) {
		$fixedRS = array();
		for($i=0; $i<count($rs)-1; $i = $i+2) {
			$tempRecord = array();
			foreach($rs[$i] as $k => $v) {
				if(stripos($k,'prd_')===0) {
					$tempRecord[$k] = $v;
				}
				else {
					$tempRecord[$k."_".$rs[$i]['pdd_lang']] = $v;	
				}
			}
			foreach($rs[$i+1] as $k => $v) {
				if(stripos($k,'pdd_')===0) {
					$tempRecord[$k."_".$rs[$i+1]['pdd_lang']] = $v;
				}
			}
			$fixedRS[] = $tempRecord;
		}
		return $fixedRS;
	}

}
?>