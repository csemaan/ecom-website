<?php 
class SlideshowModel extends Sql {
    private $languages = array("fr","en");

    /*
     * Add a new slide.
     * 
     */
	function add($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		
		if($sli_id==0) {
			$q1 = "INSERT INTO slide VALUES (
										$sli_id,
										'',
										$sli_active)";
			$r1 = $this->insert($q1);
			if($r1) {
				$multiInsert = "";
				foreach($this->languages as $lan) {
		        	$multiInsert .=
		        			"(0,'$lan','"
		        			.${'sld_title_'.$lan}."','"
		        			.${'sld_desc_'.$lan}."','"
		        			.${'sld_link_'.$lan}."',"
		        			."$r1),";
		        }
			    $q2 = "INSERT INTO slide_detail VALUES ".rtrim($multiInsert,",");
			    $r2 = $this->insert($q2);
			}
		}
		if(isset($r1))
			$sli_id = $r1;

		// Save Uploaded Image Files
		$imgFileNames = array();
		if(isset($_FILES) && count($_FILES)>0) {
			$imgFileNames = Util::saveUploadedFiles($_FILES,$f_catalogue,$sli_id);
		}
		// Get filenames
		$sli_img = (isset($imgFileNames["sli_img"]))?$imgFileNames["sli_img"]:"";
		
	    // Update record.
	    $p["sli_id"] = $sli_id;
	    $this->modify($p,$imgFileNames);

	    // Return ID of new record.
		return (isset($r1)?$r1:0);
	}

	
	/*
	 * 
	 * Modify a slide record, updating all fields.
	 * 
	 */
	private function modify($p,$imgFileNames) {
	    $this->connect();
		$p = $this->clean($p);
		extract($p);

		$updateImage = "";
		if(isset($imgFileNames['sli_img'])) {
			$updateImage = "sli_img	= '".$imgFileNames['sli_img']."',";
		}
		else if(isset($sli_img_update) && $sli_img_update=='') {
			$updateImage = "sli_img	= '',";
		}
		
	    $q1 = "UPDATE slide SET $updateImage
							sli_active 			= $sli_active
	    					WHERE sli_id=$sli_id";
	    $r1 = $this->update($q1);
        foreach($this->languages as $lan) {
        	$q2 = "UPDATE slide_detail SET
        					sld_title		= '".${'sld_title_'.$lan}."',
        					sld_desc	= '".${'sld_desc_'.$lan}."',
        					sld_link = '".${'sld_link_'.$lan}."'
        				WHERE sld_sli_id_fk=$sli_id AND sld_lang='$lan'";
        	$r2 = $this->update($q2);
        }
	    $this->close();
	    if(count($_FILES)>0 && $r1) {
			$imgFileNames = Util::saveUploadedFiles($_FILES,$f_catalogue,$sli_id);
		}
	    return $r1;
	}
	
	/*
	 * 
	 * Get data in specified language for all slides satisfying : 
	 *					status and keywords.
	 * 
	 */
	function get($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		// We need to make keyword search separately as it is language dependant and will 
		// not always return the whole record with EN and FR details.
		if(isset($f_kw) && $f_kw != "") {
			$re = preg_replace("/\s+/", "|", trim($f_kw));
			if(isset($re) && $re != "") {
				$sq = "SELECT GROUP_CONCAT(DISTINCT CAST(sli_id AS CHAR)) as sli_id  
						FROM slide JOIN slide_detail 
							ON sld_sli_id_fk=sli_id 
							WHERE (sld_title REGEXP '$re' 
        						OR sld_desc REGEXP '$re'
        						OR sld_link REGEXP '$re')";
				$sr = $this->select($sq);
				if($sr && count($sr)>0 && $sr[0]["sli_id"]!==NULL) {
					$matchingIds = $sr[0]["sli_id"];
				}
				else {
					$matchingIds = '0';	
				}
			}
		}
		//return count($sr);
        $q = "SELECT * FROM slide 
        				JOIN slide_detail 
                		ON sld_sli_id_fk = sli_id 
                		WHERE 1=1";
        $q .= (isset($f_status)&&$f_status!=="")?" AND sli_active=$f_status":"";
        $q .= (isset($matchingIds))?" AND sli_id IN ($matchingIds)":"";
        $q .= (isset($srt_idseries))?" ORDER BY FIELD(sli_id,$srt_idseries)":"";
       	$r = $this->select($q);
		$this->close();
		return $this -> fixPivot($r);
	}

	/*
	 * 
	 * Get text detail for one slide in specified language.
	 * 
	 */
	function getOne($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		$q = "SELECT * FROM slide_detail 
                		WHERE sld_lang = '$f_lang' 
                		AND sld_sli_id_fk = $sli_id";
		$r = $this->select($q);
		$this->close();
		return $r;
	}

	
	/*
	 * 
	 * Remove one slide.
	 * 
	 */
	function remove($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		$q = "DELETE FROM slide WHERE sli_id=$sli_id";
		$r2 = $this->delete($q);
		$this->close();
		return $r2;
	}

	/*
	 * 
	 * Deactivate/Activate one slide.
	 * 
	 */
	function toggleStatus($p) {
	    $this->connect();
		$p = $this->clean($p);
		extract($p);
	    $q = "UPDATE slide SET sli_active = MOD(sli_active+1,2) 
	    											WHERE sli_id=$item_id";
	    $r = $this->update($q);
	    $this->close();
	    return $r;
	}

	/*
		Fixes multiple rows resultsets for slide and slide details
		Should be extended to work for other tables too.
	*/
	private function fixPivot($rs) {
		$fixedRS = array();
		for($i=0; $i<count($rs)-1; $i = $i+2) {
			$tempRecord = array();
			foreach($rs[$i] as $k => $v) {
				if(stripos($k,'sli_')===0) {
					$tempRecord[$k] = $v;
				}
				else {
					$tempRecord[$k."_".$rs[$i]['sld_lang']] = $v;	
				}
			}
			foreach($rs[$i+1] as $k => $v) {
				if(stripos($k,'sld_')===0) {
					$tempRecord[$k."_".$rs[$i+1]['sld_lang']] = $v;
				}
			}
			$fixedRS[] = $tempRecord;
		}
		return $fixedRS;
	}

}
?>