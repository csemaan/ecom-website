<?php 
class ClassModel extends Sql {
    private $languages = array("fr","en");

    /*
     * Add a new product.
     * 
     */
	function add($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		
		if($cls_id==0) {
			$q1 = "INSERT INTO class VALUES (
										$cls_id,
										'$cls_lang',
										'$cls_desc',
										'$cls_title',
										'$cls_subtitle',
										'$cls_instructor',
										'".Util::slugify($cls_instructor)."',
										'$cls_instructor_header',
										".str_replace(',', '.', $cls_price).",
										$cls_taxable,
										'',
										'',
										$cls_active)";
			$r1 = $this->insert($q1);
			if($r1 && isset($sch_id)) {
				$multiInsert = "";
				for($i=0; $i<count($sch_id); $i++) {
					$id = $sch_id[$i];
					$location = $sch_location[$i];
					$date = $sch_date[$i];
					$time = $sch_time[$i];
					$duration = $sch_duration[$i];
					$groupsize = $sch_groupsize[$i];
					$stock = $sch_stock[$i];
					$active = $sch_active[$i];
					
		        	$multiInsert .=
		        			"($id,'$location','$date','$time',$duration,$groupsize,$stock,$active,$r1),";
		        }
			    $q2 = "INSERT INTO class_schedule VALUES ".rtrim($multiInsert,",");
			    $r2 = $this->insert($q2);
			}
		}

		// Save uploaded image files
		$imgFileNames = array();
		if(isset($_FILES) && count($_FILES)>0) {
			$imgFileNames = Util::saveUploadedFiles($_FILES,$f_catalogue,(isset($r1))?$r1:$cls_id);
		}

		// Update record with image filenames.
		if(isset($r1)) {
			$this->updateImageNames($r1,$imgFileNames);
			return $r1;
		}
		else {
			return $this->modify($p,$imgFileNames);
		}
	}

	private function updateImageNames($clsId,$imgFileNames) {
		$updateImageCourse = "";
		if(isset($imgFileNames['cls_img_course'])) {
			$updateImageCourse = "cls_img_course	= '".$imgFileNames['cls_img_course']."'";
		}
		else if(isset($cls_img_course_update) && $cls_img_course_update=='') {
			$updateImageCourse = "cls_img_course	= ''";
		}
		$updateImageInstructor = "";
		if(isset($imgFileNames['cls_img_instructor'])) {
			$updateImageInstructor = "cls_img_instructor	= '".$imgFileNames['cls_img_instructor']."'";
		}
		else if(isset($cls_img_instructor_update) && $cls_img_instructor_update=='') {
			$updateImageInstructor = "cls_img_instructor	= ''";
		}
		$separator = ($updateImageCourse=="" || $updateImageInstructor=="")?"":",";
		$q1 = "UPDATE class SET $updateImageCourse $separator $updateImageInstructor WHERE cls_id=$clsId";
	    $r1 = $this->update($q1);
	}


	/*
	 * 
	 * Modify a record, updating all fields.
	 * 
	 */
	private function modify($p,$imgFileNames) {
	    $this->connect();
		$p = $this->clean($p);
		extract($p);

		$this->updateImageNames($cls_id,$imgFileNames);
		
	    $q1 = "UPDATE class SET
		    				cls_lang = '$cls_lang',
							cls_desc = '$cls_desc',
							cls_title = '$cls_title',
							cls_subtitle = '$cls_subtitle',
							cls_instructor = '$cls_instructor',
							cls_instructor_slug = '".Util::slugify($cls_instructor)."',
							cls_instructor_header = '$cls_instructor_header',
							cls_price = ".str_replace(',', '.', $cls_price).",
							cls_taxable = $cls_taxable,
							cls_active = $cls_active
	    					WHERE cls_id=$cls_id";
	    $r1 = $this->update($q1);
       	if(isset($sch_id)) {
	       	$multiInsert = '';
	       	for($i=0; $i<count($sch_id); $i++) {
				$id = $sch_id[$i];
				$location = $sch_location[$i];
				$date = $sch_date[$i];
				$time = $sch_time[$i];
				$duration = $sch_duration[$i];
				$groupsize = $sch_groupsize[$i];
				$stock = $sch_stock[$i];
				$active = $sch_active[$i];
				
	        	$multiInsert .=
	        			"($id,'$location','$date','$time',$duration,$groupsize,$stock,$active,$cls_id),";
	        }
	        $q2 = "REPLACE INTO class_schedule VALUES ".rtrim($multiInsert,",");
	        $r2 = $this->insert($q2);
	    }
	    $this->close();
	    return $r1;
	}
	
	/*
	 * 
	 * Get data in specified language for all products satisfying filters: 
	 *					category, type, status, keywords.
	 * 
	 */
	function get($p) {
		/**/
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		
		if(isset($f_kw) && $f_kw != "") {
			$re = preg_replace("/\s+/", "|", trim($f_kw));
			if(isset($re) && $re != "") {
				$sq = "SELECT GROUP_CONCAT(DISTINCT CAST(cls_id AS CHAR)) as cls_id  
						FROM class JOIN class_schedule 
							ON sch_cls_id_fk=cls_id 
							WHERE (cls_title REGEXP '$re' 
        						OR cls_instructor REGEXP '$re'
        						OR cls_desc REGEXP '$re' 
        						OR sch_location REGEXP '$re')";
				$sr = $this->select($sq);
				if($sr && count($sr)>0 && $sr[0]["cls_id"]!==NULL) {
					$matchingIds = $sr[0]["cls_id"];
				}
				else {
					$matchingIds = '0';	
				}
			}
		}

        $q = "SELECT * FROM class WHERE 1 = 1";
        $q .= (isset($f_status)&&$f_status!=="")?" AND cls_active=$f_status":"";
        $q .= (isset($matchingIds))?" AND cls_id IN ($matchingIds)":"";
        $q .= (isset($srt_idseries))?" ORDER BY FIELD(cls_id,$srt_idseries)":"";
       	$r = $this->select($q);
		$this->close();
		return $this -> fixPivot($r);
	}

	/*
	 * 
	 * Get text detail for one product in specified language.
	 * 
	 */
	function getSchedule($clsId) {
		$this->connect();
		$q = "SELECT *,DATE_FORMAT(sch_time, '%H:%i') AS sch_time FROM class_schedule 
                	   WHERE sch_active <> 2 
                	   AND CONCAT(sch_date,' ',sch_time) >= DATE_SUB(NOW(), INTERVAL 7 DAY)
                	   AND sch_cls_id_fk =$clsId";
		$r = $this->select($q);
		$this->close();
		return $r;
	}

	function getUpcomingString($clsId) {
		$this->connect();
		$q = "SELECT GROUP_CONCAT(DISTINCT sch_date ORDER BY sch_date ASC SEPARATOR ', ') AS sch_upcoming
						FROM class_schedule 
                	   	WHERE sch_active = 1
                	   	AND CONCAT(sch_date,' ',sch_time) >= NOW()
                	   	AND sch_cls_id_fk=$clsId";
		$r = $this->select($q);
		$this->close();
		return $r;
	}

	
	/*
	 * 
	 * Remove one schedule.
	 * 
	 */
	function removeSchedule($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		$q = "DELETE FROM class_schedule WHERE sch_id=$sch_id";
		$r2 = $this->delete($q);
		$this->close();
		return $r2;
	}

	/*
	 * 
	 * Deactivate/Activate one product.
	 * 
	 */
	function toggleStatus($p) {
	    $this->connect();
		$p = $this->clean($p);
		extract($p);
	    $q = "UPDATE class SET cls_active = MOD(cls_active+1,2) 
	    											WHERE cls_id=$item_id";
	    $r = $this->update($q);
	    $this->close();
	    return $r;
	}

	function getTypeahead($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		$q = array();
		$r = array();
		$q = "SELECT 
        		GROUP_CONCAT(DISTINCT cls_title SEPARATOR '|') AS cls_title,
				GROUP_CONCAT(DISTINCT cls_subtitle SEPARATOR '|') AS cls_subtitle,
				GROUP_CONCAT(DISTINCT cls_instructor SEPARATOR '|') AS cls_instructor,
				GROUP_CONCAT(DISTINCT cls_instructor_header SEPARATOR '|') AS cls_instructor_header,
				GROUP_CONCAT(DISTINCT sch_location SEPARATOR '|') AS sch_location
				FROM class_schedule JOIN class ON sch_cls_id_fk=cls_id";
		$r = $this->select($q);
		array_walk($r[0],function(&$v,$k){$v = explode("|",trim($v,"|"));});
		$this->close();
		return $r[0];
	}

	/*
		Fixes multiple rows resultsets for product and product details
		Should be extended to work for other tables too.
	*/
	private function fixPivot($rs) {
		$fixedRS = array();
		for($i=0; $i<count($rs); $i++) {
			$scheduleRecords = $this->getSchedule($rs[$i]["cls_id"]);
			$upcomingString = $this->getUpcomingString($rs[$i]["cls_id"]);
			$rs[$i]["schedule"] = $scheduleRecords;
			$rs[$i]["sch_upcoming"] = $upcomingString[0]["sch_upcoming"];
			$fixedRS[] = $rs[$i];
		}
		return $fixedRS;
	}

}
?>