<?php 
//define('CONFIG_FILE_PATH',"/var/www/vhosts/xxxxxxxxxxxxxxxxxxxxx.com/private/xxxxxxxxxxxxxxxxxxxxxx.conf.php");
//define('CONFIG_FILE_PATH',"/var/www/vhosts/xxxxxxxxxxxxxxxxxxxxx/private/xxxxxxxxxxxxxxxxxxxxxx.conf.php");

if(isset($_GET["url"])) {
    session_start();
    require_once(CONFIG_FILE_PATH);
    header("Content-Type: application/json");
    if(!isset($_SESSION["loggedInUserID"]) && $_GET["url"]!="user/login") {
		echo "No Access."; // This string is important as we test against it after each AJAX request completes.
	}
	else {
	    $router = new Router($_GET["url"]);
	    echo json_encode($router->route());
	}
}

class Router { 
    private $url;  
    
	function __construct($url) {
	    $this->url = $url;
	    spl_autoload_register(array($this,'classAutoload'));
	}
	
	public function route() {
	    // We assume the following URL convention: 
	    // module/action/param1/param2/.../paramN
		$urlArray = explode("/",$this->url);
		$module = $urlArray[0];
		array_shift($urlArray);
		$action = $urlArray[0];
		array_shift($urlArray);
		$queryParams = $urlArray;
		
		$controllerName = ucfirst($module)."Controller";
		$modelName = ucfirst($module)."Model";
		$controllerInstance = new $controllerName($modelName,$action);
		if(method_exists($controllerName,$action)) {
			return call_user_func_array(array($controllerInstance,$action),
			        $queryParams);
		}
		else {
			return false;
		}
	}
	
	static private function classNameToFileName($className) {
		return strtolower(preg_replace("/(.)([A-Z])/","$1_$2",$className));
	}
	
	private function classAutoload($className) {
		$fn = Router::classNameToFileName($className);
		if(file_exists("../controllers/".$fn.".class.php"))
			require_once("../controllers/".$fn.".class.php");
		else if(file_exists("../models/".$fn.".class.php"))
			require_once("../models/".$fn.".class.php");
		else if(file_exists("../lib/".$fn.".class.php"))
			require_once("../lib/".$fn.".class.php");
	}
}
?>