SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `oliveolives` ;
CREATE SCHEMA IF NOT EXISTS `oliveolives` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `oliveolives` ;

-- -----------------------------------------------------
-- Table `oliveolives`.`category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`category` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`category` (
  `cat_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `cat_name_fr` VARCHAR(100) NOT NULL COMMENT 'Nom de la catégorie en français.' ,
  `cat_name_en` VARCHAR(100) NOT NULL COMMENT 'Nom de la catégorie en anglais.' ,
  PRIMARY KEY (`cat_id`) ,
  UNIQUE INDEX `cat_name_en` (`cat_name_en` ASC) ,
  UNIQUE INDEX `cat_name_fr` (`cat_name_fr` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Catégorie de produits.';


-- -----------------------------------------------------
-- Table `oliveolives`.`product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`product` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`product` (
  `prd_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `prd_sku` SMALLINT UNSIGNED NOT NULL COMMENT 'Numéro SKU dans le catalogue Olive & Olives' ,
  `prd_price` DECIMAL(6,2) UNSIGNED NOT NULL COMMENT 'Prix ordinaire sous la forme 123.45 (point décimal)' ,
  `prd_saleprice` DECIMAL(6,2) UNSIGNED NULL DEFAULT NULL COMMENT 'Prix soldé dans le format 123.45 (point décimal et non pas virgule)' ,
  `prd_taxable` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Indiquer si le produit est taxable ou non (0:pas taxable,1:tps,2:tvp,3:tps+tvp)' ,
  `prd_stock` TINYINT UNSIGNED NULL DEFAULT NULL COMMENT 'Quantité de cet article en stock.' ,
  `prd_weight` SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT 'Poids en gramme estimé du produit emballé.' ,
  `prd_length` TINYINT UNSIGNED NULL DEFAULT NULL COMMENT 'Mesure du plus long côté du produit en mm.' ,
  `prd_width` SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT 'Mesure du second plus long côté du produit en mm.' ,
  `prd_height` SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT 'Mesure du plus petit côté du produit en mm.' ,
  `prd_img_small` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Nom du fichier image - petit format' ,
  `prd_img_large` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Nom du fichier image - grand format' ,
  `prd_sortorder` SMALLINT UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Ordre de tri arbitraire dans lequel ce produit sera placé.' ,
  `prd_date` DATE NULL DEFAULT NULL COMMENT 'Date d\'ajout du produit dans le catalogue.' ,
  `prd_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'Produit actif ou non.' ,
  `prd_cat_id_fk` TINYINT UNSIGNED NOT NULL COMMENT 'Identifiant de la catégorie de produit' ,
  `prd_typ_id_fk` TINYINT UNSIGNED NULL DEFAULT NULL COMMENT 'Identifiant du type de produit' ,
  PRIMARY KEY (`prd_id`) ,
  UNIQUE INDEX `prd_sku` (`prd_sku` ASC) ,
  INDEX `prd_cat_id_fk` (`prd_cat_id_fk` ASC) ,
  INDEX `prd_typ_id_fk` (`prd_typ_id_fk` ASC) ,
  CONSTRAINT `product_ibfk_1`
    FOREIGN KEY (`prd_cat_id_fk` )
    REFERENCES `oliveolives`.`category` (`cat_id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Produits de toutes les catégories';


-- -----------------------------------------------------
-- Table `oliveolives`.`product_detail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`product_detail` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`product_detail` (
  `pdd_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `pdd_lang` ENUM('fr','en') NOT NULL ,
  `pdd_name` VARCHAR(255) NOT NULL COMMENT 'Nom du produit dans la langue spécifiée.' ,
  `pdd_origin` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Provenance du produit dans la langue spécifiée' ,
  `pdd_format` VARCHAR(100) NULL DEFAULT NULL COMMENT 'Format de présentation du produit' ,
  `pdd_desc1_title` VARCHAR(25) NULL DEFAULT NULL COMMENT 'Étiquette dans la langue spécifiée du premier texte associé au produit' ,
  `pdd_desc1_text` MEDIUMTEXT NULL DEFAULT NULL COMMENT 'Texte dans la langue spécifiée du premier texte associé au produit' ,
  `pdd_desc2_title` VARCHAR(25) NULL DEFAULT NULL COMMENT 'Étiquette dans la langue spécifiée du deuxième texte associé au produit' ,
  `pdd_desc2_text` MEDIUMTEXT NULL DEFAULT NULL COMMENT 'Texte  dans la langue spécifiée du deuxième texte associé au produit' ,
  `pdd_desc3_title` VARCHAR(25) NULL DEFAULT NULL COMMENT 'Étiquette dans la langue spécifiée du troisième texte associé au produit' ,
  `pdd_desc3_text` MEDIUMTEXT NULL DEFAULT NULL COMMENT 'Texte dans la langue spécifiée du troisième texte associé au produit' ,
  `pdd_slug` VARCHAR(255) NOT NULL COMMENT 'URL de référencement pour ce produit' ,
  `pdd_prd_id_fk` SMALLINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`pdd_id`) ,
  INDEX `pdd_prd_id_fk` (`pdd_prd_id_fk` ASC) ,
  CONSTRAINT `product_detail_ibfk_1`
    FOREIGN KEY (`pdd_prd_id_fk` )
    REFERENCES `oliveolives`.`product` (`prd_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Textes à localiser associés au produit';


-- -----------------------------------------------------
-- Table `oliveolives`.`sort`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`sort` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`sort` (
  `srt_catalogue` ENUM('product','class','slide','recipe') CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `srt_prd_cat_id` TINYINT(4) NOT NULL DEFAULT '0' ,
  `srt_idseries` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT 'Série des identifiants de produits dans l\'ordre souhaité, séparés par des virgules.' ,
  UNIQUE INDEX `srt_catalogue` (`srt_catalogue` ASC, `srt_prd_cat_id` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `oliveolives`.`type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`type` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`type` (
  `typ_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `typ_name_fr` VARCHAR(25) NOT NULL COMMENT 'Nom du type de produits en français.' ,
  `typ_name_en` VARCHAR(25) NOT NULL COMMENT 'Nom du type de produits en anglais.' ,
  `typ_sortorder` TINYINT UNSIGNED NOT NULL COMMENT 'Ordre de tri des types de produits dans l\'affichage de la catégorie' ,
  `typ_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  `typ_cat_id_fk` TINYINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`typ_id`) ,
  UNIQUE INDEX `typ_name_fr` (`typ_name_fr` ASC, `typ_cat_id_fk` ASC) ,
  UNIQUE INDEX `typ_name_en` (`typ_name_en` ASC, `typ_cat_id_fk` ASC) ,
  INDEX `typ_cat_id_fk` (`typ_cat_id_fk` ASC) ,
  CONSTRAINT `type_ibfk_1`
    FOREIGN KEY (`typ_cat_id_fk` )
    REFERENCES `oliveolives`.`category` (`cat_id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Type de produit dans chacune des catégories';


-- -----------------------------------------------------
-- Table `oliveolives`.`cart`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`cart` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`cart` (
  `crt_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `crt_serial` CHAR(32) NOT NULL COMMENT 'Numéro de série du panier d\'achats.' ,
  `crt_date` DATE NOT NULL COMMENT 'Date de dernière modification de ce panier.' ,
  PRIMARY KEY (`crt_id`) ,
  UNIQUE INDEX `crt_serial` (`crt_serial` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Panier d\'achat d\'un utilisateur du site.';


-- -----------------------------------------------------
-- Table `oliveolives`.`class`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`class` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`class` (
  `cls_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `cls_lang` ENUM('fr','en') NOT NULL DEFAULT 'en' COMMENT 'Langue dans laquelle le cours est donné.' ,
  `cls_location` VARCHAR(255) NOT NULL COMMENT 'Emplacement où aura lieu le cours.' ,
  `cls_desc` MEDIUMTEXT NOT NULL COMMENT 'Description du cours' ,
  `cls_title` VARCHAR(255) NOT NULL COMMENT 'Titre du cours' ,
  `cls_instructor` VARCHAR(25) NOT NULL COMMENT 'Nom du formateur.' ,
  `cls_price` DECIMAL(3,2) NOT NULL COMMENT 'Prix de l\'inscription au cours' ,
  `cls_taxable` TINYINT UNSIGNED NOT NULL DEFAULT 3 COMMENT 'Taxes à appliquer (0:aucune, 1: tps, 2: tvp, 3: tps+tvp).' ,
  `cls_stock` TINYINT UNSIGNED NOT NULL COMMENT 'Nombre totale d\'admissions' ,
  `cls_img` VARCHAR(50) NOT NULL COMMENT 'Nom du fichier image' ,
  `cls_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`cls_id`) ,
  UNIQUE INDEX `cls_title` (`cls_title` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Cours de cuisine.';


-- -----------------------------------------------------
-- Table `oliveolives`.`class_schedule`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`class_schedule` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`class_schedule` (
  `sch_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `sch_date` DATE NOT NULL COMMENT 'Date du cours' ,
  `sch_time` TIME NOT NULL COMMENT 'Heure à laquelle se donne le cours' ,
  `sch_duration` TINYINT UNSIGNED NOT NULL COMMENT 'Durée en heures' ,
  `sch_stock` TINYINT UNSIGNED NOT NULL COMMENT 'Nombre de places restantes' ,
  `sch_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  `sch_cls_id_fk` TINYINT UNSIGNED NOT NULL COMMENT 'Identifiant du cours' ,
  PRIMARY KEY (`sch_id`) ,
  INDEX `sch_cls_id_fk` (`sch_cls_id_fk` ASC) ,
  CONSTRAINT `class_schedule_ibfk_1`
    FOREIGN KEY (`sch_cls_id_fk` )
    REFERENCES `oliveolives`.`class` (`cls_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Horaire d\'un cours.';


-- -----------------------------------------------------
-- Table `oliveolives`.`cart_detail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`cart_detail` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`cart_detail` (
  `crd_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `crd_quantity` TINYINT UNSIGNED NOT NULL DEFAULT 1 ,
  `crd_date` DATE NOT NULL ,
  `crd_crt_id_fk` SMALLINT UNSIGNED NOT NULL ,
  `crd_prd_id_fk` SMALLINT UNSIGNED NOT NULL ,
  `crd_sch_id_fk` TINYINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`crd_id`) ,
  UNIQUE INDEX `crd_crt_id_fk_2` (`crd_crt_id_fk` ASC, `crd_prd_id_fk` ASC) ,
  UNIQUE INDEX `crd_crt_id_fk_3` (`crd_crt_id_fk` ASC, `crd_sch_id_fk` ASC) ,
  INDEX `crd_crt_id_fk` (`crd_crt_id_fk` ASC) ,
  INDEX `crd_prd_id_fk` (`crd_prd_id_fk` ASC) ,
  INDEX `crd_sch_id_fk` (`crd_sch_id_fk` ASC) ,
  CONSTRAINT `cart_detail_ibfk_1`
    FOREIGN KEY (`crd_prd_id_fk` )
    REFERENCES `oliveolives`.`product` (`prd_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `cart_detail_ibfk_2`
    FOREIGN KEY (`crd_sch_id_fk` )
    REFERENCES `oliveolives`.`class_schedule` (`sch_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `cart_detail_ibfk_3`
    FOREIGN KEY (`crd_crt_id_fk` )
    REFERENCES `oliveolives`.`cart` (`crt_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Article dans un panier d\'achats';


-- -----------------------------------------------------
-- Table `oliveolives`.`client`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`client` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`client` (
  `cli_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `cli_firstname` VARCHAR(25) NOT NULL COMMENT 'Prénom' ,
  `cli_lastname` VARCHAR(25) NOT NULL COMMENT 'Nom de famille' ,
  `cli_email` VARCHAR(50) NOT NULL COMMENT 'Adresse de courriel' ,
  `cli_phone` VARCHAR(15) NOT NULL COMMENT 'Numéro de téléphone' ,
  `cli_newsletter_optin` TINYINT UNSIGNED NOT NULL COMMENT 'Indique si le client est inscrit à l\'infolettre.' ,
  `cli_date` DATE NOT NULL COMMENT 'Date de l\'ajout de ce client dans le système.' ,
  PRIMARY KEY (`cli_id`) ,
  UNIQUE INDEX `cli_firstname` (`cli_firstname` ASC, `cli_lastname` ASC, `cli_email` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Client ayant déjà complété une commande.';


-- -----------------------------------------------------
-- Table `oliveolives`.`client_address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`client_address` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`client_address` (
  `adr_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `adr_street` VARCHAR(25) NOT NULL DEFAULT 'NULL' ,
  `adr_city` VARCHAR(25) NOT NULL ,
  `adr_state` VARCHAR(25) NOT NULL ,
  `adr_country` VARCHAR(25) NOT NULL ,
  `adr_zip` VARCHAR(10) NOT NULL ,
  `adr_cli_id_fk` MEDIUMINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`adr_id`) ,
  INDEX `adr_cli_id_fk` (`adr_cli_id_fk` ASC) ,
  CONSTRAINT `client_address_ibfk_1`
    FOREIGN KEY (`adr_cli_id_fk` )
    REFERENCES `oliveolives`.`client` (`cli_id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Adresse associée à un client.';


-- -----------------------------------------------------
-- Table `oliveolives`.`order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`order` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`order` (
  `ord_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ord_paymethod` ENUM('visa','mc','pp') NOT NULL DEFAULT 'visa' COMMENT 'Méthode de paiement : Visa, MasterCard ou Paypal' ,
  `ord_pptransactionid` VARCHAR(30) NOT NULL COMMENT 'Code de transaction Paypal' ,
  `ord_ppavs` TINYINT UNSIGNED NOT NULL COMMENT 'Code de réponse de sécurité AVS de Paypal' ,
  `ord_ppcvv2` CHAR(1) NOT NULL COMMENT 'Code de réponse de sécurité CVV2 de Paypal' ,
  `ord_subtotal` DECIMAL(6,2) UNSIGNED NOT NULL COMMENT 'Sous-total de la commande avant taxes et frais d\'envoi' ,
  `ord_shippingfees` DECIMAL(6,2) UNSIGNED NOT NULL COMMENT 'Frais d\'envoi' ,
  `ord_taxes` DECIMAL(6,2) UNSIGNED NOT NULL COMMENT 'Montant des taxes' ,
  `ord_giftwrap` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Emballage cadeau ou non.' ,
  `ord_giftmsg` MEDIUMTEXT NOT NULL COMMENT 'Message à ajouter au coli-cadeau' ,
  `ord_status` ENUM('shipped','paid','cancelled','pending') NOT NULL DEFAULT 'paid' COMMENT 'Statut de la commande s\'il y a lieu' ,
  `ord_date` DATE NOT NULL COMMENT 'Date de la commande' ,
  `ord_cli_id_fk` MEDIUMINT UNSIGNED NOT NULL COMMENT 'Identifiant du client' ,
  `ord_shipping_adr_id_fk` MEDIUMINT UNSIGNED NOT NULL COMMENT 'Identifiant de l\'adresse de livraison' ,
  `ord_billing_adr_id_fk` MEDIUMINT UNSIGNED NOT NULL COMMENT 'Identifiant de l\'adresse de facturation' ,
  PRIMARY KEY (`ord_id`) ,
  INDEX `ord_cli_id_fk` (`ord_cli_id_fk` ASC) ,
  INDEX `ord_shipping_adr_id_fk` (`ord_shipping_adr_id_fk` ASC) ,
  INDEX `ord_billing_adr_id_fk` (`ord_billing_adr_id_fk` ASC) ,
  CONSTRAINT `order_ibfk_1`
    FOREIGN KEY (`ord_cli_id_fk` )
    REFERENCES `oliveolives`.`client` (`cli_id` ),
  CONSTRAINT `order_ibfk_2`
    FOREIGN KEY (`ord_shipping_adr_id_fk` )
    REFERENCES `oliveolives`.`client_address` (`adr_id` ),
  CONSTRAINT `order_ibfk_3`
    FOREIGN KEY (`ord_billing_adr_id_fk` )
    REFERENCES `oliveolives`.`client_address` (`adr_id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Commandes payées';


-- -----------------------------------------------------
-- Table `oliveolives`.`order_detail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`order_detail` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`order_detail` (
  `odd_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `odd_quantity` TINYINT UNSIGNED NOT NULL COMMENT 'Quantité de cet article' ,
  `odd_price` DECIMAL(6,2) UNSIGNED NOT NULL COMMENT 'Prix de l\'article dans la commande' ,
  `odd_ord_id_fk` MEDIUMINT UNSIGNED NOT NULL COMMENT 'Identifiant de la commande' ,
  `odd_prd_id_fk` SMALLINT UNSIGNED NOT NULL COMMENT 'Identifiant du produit' ,
  `odd_sch_id_fk` TINYINT UNSIGNED NOT NULL COMMENT 'Identifiant de l\'horaire du cours le cas échéant' ,
  PRIMARY KEY (`odd_id`) ,
  UNIQUE INDEX `odd_ord_id_fk_2` (`odd_ord_id_fk` ASC, `odd_prd_id_fk` ASC) ,
  UNIQUE INDEX `odd_ord_id_fk_3` (`odd_ord_id_fk` ASC, `odd_sch_id_fk` ASC) ,
  INDEX `odd_ord_id_fk` (`odd_ord_id_fk` ASC) ,
  INDEX `odd_prd_id_fk` (`odd_prd_id_fk` ASC) ,
  INDEX `odd_sch_id_fk` (`odd_sch_id_fk` ASC) ,
  CONSTRAINT `order_detail_ibfk_1`
    FOREIGN KEY (`odd_ord_id_fk` )
    REFERENCES `oliveolives`.`order` (`ord_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `order_detail_ibfk_2`
    FOREIGN KEY (`odd_prd_id_fk` )
    REFERENCES `oliveolives`.`product` (`prd_id` ),
  CONSTRAINT `order_detail_ibfk_3`
    FOREIGN KEY (`odd_sch_id_fk` )
    REFERENCES `oliveolives`.`class_schedule` (`sch_id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Article dans une commande.';


-- -----------------------------------------------------
-- Table `oliveolives`.`slide`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`slide` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`slide` (
  `sli_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `sli_img` VARCHAR(50) NOT NULL COMMENT 'Nom du fichier image' ,
  `sli_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'Diapo active ou non.' ,
  `sli_sortorder` TINYINT UNSIGNED NOT NULL COMMENT 'Ordre de présentation de cette diapo dans le diaporama' ,
  PRIMARY KEY (`sli_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Une diapo dans le diaporama de la page d\'accueil.';


-- -----------------------------------------------------
-- Table `oliveolives`.`slide_detail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`slide_detail` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`slide_detail` (
  `sld_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `sld_lang` ENUM('fr','en') NOT NULL COMMENT 'Langue de cettte inscription' ,
  `sld_title` VARCHAR(255) NOT NULL COMMENT 'Titre de la diapo' ,
  `sld_desc` VARCHAR(255) NOT NULL COMMENT 'Description de la diapo' ,
  `sld_link` VARCHAR(255) NOT NULL COMMENT 'Lien URL de la diapo s\'il y a lieu' ,
  `sld_sli_id_fk` SMALLINT UNSIGNED NOT NULL COMMENT 'Identifiant de la diapo' ,
  PRIMARY KEY (`sld_id`) ,
  INDEX `sld_sli_id_fk` (`sld_sli_id_fk` ASC) ,
  CONSTRAINT `slide_detail_ibfk_1`
    FOREIGN KEY (`sld_sli_id_fk` )
    REFERENCES `oliveolives`.`slide` (`sli_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Textes associés à une diapo';


-- -----------------------------------------------------
-- Table `oliveolives`.`recipe`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`recipe` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`recipe` (
  `rcp_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `rcp_product_links` VARCHAR(50) NOT NULL COMMENT 'Liste de produits identifiés par SKU, séparés par des virgules. Exemple: 582,398,255, ' ,
  `rcp_img_small` VARCHAR(50) NOT NULL COMMENT 'Nom du fichier image - petit format' ,
  `rcp_img_large` VARCHAR(50) NOT NULL COMMENT 'Nom du fichier image - grand format' ,
  `rcp_date` DATE NOT NULL COMMENT 'Date de l\'ajout de cette recette dans le système.' ,
  `rcp_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`rcp_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Recette avec liens vers SKU de produits';


-- -----------------------------------------------------
-- Table `oliveolives`.`recipe_detail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`recipe_detail` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`recipe_detail` (
  `rcd_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `rcd_lang` ENUM('fr','en') NOT NULL COMMENT 'Langue de cette inscription' ,
  `rcd_title` VARCHAR(255) NOT NULL COMMENT 'Titre de la recette' ,
  `rcd_ingredients` MEDIUMTEXT NOT NULL COMMENT 'Liste d\'ingrédients' ,
  `rcd_preparation` MEDIUMTEXT NOT NULL COMMENT 'Étapes de la préparation' ,
  `rcd_keywords` VARCHAR(255) NOT NULL COMMENT 'Liste de mots-clés associés à cette recette dans la langue courante.' ,
  `rcd_slug` VARCHAR(255) NOT NULL COMMENT 'URL de référencement pour la page de ctte recette.' ,
  `rcd_rcp_id_fk` SMALLINT UNSIGNED NOT NULL COMMENT 'Identifiant de la recette.' ,
  PRIMARY KEY (`rcd_id`) ,
  INDEX `rcd_rcp_id_fk` (`rcd_rcp_id_fk` ASC) ,
  CONSTRAINT `recipe_detail_ibfk_1`
    FOREIGN KEY (`rcd_rcp_id_fk` )
    REFERENCES `oliveolives`.`recipe` (`rcp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Textes associés à une recette';


-- -----------------------------------------------------
-- Table `oliveolives`.`cmsuser`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oliveolives`.`cmsuser` ;

CREATE  TABLE IF NOT EXISTS `oliveolives`.`cmsuser` (
  `usr_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `usr_fullname` VARCHAR(50) NOT NULL COMMENT 'Nom complet de l\'utilisateur' ,
  `usr_uname` VARCHAR(25) NOT NULL COMMENT 'Code d\'utilisateur pour la connexion' ,
  `usr_pwd` CHAR(128) NOT NULL COMMENT 'Mot de passe de l\'utilisateur' ,
  `usr_salt` CHAR(150) NOT NULL COMMENT 'Chaîne produite aléatoirement pour augmenter la sécurité du mot de passe.' ,
  `usr_email` VARCHAR(50) NOT NULL COMMENT 'Adresse de courriel de l\'utilisateur' ,
  `usr_disabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Pour désactiver rapidement un utilisateur sans supprimer l\'inscription (0:utilisateur actif, 1:utilisateur désactivé)' ,
  PRIMARY KEY (`usr_id`) ,
  UNIQUE INDEX `usr_uname` (`usr_uname` ASC) ,
  UNIQUE INDEX `usr_pwd` (`usr_pwd` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Utilisateur du gestionnaire de contenu';

USE `oliveolives` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
