<?php 
//define('CONFIG_FILE_PATH',"/var/www/vhosts/g8qd-lzlw.accessdomain.com/private/oliveolives.conf.php");
define('CONFIG_FILE_PATH',"/var/www/vhosts/oliveolives.com/private/oliveolives.conf.php");

session_start();
require_once(CONFIG_FILE_PATH);
$router = new Router(isset($_GET["url"])?$_GET["url"]:"");
$router->route();

class Router { 
    private $url;
    
	function __construct($url) {
	    $this->url = $url;
	    spl_autoload_register(array($this,'classAutoload'));
	}
	
	public function route() {
		global $urlNodes;
	    // We assume the following URL convention:
	    // lan/catalogue/category/type/param1/param2/.../paramN
		$urlArray = explode("/",trim($this->url,"/"));
		
		$lan = Util::getLan();
		$module = DEFAULT_MODULE;
		$action = DEFAULT_ACTION;
		if(isset($urlArray[0]) && strlen($urlArray[0])==2) {
			$lan = $urlArray[0];
			if($lan != Util::getLan()) {
				if(Util::setLan($lan)) {
					array_shift($urlArray);
				}
				else{
					self::handleURLError('fr');
				}
			} 
			else {
				array_shift($urlArray);
			}
		}
		else {
			setcookie("oolandedredirect","true");
			header("Location:".BASE_URL."$lan/".implode('/', $urlArray));
		}

		if(isset($urlArray[0]) && $urlArray[0]!="") {
			// Module name may be given in FR.
			$module = ($modEn = array_search($urlArray[0], $urlNodes))?$modEn:$urlArray[0];
			array_shift($urlArray);
		}

		$controllerName = ucfirst($module)."Controller";

		if(class_exists($controllerName)) {
			$modelName = ucfirst($module)."Model";
			if(method_exists($controllerName,$action)) {
				$controllerInstance = new $controllerName($modelName,$module,$action,$lan,$_SERVER['REQUEST_URI']);
				call_user_func_array(array($controllerInstance,$action),array($urlArray));
			}
			else {
				self::handleURLError($lan);
			}
		}
		else {
			self::handleURLError($lan);
		}
	}
	
	static private function classNameToFileName($className) {
		return strtolower(preg_replace("/(.)([A-Z])/","$1_$2",$className));
	}

	private static function handleURLError($lan) {
		$e = new ErrorController('ErrorModel','error','_404',$lan,$_SERVER['REQUEST_URI']);
		call_user_func_array(array($e,"_404"),array());
	}
	
	private function classAutoload($className) {
		$fn = Router::classNameToFileName($className);
		if(file_exists("webapp/controllers/".$fn.".class.php"))
			require_once("webapp/controllers/".$fn.".class.php");
		else if(file_exists("webapp/models/".$fn.".class.php"))
			require_once("webapp/models/".$fn.".class.php");
		else if(file_exists("webapp/lib/".$fn.".class.php"))
			require_once("webapp/lib/".$fn.".class.php");
		else if(file_exists("webapp/templates/".$fn.".class.php"))
			require_once("webapp/templates/".$fn.".class.php");	
	}
}
?>