<?php
        $counter = 1;
        if($recipes):
                foreach ($recipes as $recipe):
?>
                        <a class="cs_recipeLink" href="<?=Util::baseUrl('recipe/'.$recipe['rcp_id'].'/'.$recipe['rcd_slug'],$lan)?>">
                                <div class="recipe_block<?=($counter%4==0)?' last_element':''?>">
                                        <img src="data/images/recipe/<?=$recipe['rcp_img_small']?>" height="148" width="148" alt="" class="postcard">
                                        <p><?=$recipe['rcd_title']?></p>
                                </div>
                        </a>
<?php 
                        $counter++;
                endforeach;
        else:
                echo $rcp_emptyset;
        endif;
?>