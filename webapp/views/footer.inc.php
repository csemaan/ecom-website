	<div class="full_width_container hp_footer_header clearfix">
		<div class="content_container newsletter">
			<div class="hp_feature postcard newsletter_height_fix">
				<h2><?=$foo_subscribe_newsletter?></h1>
				<form class="cs_newsletter_form" novalidate data-invalid-msg="<?=$foo_nlinput_pattern_error?>">
					<input type="text" name="newsletter" width="100%" class="cs_placeholder" data-placeholder='<?=$foo_nlinput_placeholder?>'  maxlength="50" required pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2,10})\b">
					<img src="assets/images/btn_submit_arrow.gif" height="28" width="28" alt="" class="newsletter_button cs_newsletter_btn">
					<div class="ajaxloader"><img src="assets/images/loading.gif"/></div>
				</form>
			</div>
			<div class="hp_feature postcard newsletter_height_fix last_element">
				<h2>
					<?=$com_free_delivery?>
					<a href="<?=Util::baseUrl('policies/3/shipping',$lan)?>"><?=$foo_delivery_info?> <img src="assets/images/green_arrow.gif" height="11" width="10" alt="" class="link_arrow"></a>
				</h2>
			</div>				
		</div>
	</div>		
	<footer class="clearfix">
		<div class="content_container">
			<h4><b style="color:#95b823"><?=$foo_moto_oo ?></b><?=$foo_moto ?></h4>
			<hr/>
			<table class="footer_nav">
				<td nowrap>
					<a href="javascript:this.preventDefault();"><b><?=$foo_contact_phone_label." ".INFO_PHONE?></b></a><br>
					<a href="mailto:<?=INFO_EMAIL?>"><b><?=$foo_contact_email_label." ".INFO_EMAIL?></b></a><br>
					<a href="https://www.facebook.com/olive.olives" target="_blank"><img src="assets/images/footer_facebook.gif" height="18" width="18" alt="Facebook logo"><b><?=$foo_contact_fb?></b></a><br>
					<img src="assets/images/footer_paypal.gif" alt="PayPal logo">
					<img src="assets/images/footer_visa.gif" alt="Visa logo">
					<img src="assets/images/footer_mastercard.gif" alt="Masterard Logo">
				</td>
				<td nowrap>
					<a href="<?=Util::baseUrl('products',$lan)?>"><?=$hea_menu_product?></a><br>
					<a href="<?=Util::baseUrl('recipes',$lan)?>"><?=$hea_menu_recipe?></a><br>
					<!-- Classes/Ateliers : removed at client request Dec 2015 -->
					<!-- <a href="<?=Util::baseUrl('classes',$lan)?>"><?=$hea_menu_class?></a><br> -->
					<a href="<?=Util::baseUrl('stores',$lan)?>"><?=$hea_menu_address?></a><br>
					<a href="<?=Util::baseUrl('about',$lan)?>"><?=$hea_menu_about?></a>						
				</td>
				<?php foreach ($allCatsAndTypes as $catType): ?>
					<td nowrap>
						<a href="<?=Util::baseUrl('products/'.$catType['catinfo']['cat_id'].'/'.Util::slugify($catType['catinfo']['cat_name_'.$lan]).'/',$lan)?>"><b><?=($catType['catinfo']['cat_name_'.$lan])?></b></a><br>
						<?php foreach ($catType['types'] as $type): ?>
							<a href="<?=Util::baseUrl('products/'.$catType['catinfo']['cat_id'].'/'.Util::slugify($catType['catinfo']['cat_name_'.$lan]).'/'.$type['typ_id'].'/'.Util::slugify($type['typ_name_'.$lan]).'/',$lan)?>"><?=$type['typ_name_'.$lan]?></a><br>
						<?php endforeach; ?>
					</td>
				<?php endforeach; ?>
			</table>
			<p class="footer_links"><?=$foo_copyright?>
				<a href="<?=Util::baseUrl('policies/0/privacy',$lan)?>" class="footerlinks"><?=$foo_privacy?></a>
				|
				<a href="<?=Util::baseUrl('policies/1/use',$lan)?>"><?=$foo_usepolicy?></a>
				|
				<a href="<?=Util::baseUrl('policies/2/cancellation',$lan)?>"><?=$foo_cancellationpolicy?></a>
				|
				<a href="<?=Util::baseUrl('policies/3/shipping',$lan)?>"><?=$foo_delivery_info?></a>
			</p>
		</div>				
	</footer>
	<div id="back_to_top">
		<a href="#" id="top-link"><?=$foo_totop?></a>     
	</div>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/js/lib/jquery-1.10.1.min.js">\x3C/script>')</script>
	<script src="assets/js/lib/bootstrap.min.js"></script>
	<script src="assets/js/oomain.js"></script>
</body>
</html>