<div class="secondary_nav">
	<h2><img src="assets/images/truck.png" height="29" width="62" alt=""><?=$crt_catchphrase?></h2>	
</div>
<div class="content_container clearfix">
	<div class="cs_sc_step1">
		<div id="shopping_cart" class="clearfix">
			<h1><?=$crt_cart_title?></h1>
			<h2></h2>
			<form novalidate name="frm_cart" method="post" action="<?=Util::baseUrl('cart/',$lan)?>" data-invalid-msg="<?=$com_qty_pattern_error?>" data-invalid-maxvalue-msg="<?=$com_qty_stock_error?>">
				<input type='hidden' name='action' value='update'/>
				<table class="cart_table">
					<tr class="top">
						<td nowrap><img src="assets/images/trash.png" height="16" width="13" alt="X" title="<?=$crt_lbl_remove?>"></td>
						<td class="centre"><?=$crt_lbl_item?></td>
						<td>&nbsp;</td>
						<td class="centre"><?=$crt_lbl_price?></td>
						<td class="centre"><?=$crt_lbl_qty?></td>
						<td class="right"><?=$crt_lbl_subtotal?></td>
					</tr>
					<?php if (count($cartContent)>0): ?>
					<?php 
						$checkoutTmpl = "";
						if (isset($cartContent['product'])):
							$totalProductsWeight = 0;
							foreach ($cartContent['product'] as $product):
								$totalProductsWeight += ($product['weight']/1000);
								$checkoutTmpl .='<tr class="row">
													<td class="product">'.$product['pdd_name'].'</td>
													<td class="quantity">'.$product['crd_quantity'].'</td>
													<td class="price">'.Util::formatPrice($product['prd_price']*$product['crd_quantity'],$lan).'</td>
												</tr>';
					?>
								<tr>
									<td class="remove"><input type="checkbox" name="remove[]" value="<?=$product['crd_id']?>" title="<?=$crt_lbl_remove?>"></td>
									<td class="image centre">
										<img src="data/images/product/<?=$product['prd_img_small']?>" alt="">	
									</td>
									<td>
										<h1><?=$product['pdd_name']?></h1>
										<p><?=$product['pdd_format']?></p>
									</td>
									<td class="price centre"><?=Util::formatPrice($product['prd_price'],$lan)?></td>
									<td class="centre">
										<input type="hidden" name="crd_id[]" value="<?=$product['crd_id']?>">
										<span class="cs_posAnchor"><input autocomplete='off' name="crd_quantity[]" type="text" value="<?=$product['crd_quantity']?>" class="quantity" required pattern="(0|[1-9][0-9]?)" maxlength="2" maxvalue="<?=$product['prd_stock']?>"></span>
									</td>
									<td class="price subtotal centre"><?=Util::formatPrice($product['prd_price']*$product['crd_quantity'],$lan)?></td>
								</tr>
					<?php 
							endforeach;
						endif;
					?>
					<?php 
						if (isset($cartContent['class'])):
							foreach ($cartContent['class'] as $class):
								$checkoutTmpl .='<tr class="row">
													<td class="product">'.$class['cls_title'].'</td>
													<td class="quantity">'.$class['crd_quantity'].'</td>
													<td class="price">'.Util::formatPrice($class['cls_price']*$class['crd_quantity'],$lan).'</td>
												</tr>';
					?>
								<tr>
									<td class="remove"><input type="checkbox" name="remove[]" value="<?=$class['crd_id']?>" title="<?=$crt_lbl_remove?>"></td>
									<td class="image centre">
										<img src="data/images/class/<?=$class['cls_img_course']?>" alt="">	
									</td>
									<td>
										<h1><?=$class['cls_title']?></h1>
										<p><?=Util::makeDate($class['sch_date'],$lan)?></p>
									</td>
									<td class="price centre"><?=Util::formatPrice($class['cls_price'],$lan)?></td>
									<td class="centre">
										<input type="hidden" name="crd_id[]" value="<?=$class['crd_id']?>">
										<span class="cs_posAnchor"><input name="crd_quantity[]" type="text" value="<?=$class['crd_quantity']?>" class="quantity" required pattern="(0|[1-9][0-9]?)" maxlength="2" maxvalue="<?=$class['sch_stock']?>"></span>
									</td>
									<td class="price subtotal centre"><?=Util::formatPrice($class['cls_price']*$class['crd_quantity'],$lan)?></td>
								</tr>
					<?php 
							endforeach;
						endif;
					?>
					<?php else: ?>	
						<tr style="height:80px">
							<td colspan="6"><?=$crt_empty_msg;?></td>
						</tr>
					<?php endif; ?>	
					<tr class="button_row last_row">
						<td colspan="6">
							<a href="<?=(isset($currentProductsRoute))?$currentProductsRoute:Util::baseUrl('products',$lan)?>" class="pr_button"><?=$crt_continue_btn?></a>
							<a href="javascript:void(0);" class="pr_button last_element cs_cartupdatebtn"><?=$crt_update_btn?></a>
						</td>
					</tr>
				</table>
			</form>
		</div>
		<div id="cart_total">
			<div class="header clearfix">
				<span class="sub_text"><?=$crt_lbl_subtotal?> <span style="color:#95b823">(CAD)</span></span>
				<span class="sub_price"><?=Util::formatPrice($cartSummary['product'][0]['subtotal']+$cartSummary['class'][0]['subtotal'],$lan)?></span>
			</div>
			<p><?=$crt_dontworry?></p>
			<a href="javascript:void(0)" class="proceed_to_checkout cs_proceedToCheckout<?php if (count($cartContent)==0) {echo " btn_off";}?>"><?=$crt_checkout_btn?></a>	
		</div>
	</div>
	<!-- Step 2 -->
	<div class="cs_sc_step2">
		<div id="shopping_cart" class="clearfix cs_sc_step2">
			<h1><?=$crt_checkout_title?></h1>
			<h2></h2>
			<form class="checkout_form" name="frm_checkout" novalidate>
				<input type='hidden' name='totalProductsWeight' value='<?=$totalProductsWeight?>'>
				<input type='hidden' name='subtotalProductsAmount' value='<?=$cartSummary['product'][0]['subtotal']?>'>
				<fieldset class="cs_checkoutFormZone">
			        <legend><?=$crt_adr_billing?></legend>
			        <div class="cs_billingAddressForm">
				        <div class="feild_row clearfix">
				            <div class="feild_block" errormsg="<?=$crt_valid_generic?>">
				                <label class="required" for="first-name"><?=$crt_frm_fname?></label>
				                <input value="" id="first-name" name="bill_fname" type="text" class="half" maxlength="25" required pattern=".+">
				            </div>
				            <div class="feild_block" errormsg="<?=$crt_valid_generic?>">
				                <label class="required" for="last-name"><?=$crt_frm_lname?></label>
				                <input value="" id="last-name" name="bill_lname" type="text" class="half" maxlength="25" required pattern=".+">
				            </div>
						</div>		 
				        <div class="feild_row clearfix">				  
				            <div class="feild_block" errormsg="<?=$crt_valid_email?>">
				                <label class="required" for="email"><?=$crt_frm_email?></label>
				                <input value="" id="email" name="bill_email" type="email" class="half" maxlength="50" required pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2,10})\b">
				            </div>
				            <div class="feild_block" errormsg="<?=$crt_valid_tel?>">
				                <label class="required" for="telephone"><?=$crt_frm_tel?></label>
				                <input value="" id="telephone" name="bill_tel" type="tel" class="quarter" maxlength="10" required pattern="\d{10}">
				            </div>
				        </div>
				        <div class="feild_row newsletter">	
				            <div class="feild_block">
								<input id="newsletter" name="bill_newsletter" type="checkbox" value="1" />
								<label for="newsletter"><?=$crt_frm_newsletter?></label> 
				            </div>
				        </div>
				        <div class="feild_row clearfix">				  
				            <div class="feild_block" errormsg="<?=$crt_valid_generic?>">
				                <label class="required"><?=$crt_frm_street?></label>
				                <input value="" id="address_1" name="bill_street1" type="text" class="full" maxlength="100" required pattern=".+">
				                <input id="address_2" name="bill_street2" type="text" class="full" maxlength="100">
				                <br><!-- FF Fix for validation error message positionning - Leave this BR here -->
				            </div>
				        </div>
				        <div class="feild_row clearfix">				  
				            <div class="feild_block" errormsg="<?=$crt_valid_generic?>">
				                <label class="required" for="city"><?=$crt_frm_city?></label>
				                <input value="" id="city" name="bill_city" type="text" class="city" maxlength="50" required pattern=".+">
				            </div>
				            <div class="feild_block">
				                <label class="required" for="country"><?=$crt_frm_country?></label>
								<select id="country" name="bill_country" class="cs_countryChoice">
									<option value="ca"><?=$crt_country_ca?></option>
									<option value="us"><?=$crt_country_us?></option>
								</select>	
				            </div>
				        </div>
				        <div class="feild_row clearfix cs_statesBlock cs_statesBlock_ca">
				            <div class="feild_block" errormsg="<?=$crt_valid_province?>">
				                <label class="required" for="province"><?=$crt_frm_province?></label>
				                <select name="bill_province" required pattern="[a-zA-Z]{2}">
									<option value=""><?=$crt_frm_select?></option>
									<option value="ab"><?=$crt_prov_ab?></option>
									<option value="bc"><?=$crt_prov_bc?></option>
									<option value="mb"><?=$crt_prov_mb?></option>
									<option value="nb"><?=$crt_prov_nb?></option>
									<option value="nl"><?=$crt_prov_nl?></option>
									<option value="ns"><?=$crt_prov_ns?></option>
									<option value="on"><?=$crt_prov_on?></option>
									<option value="pe"><?=$crt_prov_pe?></option>
									<option value="qc"><?=$crt_prov_qc?></option>
									<option value="sk"><?=$crt_prov_sk?></option>
									<option value="nt"><?=$crt_prov_nt?></option>
									<option value="nu"><?=$crt_prov_nu?></option>
									<option value="yt"><?=$crt_prov_yt?></option>
								</select>	
				            </div>
				            <div class="feild_block" errormsg="<?=$crt_valid_postalcode?>">
				                <label class="required" for="postalcode"><?=$crt_frm_postalcode?></label>
				                <input value="" id="postalcode" name="bill_postalcode" type="text" class="postal_code" maxlength="10" required pattern="\s*[a-zA-Z]\d[a-zA-Z]\s*\d[a-zA-Z]\d\s*">
				            </div>		            		            
				        </div>
				        <div class="feild_row clearfix  cs_statesBlock cs_statesBlock_us hide">				  
				            <div class="feild_block" errormsg="<?=$crt_valid_state?>">
			                	<label class="required" for="state"><?=$crt_frm_state?></label>
								<select name="bill_state" id="state" pattern="[a-zA-Z]{2}">
									<option value=""><?=$crt_frm_select?></option>
									<option value="al"><?=$crt_state_al?></option>
									<option value="ak"><?=$crt_state_ak?></option>
									<option value="az"><?=$crt_state_az?></option>
									<option value="ar"><?=$crt_state_ar?></option>
									<option value="ca"><?=$crt_state_ca?></option>
									<option value="co"><?=$crt_state_co?></option>
									<option value="ct"><?=$crt_state_ct?></option>
									<option value="de"><?=$crt_state_de?></option>
									<option value="dc"><?=$crt_state_dc?></option>
									<option value="fl"><?=$crt_state_fl?></option>
									<option value="ga"><?=$crt_state_ga?></option>
									<option value="hi"><?=$crt_state_hi?></option>
									<option value="id"><?=$crt_state_id?></option>
									<option value="il"><?=$crt_state_il?></option>
									<option value="in"><?=$crt_state_in?></option>
									<option value="ia"><?=$crt_state_ia?></option>
									<option value="ks"><?=$crt_state_ks?></option>
									<option value="ky"><?=$crt_state_ky?></option>
									<option value="la"><?=$crt_state_la?></option>
									<option value="me"><?=$crt_state_me?></option>
									<option value="md"><?=$crt_state_md?></option>
									<option value="ma"><?=$crt_state_ma?></option>
									<option value="mi"><?=$crt_state_mi?></option>
									<option value="mn"><?=$crt_state_mn?></option>
									<option value="ms"><?=$crt_state_ms?></option>
									<option value="mo"><?=$crt_state_mo?></option>
									<option value="mt"><?=$crt_state_mt?></option>
									<option value="ne"><?=$crt_state_ne?></option>
									<option value="nv"><?=$crt_state_nv?></option>
									<option value="nh"><?=$crt_state_nh?></option>
									<option value="nj"><?=$crt_state_nj?></option>
									<option value="nm"><?=$crt_state_nm?></option>
									<option value="ny"><?=$crt_state_ny?></option>
									<option value="nc"><?=$crt_state_nc?></option>
									<option value="nd"><?=$crt_state_nd?></option>
									<option value="oh"><?=$crt_state_oh?></option>
									<option value="ok"><?=$crt_state_ok?></option>
									<option value="or"><?=$crt_state_or?></option>
									<option value="pa"><?=$crt_state_pa?></option>
									<option value="ri"><?=$crt_state_ri?></option>
									<option value="sc"><?=$crt_state_sc?></option>
									<option value="sd"><?=$crt_state_sd?></option>
									<option value="tn"><?=$crt_state_tn?></option>
									<option value="tx"><?=$crt_state_tx?></option>
									<option value="ut"><?=$crt_state_ut?></option>
									<option value="vt"><?=$crt_state_vt?></option>
									<option value="va"><?=$crt_state_va?></option>
									<option value="wa"><?=$crt_state_wa?></option>
									<option value="wv"><?=$crt_state_wv?></option>
									<option value="wi"><?=$crt_state_wi?></option>
									<option value="wy"><?=$crt_state_wy?></option>
								</select>
							</div>
				            <div class="feild_block" errormsg="<?=$crt_valid_zip?>">
				                <label class="required" for="zipcode"><?=$crt_frm_zip?></label>
				                <input id="zipcode" type="text" name="bill_zip" class="postal_code" maxlength="5" pattern="\d{5}">
				            </div>
				        </div>
				    </div>
					<legend class"shipping_label"><?=$crt_adr_shipping?><p class="shipping_note">*<?=$crt_note_shipping?></p></legend>
					<div class="feild_row clearfix">
			            <div class="feild_block cs_sameBillShipBlock" warningmsg="<?=$crt_shipping_notavailable?>">
			                <input class="cs_sameBillShip" id="same" type="radio" name="shipping" value="same">
			                <label for="same" class="radio_label"><?=$ship_same_bill_lbl?></label>
			            </div>
			            <div class="feild_block">
			                <input class="cs_diffBillShip" id="different" type="radio" name="shipping" value="different">
			                <label for="different" class="radio_label"><?=$ship_diff_bill_lbl?></label>
			            </div>
					</div>
					<div class="cs_shippingAddressForm">
						<div class="feild_row clearfix">
				            <div class="feild_block" errormsg="<?=$crt_valid_generic?>">
				                <label class="required" for="first-name"><?=$crt_frm_fname?></label>
				                <input id="first-name" name="ship_fname" type="text" class="half" maxlength="25" required pattern=".+">
				            </div>
				            <div class="feild_block" errormsg="<?=$crt_valid_generic?>">
				                <label class="required" for="last-name"><?=$crt_frm_lname?></label>
				                <input id="last-name" name="ship_lname" type="text" class="half" maxlength="25" required pattern=".+">
				            </div>
						</div>		 
				        <div class="feild_row clearfix">				  
				            <div class="feild_block" errormsg="<?=$crt_valid_email?>">
				                <label class="required" for="email"><?=$crt_frm_email?></label>
				                <input id="email" name="ship_email" type="email" class="half" maxlength="50" required pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2,10})\b">
				            </div>
				            <div class="feild_block" errormsg="<?=$crt_valid_tel?>">
				                <label class="required" for="telephone"><?=$crt_frm_tel?></label>
				                <input id="telephone" name="ship_tel" type="text" class="quarter" maxlength="10" required pattern="\d{10}">
				            </div>
				        </div>
				        <div class="feild_row clearfix">				  
				            <div class="feild_block" errormsg="<?=$crt_valid_generic?>">
				                <label class="required" for="address_1"><?=$crt_frm_street?></label>
				                <input id="address_1" name="ship_street1" type="text" class="full" maxlength="100" required pattern=".+">
				                <input id="address_2" name="ship_street2" type="text" class="full" maxlength="100">
				                <br><!-- FF Fix for validation error message positionning - Leave this BR here -->                
				            </div>
				        </div>
				        <div class="feild_row clearfix">				  
				            <div class="feild_block" errormsg="<?=$crt_valid_generic?>">
				                <label class="required" for="city"><?=$crt_frm_city?></label>
				                <input id="city" name="ship_city" type="text" class="city" maxlength="50" required pattern=".+">
				            </div>
				            <div class="feild_block" errormsg="<?=$crt_valid_province?>">
				                <label class="required" for="province"><?=$crt_frm_province?></label>
				                <select name="ship_province" required pattern="[a-zA-Z]{2}">
									<option value=""><?=$crt_frm_select?></option>
									<option value="ab"><?=$crt_prov_ab?></option>
									<option value="bc"><?=$crt_prov_bc?></option>
									<option value="mb"><?=$crt_prov_mb?></option>
									<option value="nb"><?=$crt_prov_nb?></option>
									<option value="nl"><?=$crt_prov_nl?></option>
									<option value="ns"><?=$crt_prov_ns?></option>
									<option value="on"><?=$crt_prov_on?></option>
									<option value="pe"><?=$crt_prov_pe?></option>
									<option value="qc"><?=$crt_prov_qc?></option>
									<option value="sk"><?=$crt_prov_sk?></option>
									<option value="nt"><?=$crt_prov_nt?></option>
									<option value="nu"><?=$crt_prov_nu?></option>
									<option value="yt"><?=$crt_prov_yt?></option>
								</select>	
				            </div>
				            <div class="feild_block" errormsg="<?=$crt_valid_postalcode?>">
				                <label class="required" for="postalcode"><?=$crt_frm_postalcode?></label>
				                <input id="postalcode" name="ship_postalcode" type="text" class="postalcode"  maxlength="10" required pattern="\s*[a-zA-Z]\d[a-zA-Z]\s*\d[a-zA-Z]\d\s*">
				            </div>		            		            
			        	</div>
			        </div>
			        <legend><?=$crt_confirm_header?></legend>
					<p><?=$crt_confirm_msg?></p>
					<a href="javascript:void(0)" style="color:white" class="proceed_to_checkout confirmation btn_off cs_shippingbilling_btn" title="<?=$crt_shipbill_btn_msg?>"><?=$crt_shipbill_btn?><div class="ajaxloader"><img src="assets/images/loading.gif"/></div></a>
					<div class="cs_frmInvalidMsg"><?=$crt_confirm_valid_error_msg?></div>
					<div class="cs_canadaPostProblemMsg"><?=$crt_confirm_temp_problem_error_msg?></div>
			    </fieldset>
				<table class="cart_table">
					<!-- Gift Message-->								
					<tr class="gift_msg">
						<td colspan="6" class="centre">
							<div class="gift_heading">
								<img src="assets/images/green_gift_box.png" height="30" width="28" alt="">
								<h3><?=$crt_giftmsg?></h3>
							</div>
							<textarea name="gift_message" maxlength="500" pattern="(.|\s){0,500}" data-donotdisable="true"></textarea>
						</td>
					</tr>
				</table>
			</form>
		</div>	
		<div id="cart_total">
			<table class="receipt cs_receiptColumnProductList">
				<tr id="header">
					<td class="product"><?=$crt_lbl_item?></td>
					<td class="quantity"><?=$crt_lbl_qty?></td>
					<td class="price"><?=$crt_lbl_price?></td>
				</tr>
				<?=(isset($checkoutTmpl))?$checkoutTmpl:""?>
			</table>
			<table class="receipt">							
				<tr class="row">
					<td class="rec_label"><?=$crt_lbl_subtotal?></td>
					<td class="sub"><?=Util::formatPrice($cartSummary['product'][0]['subtotal']+$cartSummary['class'][0]['subtotal'],$lan)?></td>
				</tr>
				<tr class="row">
					<td class="rec_label"><?=$crt_lbl_shipping?></td>
					<td class="shipping cs_shippingAmount cs_tbc_label"><?=$crt_msg_complete?></td>
				</tr>
				<!--
				<tr class="row hide cs_updateAddressBtnRow">
					<td class="update" colspan="2"><a href="javascript:void(0)" class="cs_updateAddressBtn"><?=$crt_update_addresses?></a></td>
				</tr>
				-->				
				<tr class="row taxes">
					<td class="rec_label"><?=$crt_lbl_taxes?></td>
					<td class="tax cs_taxesAmount cs_tbc_label"><?=$crt_msg_complete?></td>
				</tr>	
				<tr class="row">
					<td class="rec_label last"><?=$crt_lbl_total?></td>
					<td class="total last cs_totalAmount cs_tbc_label"><?=$crt_msg_complete?></td>
				</tr>												
			</table>
			<form class="cs_ppPaymentForm" action="<?=PP_PAYMENT_ENDPOINT?>" method="POST" data-mode='<?=(DEV?'dev':'')?>'>
				<?php if (DEV): ?>
					
				<?php else: ?>
					<input type="hidden" name="cmd" value="_s-xclick"/> 
					<input class="cs_ppwps_encryptedCart" type="hidden" name="encrypted" value="" />
				<?php endif ?>
				<a data-placement="left" data-trigger='manual' data-content="<?=$crt_popover_proceed2payment?>" href="javascript:void(0)" class="proceed_to_checkout payment btn_off cs_payWithPaypal cs_greenPopOver"><?=$crt_payment_btn?> ><div class="ajaxloader"><img src="assets/images/loading.gif"/></div></a>
				<div id="cart_messages" class="cs_frmEnableMsg">
					<?=$crt_confirm_disabled_msg?>
					<br><a href="javascript:void(0)" class="cs_updateAddressBtn"><?=$crt_update_addresses?></a>
				</div>
			</form>
		</div>
		<div class="disclaimer">
			<span class="show">
				<?=$crt_paypal_msg?>
				<div class="paypal_image clearfix"><a href="https://www.paypal.com/webapps/mpp/buying-online" target="_blank" border="0"><img src="assets/images/paypal_credit_cards.jpg" height="54" width="133" alt=""></a></div>
			</span>
		</div>
		<img src="assets/images/receipt_footer.png" height="35" width="133" alt="" class="receipt_footer">
	</div>
	
</div>