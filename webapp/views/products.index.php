<div class="secondary_nav">
	<div class="content_container">
		<?php foreach ($categories as $category):?>
	    	<div class="sub_cont<?=(($category['cat_id']==$currentCategory)?' cur_nav':'')?>"><a href="<?=Util::baseUrl('products/'.$category['cat_id'].'/'.Util::slugify($category['cat_name_'.$lan]),$lan)?>"><?=$category['cat_name_'.$lan]?></a></div>
	    <?php endforeach;?>
	    <span class="stretch"></span>				
	</div>
</div>
<div class="content_container clearfix">
	<div id="side_nav">
		<ul>
			<li class="<?=(($currentType==0)?'cur_nav':'')?>"><a href="<?=Util::baseUrl('products/'.$currentCategory.'/'.Util::slugify($categories[$currentCategory-1]['cat_name_'.$lan]).'/0/all/',$lan)?>"><?=strtoupper($prd_types_all)?></a></li>
			<?php foreach ($types as $type):?>
				<li class="<?=(($type['typ_id']==$currentType)?'cur_nav':'')?>"><a href="<?=Util::baseUrl('products/'.$currentCategory.'/'.Util::slugify($categories[$currentCategory-1]['cat_name_'.$lan],$lan).'/'.$type['typ_id'].'/'.Util::slugify($type['typ_name_'.$lan]).'/',$lan)?>"><?=$type['typ_name_'.$lan]?></a></li>
			<?php endforeach;?>
		</ul>
	</div>
	<div id="main_content">
		<?php
			$counter = 1;
			if($products):
				switch($currentCategory):
					// 2 colum display for Gifts category
					case 4:
						if($currentType == 0 || $currentType == 14) {
		?>
						<div class="gift_header">
							<table width="730" border="0" cellspacing="0" cellpadding="0">
							  <tr>
							    <td colspan="2" class="title"><?=$prd_giftboxes_header?></td>
							    <td rowspan="5"><img src="assets/images/gift_page_header.jpg" height="150" width="150" alt=""></td>
							  </tr>
							  <tr>
							    <td>
							    	<ul>
							    		<li><?=$prd_gb_item1?></li>
							    		<li><?=$prd_gb_item2?></li>
							    	</ul>
							    </td>
							    <td>
							    	<ul>
							    		<li><?=$prd_gb_item3?></li>
							    		<li><?=$prd_gb_item4?></li>
							    	</ul>
							    </td>
							  </tr>
							  <tr>
							    <td colspan="2" class="sub_title"><?=$prd_gb_contactus?></td>
							  </tr>
							  <tr>
							    <td colspan="2"><b>T</b> <?=INFO_PHONE?>  |  <a href="mailto:<?=INFO_EMAIL?>"><?=INFO_EMAIL?></a></td>
							  </tr>
							</table>
						</div>
		<?php
						}
						foreach ($products as $product):
		?>
							<div style="min-height:470px !important;" class="product_block gift_block<?=($counter%2==0)?' last_element':''?>">
								<a class="csBlockAnchor" href="<?=Util::baseUrl('product/'.$product['prd_id'].'/'.$product['pdd_slug'],$lan)?>">
									<div class="label <?=($product['prd_saleprice'])?' sale_'.$lan:''?><?=Util::isNew($product['prd_date'])?' new_'.$lan:''?>"></div>	
									<div style='width:360px; height:309px; overflow:hidden;'><img src="data/images/product/<?=$product['prd_img_small']?>" alt="" class="product_small"></div>
									<p class="desc"><?=($product['prd_typ_id_fk'])?$availableTypes[$product['prd_typ_id_fk']][$lan]:''?></p>
									<p class="title"><?=$product['pdd_name']?></p>
								</a>
								<form novalidate method="post" action="<?=Util::baseUrl('cart/',$lan)?>" data-invalid-msg="<?=$com_qty_pattern_error?>" data-invalid-maxvalue-msg="<?=$com_qty_stock_error?>">
									<input type="hidden" name="action" value="add"/>
									<input type="hidden" name="prd_id" value="<?=$product['prd_id']?>"/>
									<?php if ($product['prd_saleprice']): ?>
										<p class="pr_sale_price"><?=Util::formatPrice($product['prd_saleprice'],$lan);?></p>
										<p class='pr_prev_price'><?=Util::formatPrice($product['prd_price'],$lan);?></p>
									<?php else: ?>
										<p class="gift_price"><?=Util::formatPrice($product['prd_price'],$lan);?></p>
									<?php endif; ?>
									<?php if ($product['prd_stock']>0): ?>
										<a href="javascript:void(0);" class="add_to_cart pr_hub_btn"><?=$prd_buybtn?></a>
										<span class="cs_posAnchor"><input autocomplete='off' type="text" name="crd_quantity" value='1' required pattern="[1-9][0-9]?" maxlength="2" maxvalue="<?=$product['prd_stock']?>"></span>
									<?php else: ?>
										<span class="add_to_cart cs_no_action course_full_btn"><?=$prd_outofstock ?></span><br/>
									<?php endif; ?>
								</form>
							</div>
		<?php 
							$counter++;
						endforeach;
						break;					
					default:
						// 4-column display for all other categories
						foreach ($products as $product):
		?>
							<div style="min-height:526px !important;" class="product_block<?=($counter%4==0)?' last_element':''?>">
								<a class="csBlockAnchor" href="<?=Util::baseUrl('product/'.$product['prd_id'].'/'.$product['pdd_slug'],$lan)?>">
									<div class="label <?=($product['prd_saleprice'])?' sale_'.$lan:''?><?=Util::isNew($product['prd_date'])?' new_'.$lan:''?>"></div>	
									<div style='width:162px; height:310px; overflow:hidden;'><img src="data/images/product/<?=$product['prd_img_small']?>" alt="" class="product_small"></div>
									<p class="desc"><?=($product['prd_typ_id_fk'])?$availableTypes[$product['prd_typ_id_fk']][$lan]:''?></p>
									<p class="title"><?=$product['pdd_name']?></p>
									<?php if ($product['prd_saleprice']): ?>
										<p class="price clearfix">
											<span class='csSalePrice'><?=Util::formatPrice($product['prd_saleprice'],$lan);?></span>
											<span class='csRegPrice'><?=Util::formatPrice($product['prd_price'],$lan);?></span>
										</p>
									<?php else: ?>
										<p class="price"><?=Util::formatPrice($product['prd_price'],$lan);?></p>
									<?php endif; ?>
								</a>
								<form novalidate method="post" action="<?=Util::baseUrl('cart/',$lan)?>" data-invalid-msg="<?=$com_qty_pattern_error?>" data-invalid-maxvalue-msg="<?=$com_qty_stock_error?>">
									<?php if ($product['prd_stock']>0): ?>
										<input type="hidden" name="action" value="add"/>
										<input type="hidden" name="prd_id" value="<?=$product['prd_id']?>"/>
										<span class="cs_posAnchor"><input autocomplete='off' type="text" name="crd_quantity" value='1' required pattern="[1-9][0-9]?" maxlength="2" maxvalue="<?=$product['prd_stock']?>"></span>
										<a href="javascript:void(0);" class="add_to_cart pr_hub_btn"><?=$prd_buybtn?></a>
									<?php else: ?>
										<span class="add_to_cart cs_no_action course_full_btn"><?=$prd_outofstock ?></span><br/>
									<?php endif; ?>
								</form>
							</div>
		<?php 
							$counter++;
						endforeach;
						break;
				endswitch;
			else:
				echo $prd_emptyset;
			endif;
		?>
	</div>		
</div>