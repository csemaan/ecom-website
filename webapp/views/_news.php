<!-- Groupe-Année -->
<h2>2013</h2>
<!-- Nouvelles -->
<div class="news_block clearfix">
	<img src="assets/images/news/godfoodlocal.jpg" height="148" width="148" alt="" class="postcard">
	<div class="news_desc">
		<h2>August 28th, 2013</h2>
		<h1><a href="http://localmagazine.ca/v2/wp-content/uploads/2013/08/OliveOlives.pdf" target="_blank">Olive Oils, Tapenades and Olives, Oh My!!!</a></h1>
		<p class="reference"><b>Source:</b> localmagazine.ca</p>
	</div>
</div>
<div class="news_block clearfix">
	<img src="assets/images/news/thestar.jpg" height="148" width="148" alt="" class="postcard">
	<div class="news_desc">
		<h2>April 1st 2013</h2>
		<h1><a href="http://www.thestar.com/life/food_wine/2013/04/01/weak_olive_harvest_in_spain_means_price_of_olive_oil_will_rise.html" target="_blank">Weak olive harvest in Spain means price of olive oil will rise</a></h1>
		<p class="reference"><b>Source:</b> thestar.com</p>
	</div>
</div>
<div class="news_block clearfix">
	<img src="assets/images/news/CanadianLiving.jpg" height="148" width="148" alt="" class="postcard">
	<div class="news_desc">
		<h2>March 7th 2013</h2>
		<h1><a href="http://www.canadianliving.com/blogs/health/2013/03/07/health-benefits-of-olive-oil/" target="_blank">Health Benefits of Olive Oil</a></h1>
		<p class="reference"><b>Source:</b> Canadian Living</p>
	</div>
</div>
<div class="news_block clearfix">
	<img src="assets/images/news/Nathalie.jpg" height="148" width="148" alt="" class="postcard">
	<div class="news_desc">
		<h2>15 février 2013</h2>
		<h1><a href="http://www.natalierichard.com/blog/pour-tout-savoir-sur-lhuile-dolive/?noredirect=fr" target="_blank">Pour tout savoir sur l'huile d'olive</a></h1>
		<h2>February 15th 2013</h2>
	<h1><a href="http://www.natalierichard.com/en/2013/03/15/everything-you-always-wanted-to-know-about-olive-oil/" target="_blank">Everything You Always Wanted to Know About Olive Oil</a> </h1>
<p class="reference"><b>Source:</b> Nathalie Richard – Art de vivre</p>
</div>
</div>
<div class="news_block clearfix">
	<img src="assets/images/news/bouches.jpg" height="148" width="148" alt="" class="postcard">
	<div class="news_desc">
		<h2>15 janvier 2013</h2>
		<h1><a href="http://boucheesdoubles.net/2013/01/des-citrouilles-en-hiver/" target="_blank">Des citrouilles en hiver!</a></h1>
		<p class="reference"><b>Source:</b> Bouchées Doubles</p>
	</div>
</div>

<!-- Groupe-Année -->
<h2>2012</h2>
<div class="news_block clearfix">
	<img src="assets/images/news/cityboomers.jpg" height="148" width="148" alt="" class="postcard">
	<div class="news_desc">
		<h2>23 novembre 2012</h2>
		<h1><a href="http://www.citeboomers.com/AL-Produits--PRODUIT-L-HUILE-espagnole-Hoji-Blanca-.php?page=3&id=AL2899003" target="_blank">Produit: L'huile espagnole Hojiblanca</a></h1>
		<p class="reference"><b>Source:</b> Cité Boomers | Un style de vie</p>
	</div>
</div>
<div class="news_block clearfix">
	<img src="assets/images/news/FoodsSpain.jpg" height="148" width="148" alt="" class="postcard">
	<div class="news_desc">
		<h2>October 30th 2012</h2>
		<h1><a href="http://www.foodsfromspain.com/icex/cda/controller/pageSGT/0,9459,35868_6865989_6908352_4619339_0,00.html" target="_blank">Olive & Olives, an extra virgin olive oil boutique</a></h1>
		<p class="reference"><b>Source:</b> Foods From Spain</p>
	</div>
</div>
<div class="news_block clearfix last_news">
	<img src="assets/images/news/countlan.jpg" height="148" width="148" alt="" class="postcard">
	<div class="news_desc">
		<h2>November 13th 2012</h2>
		<h1><a href="http://issuu.com/countlan/docs/countlanissue2/5" target="_blank">Adopt An Olive Tree</a></h1>
		<p class="reference"><b>Source:</b> Countlan Magazine Issue 2</p>
	</div>
</div>