<?php
	// Required for top banner free delivery notice : Registers a variable 
	// when this is the first visit to any page of the site during current session
	if(!isset($_COOKIE["oolanded"])) {
		$firstVisit = true;
		setcookie("oolanded","true");
	}

	if(isset($nocache) && $nocache===true) {
		// header("Cache-Control: no-cache, must-revalidate");
		// header("Pragma: no-cache");
		header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
		header("Pragma: no-cache"); // HTTP/1.0
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	}
?>
<!DOCTYPE html>
<!--[if IE 8]>
	<html class="bad-ie">
<![endif]-->
<!--[if IE 7]>
	<html class="superbad-ie">
<![endif]-->
<!--[if gt IE 8]>
	<html class="good-ie">
<![endif]-->
<!--[if !IE]><!-->
<html> 
<!--<![endif]-->
<head>
	<base href="<?=((isset($_SERVER["HTTPS"]))?BASE_SURL:BASE_URL)?>"></base>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?=((isset($meta_title_override))?$meta_title_override:$meta_title);?></title>
	<link rel="shortcut icon" href="favicon.ico?v=2" />
	<meta name="viewport" content="width=1380">
	<meta name="description" content="<?=((isset($meta_desc_override))?$meta_desc_override:$meta_desc);?>">
	<meta charset="utf-8">
	<!--[if lt IE 9]>
		<script src="assets/js/lib/html5shiv.js"></script>
	<![endif]-->
	<script type="text/javascript" src="//use.typekit.net/pox7xcr.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	<link rel="stylesheet" href="assets/styles/css/normalize.css">
	<link rel="stylesheet" href="assets/styles/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/styles/css/main.css">
	<link rel="stylesheet" href="assets/styles/css/additions_cs.css">
	<!-- Facebook Meta Tags -->
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?='http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];?>" />
	<meta property="og:image" content="<?=((isset($fb_og_image))?$fb_og_image:BASE_URL.'assets/images/logo.png');?>" />
	<meta property="og:title" content="<?=((isset($meta_title_override))?$meta_title_override:$meta_title);?>" />
	<meta property="og:description" content="<?=((isset($meta_desc_override))?$meta_desc_override:$meta_desc);?>" />
	<meta property="fb:admins" content="100004609932370"/>
</head>
<body xmlns:fb="http://ogp.me/ns/fb#" lang="<?=$lan?>">
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-9668860-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
	<!-- Topbar free delivery notice banner -->
	<section class="topbar-free-shopping">
		<div class="img"><img src="assets/images/<?php echo $lan; ?>-animation<?php echo (isset($firstVisit))?"":"-end"; ?>.gif" alt=""></div>
	</section>
	<header class="header_bg">
		<div class="content_container">
			<span class="tools">
				<ul>
				    <li><a href="<?=Util::baseUrl('home',$lan)?>"><?=$hea_menu_home?></a></li>
				    <li class="shopping_cart">
				    	<a href="<?=Util::baseUrl('cart',$lan)?>"><?=$hea_menu_cart?><?php 
				    		if (isset($cartSummary) && count($cartSummary)>0): 
				    			$cartItemsNumber = $cartSummary['product'][0]['itemtotal'] + $cartSummary['class'][0]['itemtotal'];
				    			if($cartItemsNumber>0) :
				    	?><div class="cart_num show"><p><?=$cartItemsNumber?></p></div>
				    	<?php 
				    			endif;
				    		endif;
				    	?></a>
				    </li>
				    <li><a href="<?=Util::toggleLan($ruri,$extraSlugs)?>"><?=(($lan=='fr')?'EN':'FR')?></a></li>
				</ul>
			</span>
			<nav>
				<ul>
				    <li class="<?=($pageMenu=='products'||$pageMenu=='product')?'cur_nav':''?>"><a href="<?=Util::baseUrl('products',$lan)?>"><?=$hea_menu_product?></a></li>
				    <li class="<?=($pageMenu=='recipes'||$pageMenu=='recipe')?'cur_nav':''?>"><a href="<?=Util::baseUrl('recipes',$lan)?>"><?=$hea_menu_recipe?></a></li>
				    <!-- Classes/Ateliers : removed at client request Dec 2015 -->
					  <!-- <li class="<?=($pageMenu=='classes')?'cur_nav':''?>"><a href="<?=Util::baseUrl('classes',$lan)?>"><?=$hea_menu_class?></a></li> -->
				    <li class="<?=($pageMenu=='stores')?'cur_nav':''?>"><a href="<?=Util::baseUrl('stores',$lan)?>"><?=$hea_menu_address?></a></li>
				    <li class="<?=($pageMenu=='about')?'cur_nav':''?>"><a href="<?=Util::baseUrl('about',$lan)?>"><?=$hea_menu_about?></a></li>
				</ul>
			</nav>
			<a href="<?=$lan?>"><h3 class="logo"><?=$hea_logo?></h3></a>	
		</div>
	</header>