<div class="secondary_nav">
	<h2>Boutiques • Points de vente • Bureau chef</h2>
</div>
<div class="content_container clearfix">
	<div class="address_block header_address">
		<h2>Olives &amp; Épices</h2>
		<h1>Montréal</h1>
		<div class="address">
			<p class="street">3127, rue Masson<br>Montréal (Québec)  H1Y 1X8</p>
			<p class="contact">T  514 526-8989</p>
			<p class="contact">C  <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>						
			<a href="http://goo.gl/K5cdZX" class="map_btn" target="_blank">LOCALISER</a>
		</div>					
		<table class="hours">
			<tr>
				<td class="day">lundi</td>
				<td>10 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">mardi</td>
				<td>10 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">mercredi</td>
				<td>10 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">jeudi</td>
				<td>10 h à 20 h</td>
			</tr>
			<tr>
				<td class="day">vendredi</td>
				<td>10 h à 20 h</td>
			</tr>
			<tr>
				<td class="day">samedi</td>
				<td>10 h à 17 h</td>
			</tr>
			<tr>
				<td class="day">dimanche</td>
				<td>10 h à 17 h</td>
			</tr>
		</table>			
	</div>
	<div class="address_block header_address last_element">
		<h2>Olive &amp; Olives</h2>
		<h1>Montréal, Marché Jean-Talon</h1>
		<div class="address">					
			<p class="street">7070, avenue Henri-Julien<br>Montréal (Québec) H2R 1T1</p>
			<p class="contact">T  514 271-0001</p>
			<p class="contact">C  <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>	
			<a href="http://goo.gl/maps/QZSA0" class="map_btn" target="_blank">LOCALISER</a>
		</div>					
		<table class="hours">
			<tr>
				<td class="day">lundi</td>
				<td>9 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">mardi</td>
				<td>9 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">mercredi</td>
				<td>9 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">jeudi</td>
				<td>9 h à 20 h</td>
			</tr>
			<tr>
				<td class="day">vendredi</td>
				<td>9 h à 20 h</td>
			</tr>
			<tr>
				<td class="day">samedi</td>
				<td>9 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">dimanche</td>
				<td>9 h à 17 h</td>
			</tr>
		</table>			
	</div>
	<div class="address_block">
		<h2>Olive &amp; Olives</h2>
		<h1>Laval, Marché Gourmand</h1>
		<div class="address">
			<p class="street">2888, avenue du Cosmodôme<br>Laval (Québec)  H7T 2X1</p>
			<p class="contact">T  450 687-8222</p>
			<p class="contact">C  <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>							
			<a href="http://goo.gl/maps/k6Seu" class="map_btn" target="_blank">LOCALISER</a>
		</div>					
		<table class="hours">
			<tr>
				<td class="day">lundi</td>
				<td>9 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">mardi</td>
				<td>9 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">mercredi</td>
				<td>9 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">jeudi</td>
				<td>9 h à 21 h</td>
			</tr>
			<tr>
				<td class="day">vendredi</td>
				<td>9 h à 21 h</td>
			</tr>
			<tr>
				<td class="day">samedi</td>
				<td>9 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">dimanche</td>
				<td>9 h à 18 h</td>
			</tr>
		</table>			
	</div>
	<div class="address_block last_element">
		<h2>Olive &amp; Olives</h2>
		<h1>St-Lambert</h1>
		<div class="address">					
			<p class="street">428b, avenue Victoria<br>St-Lambert (Québec)  J4P 2H9</p>
			<p class="contact">T  450 923-2424</p>
			<p class="contact">C  <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>	
			<a href="http://goo.gl/maps/WmS0N" class="map_btn" target="_blank">LOCALISER</a>
		</div>					
		<table class="hours">
			<tr>
				<td class="day">lundi</td>
				<td>10 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">mardi</td>
				<td>10 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">mercredi</td>
				<td>10 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">jeudi</td>
				<td>10 h à 20 h</td>
			</tr>
			<tr>
				<td class="day">vendredi</td>
				<td>10 h à 20 h</td>
			</tr>
			<tr>
				<td class="day">samedi</td>
				<td>10 h à 17 h</td>
			</tr>
			<tr>
				<td class="day">dimanche</td>
				<td>10 h à 17 h</td>
			</tr>
		</table>			
	</div>
	<div class="address_block">
		<h2>Olive &amp; Olives</h2>
		<h1>Toronto</h1>
		<div class="address">
			<p class="street">779, Queen St East<br>Toronto (Ontario) M4M 1H5</p>
			<p class="contact">T  416 551-8181</p>
			<p class="contact">C  <a href="mailto:contact@oliveolives.com" target="_blank" class="mail_link">contact@oliveolives.com</a></p>								
			<a href="http://goo.gl/maps/eQSFd" class="map_btn" target="_blank">LOCALISER</a>
		</div>					
		<table class="hours">
			<tr>
				<td class="day">lundi</td>
				<td>11 h à 19 h</td>
			</tr>
			<tr>
				<td class="day">mardi</td>
				<td>11 h à 19 h</td>
			</tr>
			<tr>
				<td class="day">mercredi</td>
				<td>11 h à 19 h</td>
			</tr>
			<tr>
				<td class="day">jeudi</td>
				<td>11 h à 19 h</td>
			</tr>
			<tr>
				<td class="day">vendredi</td>
				<td>11 h à 19 h</td>
			</tr>
			<tr>
				<td class="day">samedi</td>
				<td>11 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">dimanche</td>
				<td>11 h à 17 h</td>
			</tr>
		</table>			
	</div>
	<div class="address_block last_element">
		<h2>Olive &amp; Olives</h2>
		<h1>Toronto, St-Lawrence Market</h1>
		<div class="address">					
			<p class="street">20, Market Street<br>Toronto (Ontario) M5A 1E9</p>
			<p class="contact">T  416 TBD ****</p>
			<p class="contact">C  <a href="mailto:contact@oliveolives.com" target="_blank" class="mail_link">contact@oliveolives.com</a></p>	
			<a href="http://goo.gl/maps/oMeKk" class="map_btn" target="_blank">LOCALISER</a>
		</div>					
		<table class="hours">
			<tr>
				<td class="day">lundi</td>
				<td>11 h à 19 h</td>
			</tr>
			<tr>
				<td class="day">mardi</td>
				<td>11 h à 19 h</td>
			</tr>
			<tr>
				<td class="day">mercredi</td>
				<td>11 h à 19 h</td>
			</tr>
			<tr>
				<td class="day">jeudi</td>
				<td>11 h à 29 h</td>
			</tr>
			<tr>
				<td class="day">vendredi</td>
				<td>11 h à 29 h</td>
			</tr>
			<tr>
				<td class="day">samedi</td>
				<td>11 h à 18 h</td>
			</tr>
			<tr>
				<td class="day">dimanche</td>
				<td>11 h à 17 h</td>
			</tr>
		</table>
	</div>
	<div class="address_block">
		<h1 style="margin-top:23px;">Olive &amp; Olives chez Costco</h1>
		<div class="address">
			<h2 class="add_full_width">Costco Brossard - Costco Pointe-Claire (Québec) :</h2>
			<p class="street">Du 9 au 21 juillet 2013</p>
			<h2 class="add_full_width">Costco Newmarket (Ontario) :</h2>
			<p class="street">Du 9 au 21 juillet 2013</p>
		</div>								
	</div>

	<div class="address_block last_element">
		<h2>Distribution Olive &amp; Olives inc.</h2>
		<h1 style="letter-spacing:-0.04em;">Montréal, bureau chef • Entrepôt</h1>
		<div class="address">					
			<p class="street">4571, boulevard Métropolitain est<br>Montréal (Québec)  H1R 1Z4</p>
			<p class="contact">T  514 381-4020</p>
			<p class="contact">C  <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>	
			<a href="http://goo.gl/maps/QsF2b" class="map_btn" target="_blank">LOCALISER</a>
		</div>
	</div>
</div>