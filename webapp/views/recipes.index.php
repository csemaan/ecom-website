<div class="secondary_nav">
	<h2><?=$rcp_catchphrase?></h2>
</div>
<div class="content_container clearfix">
	<div id="side_nav">
		<h2><img src="assets/images/search_icon.png" height="15" width="16" alt=""><?=$rcp_search_label?></h2>
		<input class="searchBox" type="text">
	</div>
	<div id="main_content" class="cs_recipeList">
		<?php
			$counter = 1;
			if($recipes):
				foreach ($recipes as $recipe):
		?>
					<a href="<?=Util::baseUrl('recipe/'.$recipe['rcp_id'].'/'.$recipe['rcd_slug'],$lan)?>">
						<div class="recipe_block<?=($counter%4==0)?' last_element':''?>">
							<img src="data/images/recipe/<?=$recipe['rcp_img_small']?>" height="148" width="148" alt="" class="postcard">
							<p><?=$recipe['rcd_title']?></p>
						</div>
					</a>
		<?php 
					$counter++;
				endforeach;
			else:
				echo $rcp_emptyset;
			endif;
		?>
	</div>		
</div>