<div class="secondary_nav">
	<h2><?=$abt_catchphrase ?></h2>
</div>
<div class="content_container clearfix">
	<div id="side_nav">
		<ul>
			<?php 
				for($i=0; $i<count($about); $i++) :
			?>
					<li class="<?=(($section==$i)?'cur_nav':'')?>"><a href="<?=Util::baseUrl('about/'.$i.'/'.$about[$i][0],$lan)?>"><?=${$about[$i][1]}?></a></li>
			<?php 
				endfor;
			?>
		</ul>
	</div>
	<div id="main_content" class="static_page">
		<?php  
			switch($section):
		
				// About Us
				case 0:
		?>
					<h1>About Olive &amp; Olives</h1>
					<hr></hr>
					<p class="intro">Founded in 2003 by two epicureans and curious businesswomen, Claudia M. Pharand and Danièle Beauchamp, Olive &amp; Olives works directly with producers in order to offer authentic and genuine products.</p>

					<img src="assets/images/PhiloEN.jpg" height="213" width="780" alt="">
				
					<h2>Philosophy</h2>

					<p>Each of the oils that you find at Olive &amp; Olives has come from a certified domain or cooperative, lovingly produced by artisans with extensive knowledge and loyalty to their craft. Our goal is to take that shared knowledge and to pass it on to you so that you can make an informed and satisfying choice.</p>

					<h2 style="margin:0; padding: 0;">Head Office</h2>
					<p>8262, Pie IX blvd<br>
					Montreal (Quebec) H1Z 3T6 <br>
					<b>T 514 381-4020</b><br>
					<b>E</b> <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>
			
			<?php 
					break;
			
				// News
				case 1:
			?>
					<h1>Olive &amp; Olives in the News</h1><hr>
					<p class="intro">
						Olive &amp; Olives’ is a one-of-a-kind boutique with a chic design that was created to eliminate direct light on the oil bottles and to keep the place spacious for better browsing. Countries from which oil is imported include Spain (the most popular), Italy, Greece, Australia, Turkey and Chile.
					</p>
					<p>
						You will also find different varieties of olives packed with hot peppers, fine herbs or garlic. Other items for sale include olive oil chips, olive paste, aromatized olive oil, vinegars, olive oil pourers and carafes.
					</p>
		<?php 
					include_once("_news.php");
					break;			
			endswitch;
		?>
	</div>	
</div>
