<div class="content_container clearfix">
	<div id="side_nav">
		<ul>
			<li class="cur_nav"><a href="">Privacy policy</a></li>
			<li><a href="">Terms and conditions</a></li>
			<li><a href="">Cancellation policy</a></li>
			<li><a href="">Shipping information</a></li>
		</ul>
	</div>
	<div id="main_content" class="static_page">
		<h1>Our Cancellation Policy</h1>
		<hr></hr>
			<p class="intro">Olive & Olives reserves the right to cancel a course due to insufficient enrolment or due to factors out of their control. Participants will be notified ahead of time by telephone. If the workshop or class cannot be moved to a later date that is convenient to the client, the class will be fully refunded. </p>

			<p>If a participant wishes to cancel a workshop or class, Olive & Olives must be informed by telephone or email seven days prior to the event in order to substitute for another date or to be eligible for a store credit. </p>

			<p>If cancellation notices are received less than seven days before the event, Olive & Olives is unable to issue a store credit or to substitute with another class. No refund.</p>
	</div>	
</div>
