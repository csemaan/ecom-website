<?php if (count($slideshowData)>1): ?>
<div class="slide_controls clearfix">
	<span class="slide_nav">
		<img class='ssc_left' src="assets/images/rotator_left.gif" height="33" width="31" alt="">
		<?php foreach ($slideshowData as $key=>$slide): ?>
			<span class='ssc_dot<?=($key==0)?' ssc_on':' ssc_off'?>'></span>
		<?php endforeach; ?>
		<img class='ssc_right' src="assets/images/rotator_right.gif" height="33" width="31" alt="">
	</span>
</div>
<?php endif; ?>

<div class="slideshow">
	<div class="slidesmask">
		<div class="slides">
		<?php 
			foreach($slideshowData as $slide):
		?>
				<article>
					<?=($slide['sld_link'])?"<a href='".$slide['sld_link']."'>":''?>
					<img src="data/images/slideshow/<?=$slide['sli_img']?>" alt=""/>
					<div class="slide">
						<h1><?=$slide['sld_title']?></h1>
						<?php if ($slide['sld_desc']!=""): ?>
							<p><?=$slide['sld_desc']?><img src="assets/images/black_arrow.png" height="11" width="7" alt="" class="link_arrow"></p>
						<?php endif ?>
					</div>
					<?=($slide['sld_link'])?'</a>':''?>
				</article>
		<?php 
			endforeach;
		?>
		</div>
	</div>
</div>
<div class="full_width_container hp_callout_bar clearfix">
	<div class="content_container">
		<a href="<?=Util::baseUrl('policies/3/shipping',$lan)?>">
			<div class="hp_feature clearfix">
				<img src="assets/images/home_feature_1.jpg" height="150" width="150" alt="" class="postcard">
				<h1><?=$hom_shipping_title?><img src="assets/images/black_arrow.png" height="11" width="7" alt="" class="link_arrow"></h1>
				<p><?=$hom_shipping_text?></p>
			</div>
		</a>
		<a href="<?=Util::baseUrl('products/4/',$lan)?>">
			<div class="hp_feature last_element">
				<img src="assets/images/home_feature_3.jpg" height="150" width="150" alt="" class="postcard">
				<h1><?=$hom_corporate_title?><img src="assets/images/black_arrow.png" height="11" width="7" alt="" class="link_arrow"></h1>
				<p><?=$hom_corporate_text?></p>
			</div>
		</a>
		<a href="<?=Util::baseUrl('recipes',$lan)?>">
			<div class="hp_feature">
				<img src="assets/images/home_feature_2.jpg" height="150" width="150" alt="" class="postcard">
				<h1><?=$hom_recipes_title?><img src="assets/images/black_arrow.png" height="11" width="7" alt="" class="link_arrow"></h1>
				<p><?=$hom_recipes_text?></p>
			</div>
		</a>
		<!-- Classes/Ateliers : removed at request of client (Dec 2015) -->
		<?php /* 
		<a href="<?=Util::baseUrl('classes',$lan)?>">
			<div class="hp_feature last_element">
				<img src="assets/images/home_feature_4.jpg" height="150" width="150" alt="" class="postcard">
				<h1><?=$hom_classes_title?><img src="assets/images/black_arrow.png" height="11" width="7" alt="" class="link_arrow"></h1>
				<p><?=$hom_classes_text?></p>
			</div>
		</a>
		*/ ?>
		<a href="<?=Util::baseUrl('stores',$lan)?>">
			<div class="hp_feature last_element">
				<img src="assets/images/home_feature_1.jpg" height="150" width="150" alt="" class="postcard">
				<h1><?=$hom_address_title?><img src="assets/images/black_arrow.png" height="11" width="7" alt="" class="link_arrow"></h1>
				<p><?=$hom_address_text?></p>
			</div>
		</a>
	</div>
</div>