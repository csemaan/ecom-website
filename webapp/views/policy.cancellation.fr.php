<div class="content_container clearfix">
	<div id="side_nav">
		<ul>
			<li class="cur_nav"><a href="">Privacy policy</a></li>
			<li><a href="">Terms and conditions</a></li>
			<li><a href="">Cancellation policy</a></li>
			<li><a href="">Shipping information</a></li>
		</ul>
	</div>
	<div id="main_content" class="static_page">
		<h1>Politique d'annulation</h1>
		<hr/>
		<p class="intro">Olive &amp; Olives se réserve le droit d’annuler un atelier ou un cours de cuisine en cas de nombre d'inscrits insuffisant et/ou pour des raisons hors de son contrôle.</p>
		<p class="intro">Olive &amp; Olives s’engage à informer le client d’une éventuelle annulation par téléphone dans les meilleurs délais. Si l’atelier ou le cours de cuisine ne peut être déplacé à une date ultérieure qui convient au client, celui-ci sera entièrement remboursé.</p>
		<h2>Annulation par le client</h2>
		<p>Tout participant ayant réservé pour un atelier ou un cours de cuisine et souhaitant annuler, l’annulation doit se faire par téléphone en communiquant au 514-381-4020 ou par courriel à <a href="maito:info@oliveolives.com">info@oliveolives.com</a>.</p>
		<p>Si un participant est incapable d’assister à un cours il doit aviser Olive &amp; Olives 7 jours avant l'évènement afin de pouvoir s’inscrire à un autre événement ou recevoir une note de crédit.</p>
		<p>Pour toute annulation reçue à moins de 7 jours d’avis précédant l’événement, Olive &amp; Olives ne pourra émettre une nouvelle inscription ou une note de crédit. Aucun remboursement.</p>
		<h2>Informations sur l'exploitant</h2>
		<p>Le site www.oliveolives.com est exploité par Distributions Olive &amp; Olives inc. Les conditions de vente décrites ci-dessus constituent un contrat entre le client et Olive &amp; Olives. Ces conditions peuvent être modifiées en tout temps par Olive &amp; Olives.</p>
	</div>	
</div>