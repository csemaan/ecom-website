<?php include_once("fblike.inc.php"); ?>
<div class="secondary_nav">
	<div class="content_container">
		<?php foreach ($categories as $category):?>
	    	<div class="sub_cont<?=(($category['cat_id']==$currentCategory)?' cur_nav':'')?>"><a href="<?=Util::baseUrl('products/'.$category['cat_id'].'/'.Util::slugify($category['cat_name_'.$lan]),$lan)?>"><?=$category['cat_name_'.$lan]?></a></div>
	    <?php endforeach;?>
	    <span class="stretch"></span>				
	</div>
</div>
<div class="content_container">
	<div class="pagination clearfix">			
		<a href="<?=$currentProductsRoute?>"><img src="assets/images/grey_left_arrow.gif" height="16" width="14" alt=""><?=$com_backtolist?></a>
		<?php 
			if(isset($productsIdSet) && count($productsIdSet)>1): 
			$currentIndexInSet = array_search($product['prd_id'], $productsIdSet);
		?>
			<a href="<?=Util::baseUrl('product/'.$productsIdSet[gmp_mod($currentIndexInSet-1,count($productsIdSet))],$lan)?>" id="prev"><img src="assets/images/grey_left_arrow.gif" height="16" width="14" alt=""></a>
			<a href="<?=Util::baseUrl('product/'.$productsIdSet[gmp_mod($currentIndexInSet+1,count($productsIdSet))],$lan)?>" id="next"><img src="assets/images/grey_right_arrow.gif" height="16" width="14" alt=""></a>
		<?php endif; ?>
		<!--
		<div id="prev_text" class="clearfix">Previous Product</div>
		<div id="next_text" class="clearfix">Next Product</div>	
		-->
	</div>
</div>
<div class="content_container clearfix">
	<div id="product_details" class="clearfix">
		<h3><?=($product['prd_typ_id_fk'])?$availableTypes[$product['prd_typ_id_fk']][$lan]:''?></h3>
		<h1><?=$product['pdd_name']?></h1>
		<h2><?=$product['pdd_origin']?></h2>
		<span class="pr_desc">
			<?php if ($product['pdd_desc1_text']): ?>
				<h3><?=$product['pdd_desc1_title']?></h3>
				<p><?=$product['pdd_desc1_text']?></p>
			<?php endif ?>
			<?php if ($product['pdd_desc2_text']): ?>
				<h3><?=$product['pdd_desc2_title']?></h3>
				<p><?=$product['pdd_desc2_text']?></p>				
			<?php endif ?>
			<?php if ($product['pdd_desc3_text']): ?>
				<h3><?=$product['pdd_desc3_title']?></h3>
				<p><?=$product['pdd_desc3_text']?></p>
			<?php endif ?>
		</span>
		<span class="pr_details">
			<?php if($product['pdd_format']):?>
				<p class="pr_size"><?=$product['pdd_format']?></p>
			<?php endif; ?>
			<?php if($product['prd_saleprice']): ?>
				<p class="pr_sale_price"><?=Util::formatPrice($product['prd_saleprice'],$lan)?></p>
				<p class="pr_prev_price"><?=Util::formatPrice($product['prd_price'],$lan)?></p>
			<?php else: ?>
				<p class="pr_sale_price"><?=Util::formatPrice($product['prd_price'],$lan)?></p>
			<?php endif; ?>

			<form novalidate class="clearfix" method="post" action="<?=Util::baseUrl('cart/',$lan)?>" data-invalid-msg="<?=$com_qty_pattern_error?>" data-invalid-maxvalue-msg="<?=$com_qty_stock_error?>"> 
				<?php if ($product['prd_stock']>0): ?>
					<input type="hidden" name="action" value="add"/>
					<input type="hidden" name="prd_id" value="<?=$product['prd_id']?>"/>
					<span class="cs_posAnchor"><input autocomplete='off' type="text" name="crd_quantity" value='1' required pattern="[1-9][0-9]?" maxlength="2" maxvalue="<?=$product['prd_stock']?>"></span>
					<a href="#" class="add_to_cart pr_detail_btn"><?=$pdd_buybtn?></a> 
				<?php else: ?>
					<p style="width:180px; margin:0;">
						<span style="float:left" class="add_to_cart cs_no_action course_full_btn"><?=$pdd_outofstock ?></span>
					</p>
				<?php endif; ?>
			</form>				
			<p class="pr_delivery"><?=$pdd_free_delivery?><br><i class="cs_smallertext"><?=$pdd_free_delivery_notice?></i></p>
			<a href="javascript:void(0);" data-popupurl="<?=Util::makeMailtoUrl($com_share_subject,$com_share_body);?>" class="cs_popuplink pr_button">+ <?=$com_share?></a>
			<a href="javascript:void(0);" data-popupurl="<?=Util::baseUrl('printproduct/'.$product['prd_id'].'/'.$product['pdd_slug'],$lan)?>" class="cs_popuplink pr_button last_element"><?=$com_print?></a>
			<br/>
			<div class="fb-like" data-href="<?='http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']?>" data-width="200" data-layout="button_count" data-show-faces="false" data-send="false" style="padding:10px 0;"></div>
		</span>
		<?php if (isset($recipes) && count($recipes)>0): ?>
			<div id="recipe_ideas">
				<h3><?=$pdd_recipe_ideas?></h3>
				<ul class="pr_recipes clearfix">
					<?php
						$counter = 1; 
						foreach ($recipes as $recipe): 
					?>
						<li<?=($counter%4==0)?" class='last_element'":""?>>
							<a href="<?=Util::baseUrl('recipe/'.$recipe['rcp_id'].'/'.$recipe['rcd_slug'],$lan)?>"><img src="data/images/recipe/<?=$recipe['rcp_img_small']?>" height="148" width="148" alt="" class="postcard"><?=$recipe['rcd_title']?></a>
						</li>
					<?php 
						$counter++;
						endforeach; 
					?>
				</ul>
			</div>
		<?php endif; ?>
	</div>
	<img src="data/images/product/<?=$product['prd_img_large']?>" alt="" class="product_large">	
</div>