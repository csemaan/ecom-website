<div class="secondary_nav">
	<h2><?=$abt_catchphrase ?></h2>
</div>
<div class="content_container clearfix">
	<div id="side_nav">
		<ul>
			<?php 
				for($i=0; $i<count($about); $i++) :
			?>
					<li class="<?=(($section==$i)?'cur_nav':'')?>"><a href="<?=Util::baseUrl('about/'.$i.'/'.$about[$i][0],$lan)?>"><?=${$about[$i][1]}?></a></li>
			<?php 
				endfor;
			?>
		</ul>
	</div>
	<div id="main_content" class="static_page">
		<?php  
			switch($section):
		
				// About Us
				case 0:
		?>
			<h1>À propos d’Olive &amp; Olives</h1>
			<hr></hr>
			<p class="intro">Fondé en 2003 par deux épicuriennes, curieuses et femmes d’affaires – Claudia M. Pharand et Danièle Beauchamp –  Olive &amp; Olives travaille directement avec les producteurs afin d’offrir un produit authentique et véritable. </p>
			
			<img src="assets/images/PhiloFR.jpg" height="213" width="780" alt="">			
			<h2>Philosophie</h2>

		

			<p>C’est en collaborant avec ces artisans de la terre que nous arrivons à donner à l’huile d’olive extra vierge la place qui lui revient. Toutes les huiles que vous retrouvez, chez-nous sont issues de domaines ou de coopératives certifiés. Ces producteurs ont toute notre admiration, pour leur savoir-faire, leur loyauté et la perfection de leur art. Nous espérons vous transmettre ce savoir qu’ils partagent avec nous afin de vous aider à faire un choix juste et éclairé.</p>

			<h2 style="margin:0; padding: 0;">Siège social</h2>
			<p>8262, boulevard Pie IX<br>
			Montréal (Québec) H1Z 3T6 <br>
			<b>T 514 381-4020</b><br>
			<b>C</b> <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>			
			
			<?php 
					break;
				// News
				case 1:
			?>
					<h1>Olive &amp; Olives dans la presse</h1><hr>
					<p class="intro">
						Olive &amp; Olives est un concept de boutiques novateur au look chic et épuré, conçu précisément pour éviter le contact de la lumière directe sur les bouteilles et pour permettre aux gens de se déplacer avec aisance entre les présentoirs. Les huiles sont importées notamment d’Espagne (les produits les plus convoités), d’Italie, de Grèce, d’Australie, de Turquie et du Chili. 
					</p>
					<p>
						Différentes sortes d’olives sont offertes préemballées, assaisonnées de piments de Cayenne, de fines herbes ou d’ail. Les amateurs de saveurs gourmandes se délecteront aussi des croustilles à l’huile d’olive, des tapenades d’olive, des huiles d’olive aromatisées, des vinaigres haut de gamme, de même que des huiliers et des carafes.
					</p>
		<?php 
					include_once("_news.php");
					break;			
			endswitch;
		?>
	</div>	
</div>
