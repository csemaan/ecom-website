<div class="content_container clearfix">
	<div id="side_nav">
		<ul>
			<?php
				for($i=0; $i<count($policies); $i++) :
			?>
					<li class="<?=(($section==$i)?'cur_nav':'')?>"><a href="<?=Util::baseUrl('policies/'.$i.'/'.$policies[$i][0],$lan)?>"><?=${$policies[$i][1]}?></a></li>
			<?php
				endfor;
			?>
		</ul>
	</div>
	<div id="main_content" class="static_page">
		<?php
			switch($section):

				// PRIVACY POLICY
				case 0:
		?>
					<h1>Votre confidentialité est notre priorité</h1>
					<hr/>
					<p class="intro">Olive &amp; Olives s’engage à protéger vos données personnelles et votre vie privée. Nous n'utilisons les renseignements que vous nous communiquez que pour honorer vos commandes et pour vous transmettre par courriel les offres promotionnelles proposées dans oliveolives.com.</p>

					<h2>Les informations recueillies et leur utilisation</h2>

					<p>Pour vos commandes, nous avons besoin de connaître votre nom, votre adresse électronique et les adresses où les produits doivent être livrés et facturés ; ce qui nous permet de les traiter et d’en assurer le suivi dans le strict respect de nos procédures de sécurité. En aucun cas, Olive &amp; Olives n’utilisera les noms et adresses de ces destinataires à des fins promotionnelles.</p>

					<p>Nous vous demandons également votre numéro de téléphone, afin de vous joindre rapidement en cas de problème relatif au contenu de vos commandes ou à leur acheminement.</p>

					<p>Lorsque vous effectuez un achat dans oliveolives.com ou que vous nous adressez des commentaires qui nécessitent un retour d’information de notre part, votre adresse électronique est indispensable.</p>

					<p>Nous utilisons ces données pour améliorer la présentation et l’organisation de oliveolives.com ou pour vous aviser des changements et des nouveaux services que vous trouverez dans notre site. L'accès aux données est limité au groupe d'employés spécialement affectés à oliveolives.com et cela, uniquement dans l'exercice de leur travail. Si vous ne voulez plus recevoir nos courriels d’informations, il vous suffit de cliquer sur le lien « Désabonnement » de l’infolettre Olive &amp; Olives.</p>


					<h2>La protection de vos données confidentielles</h2>

					<p>Lorsque vous procédez à un achat en ligne dans oliveolives.com, les informations relatives à votre carte de crédit sont véhiculées sur Internet de façon sécuritaire tout au long du traitement de la transaction, grâce à des mécanismes de cryptographie (protocole Secure Sockets Layer) utilisés par votre fureteur, de même que par oliveolives.com. Aucune information relative à votre carte de crédit n'est conservée dans le site oliveolives.com : votre numéro de carte n’est uniquement accessible que dans le but de procéder au traitement de la transaction.</p>

					<p>De plus, nous observons de strictes mesures de sécurité en ce qui a trait à l’archivage et à la divulgation des informations que vous nous avez transmises, afin d’éviter tout accès non autorisé. De ce fait, il est possible que nous vous demandions de nouveau de prouver votre identité avant de vous livrer des informations confidentielles.</p>

					<h2>Les témoins ou cookies</h2>

					<p>Les témoins, communément appelés cookies sont des informations qu’enregistre un serveur http, sur le disque dur de votre ordinateur pour vous identifier. Nos témoins ne contiennent pas d’informations confidentielles, ils nous permettent seulement de procéder à diverses opérations, telles que le traitement de vos commandes ou la mémorisation des produits que vous avez sélectionnés lors de vos visites. La plupart des navigateurs acceptent automatiquement les témoins. Cependant, vous pouvez les refuser en modifiant « Vos préférences » dans votre ordinateur. Même sans témoin, vous conservez l’accès à la majorité des promotions dans notre site et vous pouvez transmettre vos commandes.</p>

					<h2>Transmission de vos coordonnées à des tiers</h2>

					<p>En vous garantissant la plus grande confidentialité, Olive &amp; Olives s’engage à ne pas divulguer, ni commercialiser à des tiers, quelque information que ce soit vous concernant ou concernant les destinataires de vos achats, sauf aux termes d’une contrainte légale (par témoignage en justice, interrogatoire, demande de documents, citation à comparaître, demande d’enquête au civil ou autre procédure semblable suivant une ordonnance d’un tribunal compétent, ou afin de se conformer aux conditions applicables imposées par un organisme gouvernemental ou une autre autorité de réglementation, ou autres obligations légales).</p>

					<p>Olive &amp; Olives se réserve le droit de modifier en tout temps sa politique de confidentialité. Dans ce cas, elle en avisera toutes les personnes concernées.</p>
			<?php
					break;

				// TERMS AND CONDITIONS (USE)
				case 1:
			?>
					<h1>Conditions d'utilisation</h1>
					<hr/>
					<h2>Disponibilité des produits</h2>
					<p>Olive &amp; Olives s’efforce de garder en stock une quantité suffisante de produits proposés dans son site Internet. Cependant, la disponibilité de ces produits ne fait l’objet d’aucune garantie.
					<p>Olive &amp; Olives se dégage de toute responsabilité en cas de rupture de stock.
					<p>Si Olive &amp; Olives se trouve dans l’impossibilité de livrer un produit commandé dans son site www.oliveolives.com, un message sera transmis par courriel ou par téléphone à l’acheteur. Dans ce cas, toute somme débitée par carte de crédit en faveur de www.oliveolives.com à l’égard de ces produits, y compris les frais de livraison s’il y a lieu, sera créditée dès que possible.

		 			<h2>Politique de prix</h2>
		 			<p>Les prix indiqués dans le site www.oliveolives.com sont les prix fixés pour la vente aux consommateurs.
					<p>Les taxes de vente applicables ne sont pas incluses dans les prix affichés. Elles seront ajoutées au montant de la commande au moment de passer à la caisse.
					<p>www.oliveolives.com se réserve le droit de modifier en tout temps le prix des produits offerts dans son site, et ce, sans préavis.
					<p>Tous les prix indiqués dans le site www.oliveolives.com sont exprimés en dollars canadiens.
					<p>Les prix des produits offerts dans le site www.oliveolives.com peuvent différer de ceux affichés dans les succursales d'Olive &amp; Olives.

		  			<h2>Livraison (au Canada seulement)*</h2>
		  			<p>Olive &amp; Olives assure la livraison au Canada seulement de tous ses produits par le biais de Postes Canada. Les commandes sont traitées dans un délai de 24 à 48 h du lundi au vendredi.
					<p>Une fois votre commande validée, Olive &amp; Olives prépare votre commande avec le plus grand soin.
					<p>Il faudra compter, en général, 2 à 10 jours ouvrables pour recevoir votre commande  dépendamment de l’adresse de livraison. Les frais varient selon le code postal du destinataire, le poids exact de la commande avant emballage et la taille du colis. Plusieurs facteurs influencent le poids d'une commande, dont : le type de bouteille (sa forme, l'épaisseur du verre, etc.). Le poids des produits-cadeaux inclut l'emballage-cadeau (ex. : boîte de métal) et les accessoires, s'il y a lieu.
					<p style="font-size:0.85em;">*Livraison gratuite pour tout achat de 60 $ et plus (avant les taxes et excluant les inscriptions aux cours et ateliers).</p>

		  			<h2>Modalités de paiement confidentialité et sécurité des transactions</h2>
		  			<p>Toutes les opérations bancaires sont traitées et sécurisées par PAYPAL. Les transactions s’effectuent sur le site de PAYPAL et donc nous n’avons accès, pour votre sécurité, à aucune information bancaire.

		  			<h2>Commande incomplète</h2>
		  			<p>Il arrive que nous ne puissions livrer un produit. Si la commande est incomplète parce qu'il manque un produit, Olive &amp; Olives rembourse le prix d'achat du produit et la portion des frais de livraison relatifs au produit (incluant les taxes) s’il y a lieu.</p>

		  			<h2>Produit en bon état</h2>
					<p>Un produit commandé par erreur et qui a été livré en bon état peut être échangé contre un autre ou remboursé. Cependant, le produit ne doit pas avoir été ouvert et il doit être réexpédié au bureau chef d’Olive &amp; Olives avec une preuve d'achat (courriel de confirmation ou facture imprimée par le biais du site) dans un délai de trente (30) jours. Si aucune preuve d'achat n'est fournie, le produit peut uniquement être échangé.
					<p>En aucun cas, Olive &amp; Olives ne rembourse les frais de livraison facturés par www.oliveolives.com pour l'échange ou le remboursement d'un produit en bon état.
					<p>Pour connaître la liste de toutes les succursales d'Olive &amp; Olives ainsi que leur adresse, <a href="<?=Util::baseUrl('magasins','fr') ?>">cliquez ici</a>.

		  			<h2>Produit défectueux</h2>
					<p>Un produit défectueux peut être échangé contre un autre. Le produit doit être réexpédié au bureau chef d’Olive &amp; Olives, accompagné d’une preuve d'achat (courriel de confirmation ou facture imprimée par le biais du site). Il doit être aux trois quarts plein et avoir été acheté moins de trente (30) jours auparavant.

		  			<h2>Produit manquant dans la commande livrée</h2>
		  			<p>Si un client reçoit une commande dans laquelle il manque un produit, il doit communiquer avec le service à la clientèle d'Olive &amp; Olives. Deux options se présentent à lui :</p>
				  	<ul>
					  	<li>Si le client désire que le produit manquant soit remplacé, www.oliveolives.com lui fait parvenir le produit et assume tous les frais de livraison relatifs à ce nouvel envoi.</li>
					 	<li>S'il ne désire plus recevoir le produit ou s'il n'en reste plus en stock, www.oliveolives.com rembourse le prix d'achat du produit et la totalité des frais de livraison payés pour l'ensemble de la commande (incluant les taxes).</li>
				  	</ul>

		 			<h2>Politique d’annulation</h2>
					<p>Olive &amp; Olives se réserve le droit d’annuler un atelier ou un cours de cuisine en cas de nombre d'inscriptions insuffisant et/ou pour des raisons hors de son contrôle.
					<p>Olive &amp; Olives s’engage à informer le client d’une éventuelle annulation par téléphone dans les meilleurs délais. Si l’atelier ou le cours de cuisine ne peut être déplacé à une date ultérieure qui convient au client, celui-ci sera entièrement remboursé.

					<h2>Annulation par le client</h2>
					<p>Tout participant ayant réservé pour assister à un atelier ou un cours de cuisine et souhaitant annuler, l’annulation doit nous être transmise par téléphone en communiquant au (514) 381-4020 ou par courriel à <a href="mailto:info@oliveolives.com">info@oliveolives.com</a>.
					<p>Si un participant est incapable d’assister à un cours il doit aviser Olive &amp; Olives sept (7) jours avant l'événement afin de pouvoir s’inscrire à un autre événement ou recevoir une note de crédit.
					<p>Pour toute annulation reçue à moins de sept (7) jours d’avis précédant l’événement, Olive &amp; Olives ne pourra émettre une nouvelle inscription ou une note de crédit. Auncun remboursement.

					<h2>Éthique</h2>
					<p>Un message écrit peut être rajouté au colis s’il s’agit d’un cadeau. Olive &amp; Olives se réserve le droit de vérifier et de refuser l’ajout d’un message non-conforme à une certaine éthique.</p>

					<h2>Informations sur l'exploitant</h2>
					<p>Le site www.oliveolives.com est exploité par Distributions Olive &amp; Olives inc. Les conditions de vente décrites ci-dessus constituent un contrat entre le client et Olive &amp; Olives. Olive &amp; Olives se réserve le droit de modifier en tout temps la présente politique, avec prise d’effet immédiate dès l’affichage sur le site, sans aucune responsabilité envers vous ou toute autre personne.</p>

		  			<h2>Adresse légale de l'exploitant</h2>

					<p>Distributions Olive &amp; Olives inc.<br>
					8262 boul. Pie IX<br>
					Montréal (Québec) H1Z 3T6</p>

					<p>Numéro de téléphone :<br>
					(514) 381-4020<br>
					Courriel :<a href="mailto:info@oliveolives.com"><b>info@oliveolives.com</b></a>.<p>

		  			<h2>Protégez-vous contre les courriels frauduleux</h2>

		  			<p>L’envoi de courriels frauduleux (hameçonnage ou phishing*) est un procédé visant à détourner des fonds en usurpant l’identité d’entreprises commerciales bien établies. Ainsi, des courriels en apparence authentiques sont expédiés par des faussaires pour tenter de soutirer des informations personnelles aux destinataires.</p>
		  			<p>Afin de vous protéger contre ce type d’envois, Olive &amp; Olives tient à vous informer qu’elle ne communiquera jamais avec vous par courriel pour réclamer des informations personnelles telles que vos coordonnées, votre date de naissance ou votre numéro de carte de crédit.</p>
		  			<p>Si jamais vous recevez un courriel qui semble provenir d'Olive &amp; Olives et qui vous réclame de telles informations, ne répondez pas! Communiquez immédiatement avec le service à la clientèle d’Olive &amp; Olives pour signaler l’incident au 514-381-4020 ou à <a href="mailto:info@oliveolives.com"><b>info@oliveolives.com</b></a></p>
		  			<p><em>*L’Office québécois de la langue française propose la traduction hameçonnage pour le terme anglais phishing. Ce mot décrit : « l’envoi massif d’un faux courriel, apparemment authentique, utilisant l’identité d'une institution financière ou d'un site commercial connu, dans lequel on demande aux destinataires, sous différents prétextes, de mettre à jour leurs coordonnées bancaires ou personnelles, en cliquant sur un lien menant vers un faux site Web, copie conforme du site de l'institution ou de l'entreprise, où le pirate récupère ces informations, dans le but de les utiliser pour détourner des fonds à son avantage. »
		  				<br>Source : Office québécois de la langue française.</em></p>
			<?php
					break;

				// CANCELLATION
				case 2:
			?>
					<h1>Politique d'annulation</h1>
					<hr/>

		  			<p class="intro">Olive &amp; Olives se réserve le droit d’annuler un atelier ou un cours de cuisine en cas de nombre d'inscrits insuffisant et/ou pour des raisons hors de son contrôle.</P>

					<p class="intro">Olive &amp; Olives s’engage à informer le client d’une éventuelle annulation par téléphone dans les meilleurs délais. Si l’atelier ou le cours de cuisine ne peut être déplacé à une date ultérieure qui convient au client, celui-ci sera entièrement remboursé.</p>

					<h2>Annulation par le client</h2>

		  			<p>Tout participant ayant réservé pour un atelier ou un cours de cuisine et souhaitant annuler, l’annulation doit se faire par téléphone en communiquant au 514-381-4020 ou par courriel à <a href="maito:info@oliveolives.com">info@oliveolives.com</a>.</p>

		  			<p>Si un participant est incapable d’assister à un cours il doit aviser Olive &amp; Olives 7 jours avant l'évènement afin de pouvoir s’inscrire à un autre événement ou recevoir une note de crédit.</p>

		  			<p>Pour toute annulation reçue à moins de 7 jours d’avis précédant l’événement, Olive &amp; Olives ne pourra émettre une nouvelle inscription ou une note de crédit. Aucun remboursement.</p>

		  			<h2>Informations sur l'exploitant</h2>

		  			<p>Le site www.oliveolives.com est exploité par la Distributions Olive &amp; Olives inc. Les conditions de vente décrites ci-dessus constituent un contrat entre le client et Olive &amp; Olives. Ces conditions peuvent être modifiées en tout temps par Olive &amp; Olives.</p>
			<?php
					break;

				// SHIPPING
				case 3:
			?>
					<h1>Information d’expédition</h1>
					<hr/>
					<h2>Livraison (au Canada seulement)</h2>
					<!-- <div class="shipping-alert">
						<p>Madame, Monsieur,</p>
						<p>La grève chez Postes Canada engendre une suspension de tous leurs services pour une durée indéterminée. Afin de vous offrir un service exemplaire, le transport de votre commande sera assuré par FedEx.</p>
						<p>En vous remerciant et en souhaitant que la situation se résorbe rapidement,</p>
						<p>L’équipe d’Olive &amp; Olives</p>
					</div> -->
					<p>Olive &amp; Olives assure la livraison au Canada seulement de tous ses produits par le biais de Postes Canada. Les commandes sont traitées dans un délai de 24 à 48 h du lundi au vendredi.
					<p>Une fois votre commande validée, Olive &amp; Olives prépare votre commande avec le plus grand soin.
					<p>Il faudra compter, en général, 2 à 10 jours ouvrables pour recevoir votre commande  dépendamment de l’adresse de livraison. Les frais varient selon le code postal du destinataire, le poids exact de la commande avant emballage et la taille du colis. Plusieurs facteurs influencent le poids d'une commande, dont : le type de bouteille (sa forme, l'épaisseur du verre, etc.). Le poids des produits-cadeaux inclut l'emballage-cadeau (ex. : boîte de métal) et les accessoires, s'il y a lieu.
					<p><b>Livraison gratuite pour tout achat de 60 $ et plus</b> (avant les taxes et excluant les inscriptions aux cours et ateliers).
			<?php
					break;
			?>

		<?php
			endswitch;
		?>
	</div>
</div>
