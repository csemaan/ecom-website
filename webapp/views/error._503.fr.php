<div class="secondary_nav">
	<h2>Nous serons de retour dans quelques minutes!</h2>	
</div>
<div class="content_container clearfix">
	<div id="error404_cont">
		<div id="error404">Courte pause de maintenance ...</div>
		<hr id="error_rule">
		<div id="error404_text">
			<p>
				Nous procédons en ce moment à la maintenance du site Web.
			</p>
			<p>
				Le site Web sera de retour en ligne dans quelques minutes. 
				Si vous avez des questions, vous pouvez toujours nous écrire par 
				<a href="mailto:info@oliveolives.com" target="_blank">courriel</a> et il nous 
				fera plaisir de vous répondre.
			</p>
		</div>
	</div>
</div>