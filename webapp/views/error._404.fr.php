<div class="secondary_nav">
	<h2>Oups!</h2>	
</div>
<div class="content_container clearfix">
	<div id="error404_cont">
		<div id="error404">Une erreur est survenue ...</div>
		<hr id="error_rule">
		<div id="error404_text">
			<p>
				La page à laquelle vous tentez d’accéder n’existe plus ou a été déplacée. 
				Nous avons récemment fait la mise en ligne du nouveau design de notre site Web, 
				il se peut donc que certains signets ne fonctionnent plus.
			</p>
			<p>
				Trouver ce que vous recherchez a l’aide de la navigation principale de cette page. 
				Si vous avez des questions, vous pouvez toujours nous écrire par 
				<a href="mailto:info@oliveolives.com" target="_blank">courriel</a> et il nous 
				fera plaisir de vous répondre.
			</p>
		</div>
	</div>
</div>