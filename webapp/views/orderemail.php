<style type="text/css">
    body,td { color:#2f2f2f; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; }
</style>

<div style="font:11px/1.35em Verdana, Arial, Helvetica, sans-serif;">
	<table cellspacing="0" cellpadding="0" border="0" width="98%" style="margin-top:10px; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; margin-bottom:10px;">
		<tr>
			<td align="center" valign="top">
				<!-- [ header starts here] -->
				<table cellspacing="0" cellpadding="0" border="0" width="650">
					<tr>
						<td valign="top"><a href="http://www.oliveolives.com/fr/"><img src="https://www.oliveolives.com/skin/frontend/default/olive/images/logo_email.gif" alt="Magento"  style="margin-bottom:10px;" border="0"/></a></td>
					</tr>
				</table>
				<!-- [ middle starts here] -->
				<table cellspacing="0" cellpadding="0" border="0" width="650">
					<tr>
						<td valign="top">
							<p>
								<strong>Bonjour Camille Semaan</strong>,
								<br/>Merci d&rsquo;avoir utilis&eacute; la boutique en ligne d&rsquo;Olive &amp; Olives. Une fois que votre commande sera envoy&eacute;e, nous vous enverrons un courriel contenant un lien vous permettant de suivre l&rsquo;envoi. Si vous avez des questions concernant votre achat, veuillez nous &eacute;crire &agrave; <a href="mailto:commande@oliveolives.com" style="color:#1E7EC8;">commande@oliveolives.com</a> ou appelez-nous au <span class="nobr">(514) 38180934020</span> du lundi au vendredi, de 9&nbsp;h &agrave; 18&nbsp;h HNE.
							</p>
							<p>Voici la confirmation de votre commande. Merci.</p>
							<h3 style="border-bottom:2px solid #eee; font-size:1.05em; padding-bottom:1px; ">
								Votre commande #300000678 <small>(complétée le 12 septembre 2013)</small>
							</h3>
							<table cellspacing="0" cellpadding="0" border="0" width="100%">
								<thead>
									<tr>
										<th align="left" width="48.5%" bgcolor="#eaf1d3" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;">Adresse de facturation&nbsp;:</th>
										<th width="3%"></th>
										<th align="left" width="48.5%" bgcolor="#eaf1d3" style="padding:5px 9px 6px 9px; border:1px solid		 #bebcb7; border-bottom:none; line-height:1em;">M&eacute;thode de paiement&nbsp;:</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;">Camille Semaan<br/>228 Mozart Est<br/>Montréal, Quebec, H2S1B6<br/>Canada<br/>T: 5149484181<br />
										<a href="mailto:camille@xvitae.com">camille@xvitae.com</a></td>
										<td>&nbsp;</td>
										<td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;">
											<strong>Type de carte de paiement :</strong><span class="nowrap">Visa</span><br />
											<strong>Numéro de carte de paiement :</strong> <span class="nowrap">xxxx-1681</span><br />
											Date d'expiration : 03/2017
										</td>
									</tr>
								</tbody>
							</table>
							<br/>
							<table cellspacing="0" cellpadding="0" border="0" width="100%">
								<thead>
									<tr>
										<th align="left" width="48.5%" bgcolor="#eaf1d3" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;">Adresse de livraison&nbsp;:</th>
										<th width="3%"></th>
										<th align="left" width="48.5%" bgcolor="#eaf1d3" style="padding:5px 9px 6px 9px;border:1px solid #bebcb7; border-bottom:none; line-height:1em;">Mode de livraison&nbsp;:</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;">
											Camille Semaan<br/>
											228 Mozart Est<br />
											Montréal,  Quebec, H2S1B6<br/>
											Canada<br/>
											T: 5149484181&nbsp;
										</td>
										<td>&nbsp;</td>
										<td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;">
											Federal Express - Terrestre&nbsp;
										</td>
									</tr>
								</tbody>
							</table>
							<br/>
							<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border:1px solid #bebcb7; background:#f8f7f5;">
								<thead>
									<tr>
										<th align="left" bgcolor="#eaf1d3" style="padding:3px 9px">Article</th>
										<th align="left" bgcolor="#eaf1d3" style="padding:3px 9px">Réf.</th>
										<th align="center" bgcolor="#eaf1d3" style="padding:3px 9px">Quantité</th>
										<th align="right" bgcolor="#eaf1d3" style="padding:3px 9px">Sous-total</th>
									</tr>
								</thead>
								<tbody bgcolor="#eeeded">
									<tr>
										<td align="left" valign="top" style="padding:3px 9px">
										<strong>Boromeo Infusée - Chili</strong>
									</td>
	    							<td align="left" valign="top" style="padding:3px 9px">158p</td>
								    <td align="center" valign="top" style="padding:3px 9px">1</td>
								    <td align="right" valign="top" style="padding:3px 9px">
								    	<span class="price">13,95 $CAD</span>
								    </td>
								</tr>
								</tbody>
								<tbody>
									<tr>
									    <td align="left" valign="top" style="padding:3px 9px">
		        							<strong>Leonardi Crema - Orange</strong>
		        						</td>
		        						<td align="left" valign="top" style="padding:3px 9px">2299OR</td>
									    <td align="center" valign="top" style="padding:3px 9px">1</td>
									    <td align="right" valign="top" style="padding:3px 9px">
		                                    <span class="price">7,95 $CAD</span>
		                                </td>
		                            </tr>
		                        </tbody>
		                        <tbody bgcolor="#eeeded">
		                        	<tr>
		                        		<td align="left" valign="top" style="padding:3px 9px">
											<strong>Cours de cuisine à l'huile d'olive avec Jean-Michel Riverin</strong><br />
											Mercredi 2 octobre 2013 à 18&nbsp;h&nbsp;30 - 3127 rue Masson, Montréal
										</td>
									    <td align="left" valign="top" style="padding:3px 9px">131002</td>
									    <td align="center" valign="top" style="padding:3px 9px">1</td>
									    <td align="right" valign="top" style="padding:3px 9px">
									    	<span class="price">45,00 $CAD</span>
									    </td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="3" align="right" style="padding:3px 9px">Sous-total</td>
		            					<td align="right" style="padding:3px 9px"><span class="price">66,90 $CAD</span></td>
		        					</tr>
		                        	<tr>
			            				<td colspan="3" align="right" style="padding:3px 9px">Livraison et traitement</td>
			            				<td align="right" style="padding:3px 9px"><span class="price">0,00 $CAD</span></td>
			            			</tr>
			                    	<tr>
							            <td colspan="3" align="right" style="padding:3px 9px">TPS+TVQ (14.975%)</td>
							            <td align="right" style="padding:3px 9px"><span class="price">6,73 $CAD</span></td>
			        				</tr>
							        <tr bgcolor="#eaf1d3">
							        	<td colspan="3" align="right" style="padding:3px 9px"><strong><big>Montant global</big></strong></td>
							        	<td align="right" style="padding:6px 9px"><strong><big><span class="price">73,63 $CAD</span></big></strong></td>
							        </tr>
		    					</tfoot>
		    				</table>
	    					<br/>
	                    	<p>Nous vous remercions,<br/><strong>Olive & Olives</strong></p>
	                	</td>
	            	</tr>
	        	</table>
	    	</td>
		</tr>
	</table>
</div>