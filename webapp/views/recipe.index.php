<?php include_once("fblike.inc.php"); ?>
<div class="secondary_nav">
	<h2><?=$rcd_catchphrase?></h2>
</div>
<div class="content_container">
	<div class="pagination clearfix">			
		<a href="<?=Util::baseUrl('recipes/kw',$lan)?>"><img src="assets/images/grey_left_arrow.gif" height="16" width="14" alt=""><?=$com_backtolist?></a>
		<?php 
			if(isset($recipesIdSet) && count($recipesIdSet)>1): 
			$currentIndexInSet = array_search($recipe['rcp_id'], $recipesIdSet);
		?>
			<a href="<?=Util::baseUrl('recipe/'.$recipesIdSet[gmp_mod($currentIndexInSet-1,count($recipesIdSet))],$lan)?>" id="prev"><img src="assets/images/grey_left_arrow.gif" height="16" width="14" alt=""></a>
			<a href="<?=Util::baseUrl('recipe/'.$recipesIdSet[gmp_mod($currentIndexInSet+1,count($recipesIdSet))],$lan)?>" id="next"><img src="assets/images/grey_right_arrow.gif" height="16" width="14" alt=""></a>
		<?php endif; ?>
		<!--
		<div id="prev_text" class="clearfix">Previous Product</div>
		<div id="next_text" class="clearfix">Next Product</div>	
		-->
	</div>
</div>
<div class="content_container clearfix">
	<div id="recipe_details" class="clearfix">
		<h1><?=$recipe['rcd_title']?></h1>
		<h2></h2>
		<span class="">
			<h3><?=$rcd_ingredients_label?></h3>
			<p>
				<?=$recipe['rcd_ingredients'];?>
			</p>
			<h3><?=$rcd_preparation_label?></h3>
			<p>
				<?=str_replace("<br>",'<br/><br/>',$recipe['rcd_preparation']);?>
			</p>
		</span>
		
	</div>
	<div class="share_container clearfix"> 
		<img src="data/images/recipe/<?=$recipe['rcp_img_large']?>" height="298" width="300" alt="" class="recipe_large postcard">
		<span class="share_details">
			<p class="pr_size"><?=$rcd_suggest_recipe?></p>
			<a href="javascript:void(0);" data-popupurl="<?=Util::makeMailtoUrl($com_share_subject,$com_share_body);?>" class="cs_popuplink pr_button">+ <?=$com_share?></a> 
			<a href="javascript:void(0);" data-popupurl="<?=Util::baseUrl('printrecipe/'.$recipe['rcp_id'],$lan)?>" class="cs_popuplink pr_button"><?=$com_print?></a> 
			<div class="fb-like" data-href="<?='http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']?>" data-width="200" data-layout="button_count" data-show-faces="false" data-send="false"></div>
		</span>
		
		<?php if(isset($relatedProducts)): ?>
			<span class="related_products">				
				<p class="title"><?=$rcd_related_products?></p>
				<?php 
					foreach ($relatedProducts as $relatedProd): 
				?>
						<p class="sub_title"><?=$relatedProd['catName']?><?=(($lan=='fr')?' ':'')?>:</p>
				<?php 
						foreach ($relatedProd['products'] as $product):
				?>
							<a href="<?=Util::baseUrl('product/'.$product['prd_id'].'/'.$product['pdd_slug'],$lan)?>"><?=$product["pdd_name"]?></a>
				<?php
						endforeach;
					endforeach;
				?>
			</span>
		<?php endif ?>
	</div>


</div>