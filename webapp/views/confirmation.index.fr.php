<div class="secondary_nav">
	<h2><img src="assets/images/truck.png" height="29" width="62" alt="">Livraison gratuite pour toute commande supérieure à 60 $.</h2>	
</div>
<div class="content_container clearfix">
	<?php 
		if (isset($orderDetail) && $orderDetail): 
			// Google Analytics E-Commerce Tracking Code
			echo $gaTrackingString;
	?>
	<div id="shopping_cart">
		<h1>Merci !</h1>
		<h2></h2>
		<h4>Votre numéro de commande Olive &amp; Olives :  <?=$orderDetail["order"][0]["ord_number"]?></h4>		
		<p>
			Nous vous remercions pour votre commande. Vous recevrez sous peu un message de confirmation 
			de la commande par courriel, à l'adresse que vous avez spécifiée (<?=$orderDetail["order"][0]["cli_email"]?>). 
			Si vous avez des questions, n'hésitez pas à nous écrire par <a href="mailto:info@oliveolives.com">courriel</a> 
			ou nous appeler au <b style="white-space: nowrap">(514) 381-4020</b>.
		</p>

		<?php if (isset($orderDetail['product'])): ?>
			<p>
				Nous expédierons votre colis le plus rapidement possible. Prévoir le délai de livraison standard de 
				Postes Canada, qui est de <?=$_SESSION["pcShippingTransitTime"]+1?> jours ouvrables pour votre adresse.
			</p>
		<?php endif; ?>
	</div>
	<?php 
		$checkoutTmpl = "";
		if (isset($orderDetail['product'])):
			foreach ($orderDetail['product'] as $product):
				$checkoutTmpl .='<tr class="row">
									<td class="product">'.$product['pdd_name'].'</td>
									<td class="quantity">'.$product['odd_quantity'].'</td>
									<td class="price">'.Util::formatPrice($product['odd_price']*$product['odd_quantity'],$lan).'</td>
								</tr>';
			endforeach;
		endif;
		if (isset($orderDetail['class'])):
			foreach ($orderDetail['class'] as $class):
				$checkoutTmpl .='<tr class="row">
									<td class="product">'.$class['cls_title'].'</td>
									<td class="quantity">'.$class['odd_quantity'].'</td>
									<td class="price">'.Util::formatPrice($class['odd_price']*$class['odd_quantity'],$lan).'</td>
								</tr>';
			endforeach;
		endif;
	?>
	<div id="cart_total">
		<table class="receipt">
			<tr id="header">
				<td class="product"><?=$crt_lbl_item?></td>
				<td class="quantity"><?=$crt_lbl_qty?></td>
				<td class="price"><?=$crt_lbl_price?></td>
			</tr>
			<?=$checkoutTmpl?>
		</table>				
		<table class="receipt">							
			<tr class="row">
				<td class="rec_label"><?=$crt_lbl_subtotal?></td>
				<td class="sub"><?=Util::formatPrice($orderDetail["order"][0]["ord_total"]-$orderDetail["order"][0]["ord_taxes"]-$orderDetail["order"][0]["ord_shippingfees"],$lan)?></td>
			</tr>
			<tr class="row">
				<td class="rec_label"><?=$crt_lbl_shipping?></td>
				<td class="shipping cs_shippingAmount cs_tbc_label"><?=Util::formatPrice($orderDetail["order"][0]["ord_shippingfees"],$lan)?></td>
			</tr>			
			<tr class="row taxes">
				<td class="rec_label"><?=$crt_lbl_taxes?></td>
				<td class="tax cs_taxesAmount cs_tbc_label"><?=Util::formatPrice($orderDetail["order"][0]["ord_taxes"],$lan)?></td>
			</tr>	
			<tr class="row">
				<td class="rec_label last"><?=$crt_lbl_total?></td>
				<td class="total last cs_totalAmount cs_tbc_label"><?=Util::formatPrice($orderDetail["order"][0]["ord_total"],$lan)?></td>
			</tr>									
		</table>
	</div>
	<?php else: ?>
	<div>
		<h4>Votre commande n'a pu être complétée. Pour plus d'information, n'hésitez pas à nous écrire par <a href="mailto:info@oliveolives.com">courriel</a> ou à nous appeler au <b>1-800 555-1234</b>.</h4>
	</div>
	<?php endif; ?>
</div>			
