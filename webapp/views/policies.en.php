<div class="content_container clearfix">
	<div id="side_nav">
		<ul>
			<li class="<?=(($section==='privacy')?'cur_nav':'')?>"><a href="<?=Util::baseUrl('policies/privacy',$lan)?>">Privacy policy</a></li>
			<li class="<?=(($section==='use')?'cur_nav':'')?>"><a href="<?=Util::baseUrl('policies/use',$lan)?>">Terms and conditions</a></li>
			<li class="<?=(($section==='cancellation')?'cur_nav':'')?>"><a href="<?=Util::baseUrl('policies/cancellation',$lan)?>">Cancellation policy</a></li>
			<li class="<?=(($section==='shipping')?'cur_nav':'')?>"><a href="<?=Util::baseUrl('policies/shipping',$lan)?>">Shipping information</a></li>
		</ul>
	</div>
	<div id="main_content" class="static_page">
		<?php  
			switch($section):
		?>
			<?php
				case "cancellation":
			?>
					<h1>Our Cancellation Policy</h1>
					<hr/>
					<p class="intro">Olive &amp; Olives reserves the right to cancel a course due to insufficient enrolment or due to factors out of their control. Participants will be notified ahead of time by telephone. If the workshop or class cannot be moved to a later date that is convenient to the client, the class will be fully refunded. </p>
					<p>If a participant wishes to cancel a workshop or class, Olive &amp; Olives must be informed by telephone or email seven days prior to the event in order to substitute for another date or to be eligible for a store credit. </p>
					<p>If cancellation notices are received less than seven days before the event, Olive &amp; Olives is unable to issue a store credit or to substitute with another class. No refund.</p>
			<?php 
					break;
			?>
			<?php
				case "cancellation":
			?>
					<h1>Confidentiality is our priority</h1>
					<hr/>
					<p class="intro">Olive &amp; Olives is committed to protecting your personal information and privacy. The information you give us is used strictly to fill your orders or e-mail you special promotions at oliveolives.com.</p>
					<h2>How information is collected and used</h2>
					<p>To fill an order, we need to know your name, your e-mail address and the shipping and billing addresses. This enables us to process, and keep track of your orders while observing the strictest security procedures. At no time will Olive &amp; Olives or oliveolives.com use these recipients' names and addresses for promotional purposes.</p>
					<p>The information we request includes your telephone number so we can contact you rapidly in case of a problem regarding the content of your orders or their forwarding.</p>
					<p>Your e-mail address is essential when you place an order or when you send us comments by e-mail.</p>
					<p>The information is also used to enhance Olive &amp; Olives.com presentation and organization of Olive &amp; Olives.com site as well as to let you know about changes and new services found on our site. If you no longer wish to receive e-mail information, simply click on the "Unsubscribe" link at the bottom of Olive &amp; Olives e-Newsletter’s.</p>
					<p><b>Protection of your confidential information</b></p>
					<p>When purchasing online on oliveolives.com, your credit card information is securely transmitted on the Internet every step of the transaction treatment due to an encryption method (Secure Sockets Layer protocol) used by both your Web browser and by oliveolives.com. No credit card information is kept on Olive &amp; Olives web site, your card number is therefore accessible for the sole purpose of processing the transaction.</p>
					<p>Moreover, in order to limit unauthorised access, we observe strict security measures when it comes to archiving and divulging any information provided by our customers. Because of that, we may ask you to confirm your identity once again before submitting personal information.</p>
					<p><b>Cookies</b></p>
					<p>Cookies are not just sweet treats; they are also information packets recorded by an http server on the hard disk of your computer for purposes of identification. Our cookies contain no confidential information; they allow us to carry out a number of operations such as processing your orders or storing products that you have selected from our site. The majority of net users automatically accept cookies. However, you can refuse them. The choice is yours. Even without cookies, you retain access to the majority of promotions on our site and the ability to place orders.</p>
					<p><b>Disclosure of your personal information to third parties</b></p>
					<p>By guaranteeing the utmost confidentiality, Olive &amp; Olives and oliveolives.com undertake to not disclose or market any information whatsoever regarding you or the recipient of your purchases. However, in case of theft or fraudulent use of your card, we will cooperate with police services in the investigations generally conducted in such circumstances</p>
					<p>Olive &amp; Olives reserves the right to modify its privacy policy at any time. In the event Olive &amp; Olives modifies its privacy policy, all subscribers and customers will be notified ahead of time.</p>
			<?php 
					break;
			?>
			<?php
				case "use":
			?>
					<h1>Our Terms and Conditions</h1>
					<hr></hr>

		  			<p class="intro">Olive &amp; Olives makes every effort to keep a sufficient inventory of the products offered on its Web site. However, the availability of these products is not guaranteed.</p>

					<h3>Online product availability</h3>

		  			<p>Olive &amp; Olives waives any liability in case of inventory shortages.</p>

		  			<p>If Olive &amp; Olives is unable to deliver a product ordered through its www.oliveolives.com site, the buyer will be notified accordingly by e-mail or telephone. If such is the case, any amount charged to your credit card for products, including delivery charges, will be credited as soon as possible.</p>
		  
		 			<h2>Price policy</h2>
		  
		  			<p>The prices indicated on the www.oliveolives.com site are those established for consumer sales.</p>

		  			<p>The applicable sales taxes are not included in the prices displayed. They will be added to the amount of the order at the moment of check out.</p>

		  			<p>www.oliveolives.com reserves the right to modify the prices of products available on its site at any time, without prior notice.</p>

					<p>All prices quoted on the www.oliveolives.com site are in Canadian dollars.</p>
		  
		  			<p>The prices of products available on www.oliveolives.com may differ from those posted in Olive &amp; Olives outlets.</p>

		  			<h2>Delivery charges and services</h2>
		  
		  			<p>www.oliveolives.com delivers through Fedex. Orders are delivered within 3 business days. Fedex however does not guarantee this delivery time in remote areas.</p>
		  
		  			<h2>Payment options</h2>

		  			<p>Credit cards are the only form of payment accepted for online buying on Olive &amp; Olives Web site. We honour Visa and MasterCard.</p>

		  			<h2>Incomplete order</h2>

		  			<p>In the event that we are unable to deliver a product, Olive &amp; Olives refunds the purchase price of the product as well as applicable delivery charges for that product (including taxes).</p>

		  			<h2>Product in Good Condition</h2>

		  			<p>A product ordered by mistake, provided it was in good condition at the time of receipt, may be exchanged or refunded. However, the product must be sealed and resent with a proof of purchase (copy of e-mailed confirmation or invoice printed through the site) within 30 days. If no proof of purchase is presented, the product may only be exchanged.</p>

		  			<p>Olive &amp; Olives does not refund delivery charges billed by www.oliveolives.com for a product delivered in good condition that is exchanged or reimbursed.</p>

		  			<p>To obtain a list of all Olive &amp; Olives outlets and their addresses, click<a href="http://www.oliveolives.com/en/about-us/our-addresses.html">here</a>.</p>

		  			<h2>Defective Product</h2>

		  			<p>A defective product may be exchanged. the product must be resent with a proof of purchase (copy of e-mailed confirmation or invoice printed through the site). It must be at least three-quarters full and bought within the 30 days.</p>
		 
		  			<h2>Missing Product in the Delivered Order</h2>

		  			<p>A client must contact Olive &amp; Olives Customer Service if there is a missing product in the order. Two options are available:</p>

				  	<ul>
					  	<li>If the client wishes to replace the missing bottle, www.oliveolives.com sends the product and covers all delivery charges relative to the new shipment.</li>
					 	 <li>If the client no longer wants the product or if there aren't any left in stock, www.oliveolives.com reimburses the purchase price of the product as well as all the delivery charges for the entire order (including taxes).</li>
				  	</ul>

		 			<h2>About the operator</h2>

		 			<p>The www.oliveolives.com website is operated by Distributions Olive &amp; Olives inc. The sales conditions mentioned above act as a contract between the customer and Olive &amp; Olives. The sales conditions can be modified at any time by Olive &amp; Olives.</p>

		  			<h2>Legal operating address :</h2>

					<p>Distributions Olive &amp; Olives inc.<br>
					4571 East Metropolitain boul.<br>
					Montreal (Quebec)<br>
					H1R 1Z4</p>

					<p>Phone number:<br>
					514 381-4020<br>
					E-mail:<a href="mailto:info@oliveolives.com"><b>info@oliveolives.com</b></a>.<p>
		  
		  			<h3>Delivery Charges</h3>

		  			<p>Calculating Delivery Charges and Examples</p>

		  			<p>Delivery charges are calculated based on Fedex&rsquo;s preferential corporate rates for Olive &amp; Olives. These charges vary depending on the postal code, the order&rsquo;s exact weight before packaging and the package size.</p>

					<p>A number of factors influence the weight of an order, such as the type of bottle (shape, thickness of glass, etc.). The weight of gift sets includes gift packaging (e.g., metal box) and accessories (e.g., glasses) when applicable. Taxes paid by customers are calculated based on discounted delivery charges.</p>

		  			<p>Note that the higher the number of bottles ordered, the less costly the delivery charges per unit will be.</p>
		  
		  			<h2>Transaction security</h2>

		  			<p><b>Payment service</b></p>

		  			<p>The www.oliveolives.com payment service allows you to make on-the-spot purchases by credit card. This service guarantees a high level of security, reliability and efficiency.</p>

		  			<p>www.oliveolives.com offers a number of features that ensure your financial transaction is processed securely, including the following :</p>
				 	<ul>
						<li>Secure transmission of the credit card number to the payment server, which redirects the transaction to the institution that issued the card for authorization.</ul>
				  		<li>The information transmitted on the Internet is encrypted.</ul>
				  		<li>The request for authorization for the amount of the purchase is processed immediately.</li>
				  	</ul>

		  			<p>The payment service is easy to use and free of charge. All you need is one of the major credit cards.</p>

		  			<h2>Security information</h2>

		  			<p>The payment service has a number of security-related features. Additional information is given on the security mechanisms related to the payment form and your navigator's encryption details in the following sections.</p>

		  			<h2>Encoding and browsers</h2>

		  			<p>Encoding consists in turning a clearly worded text into an unintelligible text (cryptogram) using cryptographic techniques such as algorithms and keys. Access to a secret key is necessary to decode the information. Cryptographic systems use a variable length key. You can rest assured that transmission of information is totally secure:</p>

					<ul>
					  <li>The encoded mode is confirmed by a padlock in the bottom right corner of the screen.</li>
					</ul>

		  			<h2>Cryptography</h2>

		  			<p>Cryptography is the science of encoding messages so that the content can be understood only by the designated recipient. Encoding in various forms has been used since time immemorial.</p>

		  			<p>Today, encoding is used to transmit digital data via computer network or, increasingly, to ensure confidentiality of important files stored on your computer. The clearly worded text is turned into an unintelligible text using complex mathematical algorithms and cryptograms known as keys. A reverse operation is applied for decoding. Security experts foresee a growing use of encoding for non-classified computer data.</p>

		  			<p>The www.oliveolives.com payment service uses asymmetrical encoding, which ensures secured transmission of your credit card number over the Internet.</p>
		 
		  			<h2>Paying for your purchase</h2>

		  			<p>At the time of payment, www.oliveolives.com displays a form on which you are asked to enter your credit card number, year/month of expiration, as well as the CVV2 code.</p>

		  			<h2>IMPORTANT NOTICE on the CVV2 code</h2>
		 
		 			<p>Most major credit cards offer an added value in terms of security for online purchases, called the CVV2 code. This code is a group of 3 or 4 numbers (embossed on the front of the card, or printed on the signature strip) that allows a cryptographic control of the information and protects the card owner against fraud. When purchasing online, entering these numbers validates the authenticity of the card. The CVV2 code is not contained on the magnetic stripe of the card, therefore is not included on the transaction receipt.<p>

		  			<p>The information you enter on this form is encrypted before being transmitted on the Internet, in order to guarantee its confidentiality. It is then transmitted, for immediate processing, to the financial institution. Consequently, you are guaranteed that the information related to your credit card is kept strictly confidential.</p>

		  			<h2>Beware of fraudulent e-mails</h2>

		  			<p>The distribution of fraudulent e-mails (phishing*) is a technique used with the intention to reroute funds by usurping well-established company identities. To do so, authentic looking e-mails are sent by counterfeiters to retrieve personal information from the recipients.</p>

		  			<p>To protect customers, Olive &amp; Olives insists on informing you that no such e-mail will ever be sent to you requesting personal information such as your address, date of birth or credit card number.</p>

		  			<p>If you ever encounter an e-mail that seems to originate from Olive &amp; Olives and asks for personal information, do not answer! Immediately contact customer service to report the incident at 514 381-4020 or at <a href="mailto:info@oliveolives.com"><b>info@oliveolives.com</b></a></p>

		  			<p>
		  			<em>*In computing, phishing is a criminal activity using social engineering techniques. Phishers attempt to fraudulently acquire sensitive information, such as usernames, passwords and credit card details, by masquerading as a trustworthy entity in an electronic communication. Phishing is typically carried out by email or instant messaging, and often directs users to give details at a Web site, although phone contact has been used as well. Source: Wikipedia, the free encyclopedia</em></p>
		  
		  			<h2>Payment form</h2>

		  			<p>When you use the payment form, the information captured about your credit card is transmitted via the Internet using the SSL (Secure Socket Layer) protocol.</p>

					<p>This protocol, which is used by your browser (Internet Explorer or Netscape), enables secured transmission of information via the Internet to the payment service.</p>

		  			<p>The confidentiality of your information during transmission on the Internet is ensured through encoding.</p>
			<?php 
					break;
			?>
			<?php
				case "shipping":
			?>
					<h1>Our Terms and Conditions</h1>
					<hr></hr>

		  			<p class="intro">Olive &amp; Olives makes every effort to keep a sufficient inventory of the products offered on its Web site. However, the availability of these products is not guaranteed.</p>

					<h3>Online product availability</h3>

		  			<p>Olive &amp; Olives waives any liability in case of inventory shortages.</p>

		  			<p>If Olive &amp; Olives is unable to deliver a product ordered through its www.oliveolives.com site, the buyer will be notified accordingly by e-mail or telephone. If such is the case, any amount charged to your credit card for products, including delivery charges, will be credited as soon as possible.</p>
		  
		 			<h2>Price policy</h2>
		  
		  			<p>The prices indicated on the www.oliveolives.com site are those established for consumer sales.</p>

		  			<p>The applicable sales taxes are not included in the prices displayed. They will be added to the amount of the order at the moment of check out.</p>

		  			<p>www.oliveolives.com reserves the right to modify the prices of products available on its site at any time, without prior notice.</p>

					<p>All prices quoted on the www.oliveolives.com site are in Canadian dollars.</p>
		  
		  			<p>The prices of products available on www.oliveolives.com may differ from those posted in Olive &amp; Olives outlets.</p>

		  			<h2>Delivery charges and services</h2>
		  
		  			<p>www.oliveolives.com delivers through Fedex. Orders are delivered within 3 business days. Fedex however does not guarantee this delivery time in remote areas.</p>
		  
		  			<h2>Payment options</h2>

		  			<p>Credit cards are the only form of payment accepted for online buying on Olive &amp; Olives Web site. We honour Visa and MasterCard.</p>

		  			<h2>Incomplete order</h2>

		  			<p>In the event that we are unable to deliver a product, Olive &amp; Olives refunds the purchase price of the product as well as applicable delivery charges for that product (including taxes).</p>

		  			<h2>Product in Good Condition</h2>

		  			<p>A product ordered by mistake, provided it was in good condition at the time of receipt, may be exchanged or refunded. However, the product must be sealed and resent with a proof of purchase (copy of e-mailed confirmation or invoice printed through the site) within 30 days. If no proof of purchase is presented, the product may only be exchanged.</p>

		  			<p>Olive &amp; Olives does not refund delivery charges billed by www.oliveolives.com for a product delivered in good condition that is exchanged or reimbursed.</p>

		  			<p>To obtain a list of all Olive &amp; Olives outlets and their addresses, click<a href="http://www.oliveolives.com/en/about-us/our-addresses.html">here</a>.</p>

		  			<h2>Defective Product</h2>

		  			<p>A defective product may be exchanged. the product must be resent with a proof of purchase (copy of e-mailed confirmation or invoice printed through the site). It must be at least three-quarters full and bought within the 30 days.</p>
		 
		  			<h2>Missing Product in the Delivered Order</h2>

		  			<p>A client must contact Olive &amp; Olives Customer Service if there is a missing product in the order. Two options are available:</p>

				  	<ul>
					  	<li>If the client wishes to replace the missing bottle, www.oliveolives.com sends the product and covers all delivery charges relative to the new shipment.</li>
					 	 <li>If the client no longer wants the product or if there aren't any left in stock, www.oliveolives.com reimburses the purchase price of the product as well as all the delivery charges for the entire order (including taxes).</li>
				  	</ul>

		 			<h2>About the operator</h2>

		 			<p>The www.oliveolives.com website is operated by Distributions Olive &amp; Olives inc. The sales conditions mentioned above act as a contract between the customer and Olive &amp; Olives. The sales conditions can be modified at any time by Olive &amp; Olives.</p>

		  			<h2>Legal operating address :</h2>

					<p>Distributions Olive &amp; Olives inc.<br>
					4571 East Metropolitain boul.<br>
					Montreal (Quebec)<br>
					H1R 1Z4</p>

					<p>Phone number:<br>
					514 381-4020<br>
					E-mail:<a href="mailto:info@oliveolives.com"><b>info@oliveolives.com</b></a>.<p>
		  
		  			<h3>Delivery Charges</h3>

		  			<p>Calculating Delivery Charges and Examples</p>

		  			<p>Delivery charges are calculated based on Fedex&rsquo;s preferential corporate rates for Olive &amp; Olives. These charges vary depending on the postal code, the order&rsquo;s exact weight before packaging and the package size.</p>

					<p>A number of factors influence the weight of an order, such as the type of bottle (shape, thickness of glass, etc.). The weight of gift sets includes gift packaging (e.g., metal box) and accessories (e.g., glasses) when applicable. Taxes paid by customers are calculated based on discounted delivery charges.</p>

		  			<p>Note that the higher the number of bottles ordered, the less costly the delivery charges per unit will be.</p>
		  
		  			<h2>Transaction security</h2>

		  			<p><b>Payment service</b></p>

		  			<p>The www.oliveolives.com payment service allows you to make on-the-spot purchases by credit card. This service guarantees a high level of security, reliability and efficiency.</p>

		  			<p>www.oliveolives.com offers a number of features that ensure your financial transaction is processed securely, including the following :</p>
				 	<ul>
						<li>Secure transmission of the credit card number to the payment server, which redirects the transaction to the institution that issued the card for authorization.</ul>
				  		<li>The information transmitted on the Internet is encrypted.</ul>
				  		<li>The request for authorization for the amount of the purchase is processed immediately.</li>
				  	</ul>

		  			<p>The payment service is easy to use and free of charge. All you need is one of the major credit cards.</p>

		  			<h2>Security information</h2>

		  			<p>The payment service has a number of security-related features. Additional information is given on the security mechanisms related to the payment form and your navigator's encryption details in the following sections.</p>

		  			<h2>Encoding and browsers</h2>

		  			<p>Encoding consists in turning a clearly worded text into an unintelligible text (cryptogram) using cryptographic techniques such as algorithms and keys. Access to a secret key is necessary to decode the information. Cryptographic systems use a variable length key. You can rest assured that transmission of information is totally secure:</p>

					<ul>
					  <li>The encoded mode is confirmed by a padlock in the bottom right corner of the screen.</li>
					</ul>

		  			<h2>Cryptography</h2>

		  			<p>Cryptography is the science of encoding messages so that the content can be understood only by the designated recipient. Encoding in various forms has been used since time immemorial.</p>

		  			<p>Today, encoding is used to transmit digital data via computer network or, increasingly, to ensure confidentiality of important files stored on your computer. The clearly worded text is turned into an unintelligible text using complex mathematical algorithms and cryptograms known as keys. A reverse operation is applied for decoding. Security experts foresee a growing use of encoding for non-classified computer data.</p>

		  			<p>The www.oliveolives.com payment service uses asymmetrical encoding, which ensures secured transmission of your credit card number over the Internet.</p>
		 
		  			<h2>Paying for your purchase</h2>

		  			<p>At the time of payment, www.oliveolives.com displays a form on which you are asked to enter your credit card number, year/month of expiration, as well as the CVV2 code.</p>

		  			<h2>IMPORTANT NOTICE on the CVV2 code</h2>
		 
		 			<p>Most major credit cards offer an added value in terms of security for online purchases, called the CVV2 code. This code is a group of 3 or 4 numbers (embossed on the front of the card, or printed on the signature strip) that allows a cryptographic control of the information and protects the card owner against fraud. When purchasing online, entering these numbers validates the authenticity of the card. The CVV2 code is not contained on the magnetic stripe of the card, therefore is not included on the transaction receipt.<p>

		  			<p>The information you enter on this form is encrypted before being transmitted on the Internet, in order to guarantee its confidentiality. It is then transmitted, for immediate processing, to the financial institution. Consequently, you are guaranteed that the information related to your credit card is kept strictly confidential.</p>

		  			<h2>Beware of fraudulent e-mails</h2>

		  			<p>The distribution of fraudulent e-mails (phishing*) is a technique used with the intention to reroute funds by usurping well-established company identities. To do so, authentic looking e-mails are sent by counterfeiters to retrieve personal information from the recipients.</p>

		  			<p>To protect customers, Olive &amp; Olives insists on informing you that no such e-mail will ever be sent to you requesting personal information such as your address, date of birth or credit card number.</p>

		  			<p>If you ever encounter an e-mail that seems to originate from Olive &amp; Olives and asks for personal information, do not answer! Immediately contact customer service to report the incident at 514 381-4020 or at <a href="mailto:info@oliveolives.com"><b>info@oliveolives.com</b></a></p>

		  			<p>
		  			<em>*In computing, phishing is a criminal activity using social engineering techniques. Phishers attempt to fraudulently acquire sensitive information, such as usernames, passwords and credit card details, by masquerading as a trustworthy entity in an electronic communication. Phishing is typically carried out by email or instant messaging, and often directs users to give details at a Web site, although phone contact has been used as well. Source: Wikipedia, the free encyclopedia</em></p>
		  
		  			<h2>Payment form</h2>

		  			<p>When you use the payment form, the information captured about your credit card is transmitted via the Internet using the SSL (Secure Socket Layer) protocol.</p>

					<p>This protocol, which is used by your browser (Internet Explorer or Netscape), enables secured transmission of information via the Internet to the payment service.</p>

		  			<p>The confidentiality of your information during transmission on the Internet is ensured through encoding.</p>
			<?php 
					break;
			?>
			
		<?php
			endswitch;
		?>
	</div>	
</div>
