<div class="secondary_nav">
	<h2>Stores • Retail Outlets • Head Office</h2>
</div>
<div class="content_container clearfix">
	<!-- Address Start -->
	<div class="address_block header_address">
		<h2>Olive &amp; Olives</h2>
		<h1>Montreal</h1>
		<div class="address">
			<p class="street">3127 Masson St<br>Montreal (Quebec)  H1Y 1X8</p>
			<p class="contact">T  <a href="tel:514-526-8989">514 526-8989</a></p>
			<p class="contact">E  <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>
			<a href="http://goo.gl/K5cdZX" class="map_btn" target="_blank">LOCATE</a>
		</div>
		<table class="hours">
			<tr>
				<td class="day">Monday</td>
				<td>10am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Tuesday</td>
				<td>10am to 6 pm</td>
			</tr>
			<tr>
				<td class="day">Wednesday</td>
				<td>10am to 6 pm</td>
			</tr>
			<tr>
				<td class="day">Thursday</td>
				<td>10am to 8pm</td>
			</tr>
			<tr>
				<td class="day">Friday</td>
				<td>10am to 8pm</td>
			</tr>
			<tr>
				<td class="day">Saturday</td>
				<td>10am to 5pm</td>
			</tr>
			<tr>
				<td class="day">Sunday</td>
				<td>10am to 5pm</td>
			</tr>
		</table>
	</div>
	<!-- Address Start -->
	<div class="address_block header_address last_element">
		<h2>Olive &amp; Olives</h2>
		<h1>Montreal, Jean-Talon Market</h1>
		<div class="address">
			<p class="street">7070 Henri-Julien Ave.<br>Montreal (Quebec) H2R 1T1</p>
			<p class="contact">T <a href="tel:514-271-0001">514 271-0001</a></p>
			<p class="contact">E <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>
			<a href="http://goo.gl/maps/QZSA0" class="map_btn" target="_blank">LOCATE</a>
		</div>
		<table class="hours">
			<tr>
				<td class="day">Monday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Tuesday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Wednesday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Thursday</td>
				<td>9am to 8pm</td>
			</tr>
			<tr>
				<td class="day">Friday</td>
				<td>9am to 8pm</td>
			</tr>
			<tr>
				<td class="day">Saturday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Sunday</td>
				<td>9am to 5pm</td>
			</tr>
		</table>
	</div>
	<!-- Address Start -->
	<div class="address_block">
		<h2>Olive &amp; Olives</h2>
		<h1>Laval, Marché Gourmand</h1>
		<div class="address">
			<p class="street">2888 Cosmodôme Ave.<br>Laval (Quebec)  H7T 2X1</p>
			<p class="contact">T <a href="tel:450-687-8222">450 687-8222</a></p>
			<p class="contact">E <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>
			<a href="http://goo.gl/maps/k6Seu" class="map_btn" target="_blank">LOCATE</a>
		</div>
		<table class="hours">
			<tr>
				<td class="day">Monday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Tuesday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Wednesday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Thursday</td>
				<td>9am to 9pm</td>
			</tr>
			<tr>
				<td class="day">Friday</td>
				<td>9am to 9pm</td>
			</tr>
			<tr>
				<td class="day">Saturday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Sunday</td>
				<td>9am to 6pm</td>
			</tr>
		</table>
	</div>

	<!-- Address Start -->
	<div class="address_block last_element">
		<h2>Olive &amp; Olives</h2>
		<h1>St-Lambert</h1>
		<div class="address">
			<p class="street">428b Victoria Ave.<br>St-Lambert (Quebec)  J4P 2H9</p>
			<p class="contact">T <a href="tel:450-923-2424">450 923-2424</a></p>
			<p class="contact">E <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>
			<a href="http://goo.gl/maps/WmS0N" class="map_btn" target="_blank">LOCATE</a>
		</div>
		<table class="hours">
			<tr>
				<td class="day">Monday</td>
				<td>10am to 6 pm</td>
			</tr>
			<tr>
				<td class="day">Tuesday</td>
				<td>10am to 6 pm</td>
			</tr>
			<tr>
				<td class="day">Wednesday</td>
				<td>10am to 6 pm</td>
			</tr>
			<tr>
				<td class="day">Thursday</td>
				<td>10am to 8pm</td>
			</tr>
			<tr>
				<td class="day">Friday</td>
				<td>10am to 8pm</td>
			</tr>
			<tr>
				<td class="day">Saturday</td>
				<td>10am to 5pm</td>
			</tr>
			<tr>
				<td class="day">Sunday</td>
				<td>10am to 5pm</td>
			</tr>
		</table>
	</div>
	<!-- Address Start -->
	<div class="address_block">
		<h2>Olive &amp; Olives</h2>
		<h1>Premium Outlets Mirabel</h1>
		<div class="address">
			<p class="street">19001, Chemin Notre-Dame<br>Mirabel (Quebec)  J7J 0T1</p>
			<p class="contact">T  <a href="tel:450-434-7070">450 434-7070</a></p>
			<p class="contact">E <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>
			<a href="https://goo.gl/maps/vdT2L" class="map_btn" target="_blank">LOCATE</a>
		</div>
		<table class="hours">
			<tr>
				<td class="day">Monday</td>
				<td>10am to 9pm</td>
			</tr>
			<tr>
				<td class="day">Tuesday</td>
				<td>10am to 9pm</td>
			</tr>
			<tr>
				<td class="day">Wednesday</td>
				<td>10am to 9pm</td>
			</tr>
			<tr>
				<td class="day">Thursday</td>
				<td>10am to 9pm</td>
			</tr>
			<tr>
				<td class="day">Friday</td>
				<td>10am to 9pm</td>
			</tr>
			<tr>
				<td class="day">Saturday</td>
				<td>&nbsp;&nbsp;9am to 5pm</td>
			</tr>
			<tr>
				<td class="day">Sunday</td>
				<td>10am to 5pm</td>
			</tr>
		</table>
	</div>
	<!-- Address Start -->
	<!-- Removed store (By CS at demand of Claudia Pharand September 1, 2015) -->
	<!-- 
	<div class="address_block last_element">
		<h2>Olive &amp; Olives</h2>
		<h1>Québec</h1>
		<div class="address">
			<p class="street">1066 rue St-Jean<br>Québec (Québec) G1R 1S1</p>
			<p class="contact">T <a href="tel:418-692-1999">418 692-1999</a></p>
			<p class="contact">E <a href="mailto:quebec@oliveolives.com" target="_blank" class="mail_link">quebec@oliveolives.com</a></p>
			<a href="https://goo.gl/maps/yQD72" class="map_btn" target="_blank">LOCATE</a>
		</div>
		<table class="hours">
			<tr>
				<td class="day">Monday</td>
				<td>10am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Tuesday</td>
				<td>10am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Wednesday</td>
				<td>10am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Thursday</td>
				<td>10am to 8pm</td>
			</tr>
			<tr>
				<td class="day">Friday</td>
				<td>10am to 8pm</td>
			</tr>
			<tr>
				<td class="day">Saturday</td>
				<td>&nbsp;&nbsp;9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Sunday</td>
				<td>10am to 5pm</td>
			</tr>
		</table>
	</div>
	-->
	<!-- Address Start -->
	<div class="address_block last_element">
		<h2>Distributions Olive &amp; Olives inc.</h2>
		<h1 style="letter-spacing:-0.04em;">Montreal, Head Office/Warehouse</h1>
		<div class="address">
			<p class="street">8262 Pie IX blvd.<br>Montreal (Quebec)  H1Z 3T6</p>
			<p class="contact">T <a href="tel:514-381-4020">514 381-4020</a></p>
			<p class="contact">E <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>
			<a href="http://goo.gl/maps/Jasyv" class="map_btn" target="_blank">LOCATE</a>
		</div>
	</div>
	<!-- Address Start -->
	<div class="address_block">
		<h1 style="margin-top:23px;">Olive &amp; Olives Booths</h1>
		<div>
			<p class="footer_links" style="margin-top: 0;">This schedule can change without prior notice. <a href="mailto:info@oliveolives.com" style="text-decoration: none; color:#95b823; -webkit-margin-before:0;
-webkit-margin-after: 0; margin: 0;">Email us</a> for any inquiries</p>
			<?php foreach ($costcoAddresses as $costco): ?>
				<h2><?=str_replace("Québec","Quebec",$costco[2]);?>:</h2>
				<p class="street cs_street"><?=Util::makeDateSpan($costco[0],$costco[1],$lan);?></p>
			<?php endforeach ?>
		</div>
	</div>
</div>
