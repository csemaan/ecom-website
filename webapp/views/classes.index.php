<div class="secondary_nav">
	<h2><?=$cls_catchphrase?></h2>
</div>
<div class="content_container clearfix">
	<div id="side_nav">
		<ul>
			<li class="<?=(($currentInstructor==='0')?'cur_nav':'')?>"><a href="<?=Util::baseUrl('classes/',$lan)?>"><?=strtoupper($com_types_all)?></a></li>
			<?php
				if($availableInstructors): 
					foreach ($availableInstructors as $instructor):
			?>
					<li class="<?=(($instructor['cls_instructor_slug']==$currentInstructor)?'cur_nav':'')?>"><a href="<?=Util::baseUrl('classes/'.$instructor['cls_instructor_slug'].'/',$lan)?>"><?=$instructor['cls_instructor']?></a></li>
			<?php
					endforeach;
				endif;
			?>
		</ul>
	</div>
	<div id="main_content">
	<?php if ($classes): ?>
		<?php if ($currentInstructor!='0' && $classes[0]['cls_instructor']!='' && $classes[0]['cls_instructor_header']!=''): ?>		
			<div class="instructor_container">
				<img src="data/images/class/<?=$classes[0]['cls_img_instructor']?>" height="121" width="180" alt="">
				<p class="title"><?=$classes[0]['cls_instructor_header']?><br>
				<span><?=$cls_with." ".$classes[0]['cls_instructor']?><span></p>
			</div>
		<?php else: ?>
			<div class="instructor_container">
				<img src="assets/images/cls_header.jpg" height="121" width="180" alt="">
				<p class="title"><?=$cls_group_title?><br>
				<span><?=$cls_group_subtitle?><span></p>
			</div>
		<?php endif ?>
		<div class="content_tools">
			<a href="javascript:void(0)" data-popupurl="<?=Util::makeMailtoUrl($com_share_subject,$com_share_body);?>" class="cs_popuplink pr_button last_element">+ <?=$com_share?></a>		
			<!--<a href="javascript:void(0)" class="pr_button">Imprimer</a>-->
		</div>
		<?php
			foreach ($classes as $class):
		?>
				<div class="course_block">
					<img src="data/images/class/<?=$class['cls_img_course']?>" height="150" width="150" alt="" class="postcard">
					<h2><?=Util::makeDateTime($class["sch_date"],$class["sch_time"],$lan);?> (<?=$cls_duration?>: <?=$class["sch_duration"]?> <?=$cls_minutes?>)</h2>
					<h1><?=$class["cls_title"]?></h1>
					<h3><?=$class["cls_subtitle"]?></h3>
					<div class="course_desc">
						<p>
							<?=$class["cls_desc"]?>
						</p>
						<p class="title">
							<b>> <?=sprintf($cls_groupsize,$class['sch_groupsize'])?>
							<?php if ($lan!="en" && strtotime($class["sch_date"])>=strtotime(date("Y-m-d"))): ?>
								:
								<?=sprintf(($class['sch_stock']>1)?$cls_available_plural:$cls_available_singular,$class['sch_stock']);?>		
							<?php endif ?>
							</b>
						</p>
						<p class="title">> <?=$cls_where?><?=$class['sch_location']?></p>
					</div>
					<div class="course_detail">
						<p class="price"><?=Util::formatPrice(number_format($class['cls_price']),$lan);?></p>
						<p class="price_desc"><?=$cls_byperson?></p>
						<?php if ($lan!="en") { ?>
							<form novalidate method="post" action="<?=Util::baseUrl('cart/',$lan)?>" data-invalid-msg="<?=$com_qty_pattern_error?>" data-invalid-maxvalue-msg="<?=$com_qty_stock_error?>">
								<?php if ($class['sch_stock']>0 && strtotime($class["sch_date"])>=strtotime(date("Y-m-d"))) { ?>
									<input type="hidden" name="action" value="add"/>
									<input type="hidden" name="sch_id" value="<?=$class['sch_id']?>"/>
									<span class="cs_posAnchor"><input autocomplete='off' type="text" name="crd_quantity" value='1' required pattern="[1-9][0-9]?" maxlength="2" maxvalue="<?=$class['sch_stock']?>"></span>
									<a href="#" class="add_to_cart pr_hub_btn"><?=$cls_buybtn?></a>	
								<?php }else{  ?>
									<span class="add_to_cart cs_no_action course_full_btn"><?=$cls_full ?></span><br/>
								<?php } ?>
							</form>
						<?php } else { ?>
							<p class="clearfix">
								<?php if ($class['sch_stock']>0 && strtotime($class["sch_date"])>=strtotime(date("Y-m-d"))) { ?>
									<a href="javascript:void(0);" data-popupurl="<?=Util::makeClassesInquiryMailtoUrl("contact@oliveolives.com",$class["cls_title"].", ".Util::makeDateTime($class["sch_date"],$class["sch_time"],$lan),$cls_inquire_body);?>" class="cs_popuplink cs_classes_inquire">INQUIRE BY EMAIL</a>
								<?php }else{  ?>
									<span class="add_to_cart cs_no_action course_full_btn"><?=$cls_full ?></span><br/>
								<?php } ?>
							</p>
						<?php } ?>
						<a href="<?=Util::baseUrl('policies/2/shipping',$lan)?>" class="cource_policy"><?=$cls_cancelpolicy?></a>
					</div>				
				</div>
		<?php 
			endforeach;
		?>
	<?php 
	else:
		echo $cls_emptyset;
	endif;
	?>
	</div>		
</div>