<div class="secondary_nav">
	<h2><img src="assets/images/truck.png" height="29" width="62" alt="">Livraison gratuite pour toute commande supérieur à 50 $ </h2>	
</div>
<div class="content_container clearfix">
	<div id="shopping_cart">
		<h1>Thank You!</h1>
		<h2></h2>
		<h4>Your Order Number: 123456</h4>		
		<p>Thank you for your order, we appeciate your business. You should receive a confirmation email shortly to the address that you provided. If you have any questions, please do not hesitate to contact us by <a href="mailto:info@oliveolives.com">email</a> or by calling us at <b>1-800 555-1234</b>.</p>

		<p>We will ship your order out as quickly as possible, please anticipate 3-5 business days for it to arrive. </p>

		<p>You may track your package with the tracking number provided in the email that you will receive from PayPal.</p>

	</div>
	<div id="cart_total">
		<table class="receipt">
			<tr id="header">
				<td class="product">Product</td>
				<td class="quantity">Qty</td>
				<td class="price">Price</td>
			</tr>
			<tr class="row">
				<td class="product">Olive & Olives 1 Orga nic - Tim</td>
				<td class="quantity">1</td>
				<td class="price">$8.00</td>
			</tr>
			<tr class="row">
				<td class="product">Olive & Olives 1 Organic - Tim</td>
				<td class="quantity">1</td>
				<td class="price">$8.00</td>
			</tr>
			<tr class="row">
				<td class="product">Olive & Olives 1 Organic - Tim</td>
				<td class="quantity">1</td>
				<td class="price">$8.00</td>
			</tr>
			<tr class="row">
				<td class="product">Olive & Olives 1 Organic - Tim</td>
				<td class="quantity">19</td>
				<td class="price">$8.00</td>
			</tr>												
			<tr class="row">
				<td class="product last">Olive & Olives 1 Organic - Tim</td>
				<td class="quantity last">1</td>
				<td class="price last">$8.00</td>
			</tr>
		</table>				
		<table class="receipt">							
			<tr class="row">
				<td class="rec_label">Subtotal</td>
				<td class="sub">$24.00</td>
			</tr>
			<tr class="row">
				<td class="rec_label">Shipping</td>
				<td class="shipping">Complete Form</td>
			</tr>				
			<tr class="row taxes">
				<td class="rec_label">Taxes</td>
				<td class="tax">Complete Form</td>

			</tr>	
			<tr class="row">
				<td class="rec_label last">TOTAL</td>
				<td class="total last">Complete Form</td>
			</tr>												
		</table>
	</div>
</div>			
