<div class="content_container clearfix">
	<div id="side_nav">
		<ul>
			<?php
				for($i=0; $i<count($policies); $i++) :
			?>
					<li class="<?=(($section==$i)?'cur_nav':'')?>"><a href="<?=Util::baseUrl('policies/'.$i.'/'.$policies[$i][0],$lan)?>"><?=${$policies[$i][1]}?></a></li>
			<?php
				endfor;
			?>
		</ul>
	</div>
	<div id="main_content" class="static_page">
		<?php
			switch($section):
				// 0 : PRIVACY POLICY
				case 0:
		?>
					<h1>Confidentiality is our priority</h1>
					<hr/>
					<p class="intro">Olive &amp; Olives is committed to protecting your personal information and privacy. The information you give us is used strictly to fill your orders or e-mail you special promotions at oliveolives.com.</p>
					<h2>How information is collected and used</h2>
					<p>To fill an order, we need to know your name, your e-mail address and the shipping and billing addresses. This enables us to process, and keep track of your orders while observing the strictest security procedures. At no time will Olive &amp; Olives or oliveolives.com use these recipients' names and addresses for promotional purposes.</p>
					<p>The information we request includes your telephone number so we can contact you rapidly in case of a problem regarding the content of your orders or their forwarding.</p>
					<p>Your e-mail address is essential when you place an order or when you send us comments by e-mail.</p>
					<p>The information is also used to enhance Olive &amp; Olives.com presentation and organization of Olive &amp; Olives.com site as well as to let you know about changes and new services found on our site. If you no longer wish to receive e-mail information, simply click on the "Unsubscribe" link at the bottom of Olive &amp; Olives e-Newsletter’s.</p>
					<p><b>Protection of your confidential information</b></p>
					<p>When purchasing online on oliveolives.com, your credit card information is securely transmitted on the Internet every step of the transaction treatment due to an encryption method (Secure Sockets Layer protocol) used by both your Web browser and by oliveolives.com. No credit card information is kept on Olive &amp; Olives web site, your card number is therefore accessible for the sole purpose of processing the transaction.</p>
					<p>Moreover, in order to limit unauthorised access, we observe strict security measures when it comes to archiving and divulging any information provided by our customers. Because of that, we may ask you to confirm your identity once again before submitting personal information.</p>
					<p><b>Cookies</b></p>
					<p>Cookies are not just sweet treats; they are also information packets recorded by an http server on the hard disk of your computer for purposes of identification. Our cookies contain no confidential information; they allow us to carry out a number of operations such as processing your orders or storing products that you have selected from our site. The majority of net users automatically accept cookies. However, you can refuse them. The choice is yours. Even without cookies, you retain access to the majority of promotions on our site and the ability to place orders.</p>
					<p><b>Disclosure of your personal information to third parties</b></p>
					<p>By guaranteeing the utmost confidentiality, Olive &amp; Olives and oliveolives.com undertake to not disclose or market any information whatsoever regarding you or the recipient of your purchases. However, in case of theft or fraudulent use of your card, we will cooperate with police services in the investigations generally conducted in such circumstances</p>
					<p>Olive &amp; Olives reserves the right to modify its privacy policy at any time. In the event Olive &amp; Olives modifies its privacy policy, all subscribers and customers will be notified ahead of time.</p>
			<?php
					break;

				// 1 : TERMS AND CONDITIONS (USE)
				case 1:
			?>
					<h1>Terms and conditions</h1>
					<hr>

					<h2>Online product availability</h2>
					<p>Olive &amp; Olives makes every effort to keep a sufficient inventory of the products offered on its Web site. However, the availability of these products is not guaranteed.
					<p>Olive &amp; Olives waives any liability in case of inventory shortages.
					<p>If Olive &amp; Olives is unable to deliver a product ordered through its www.oliveolives.com site, the buyer will be notified accordingly by e-mail or telephone. If such is the case, any amount charged to your credit card for products, including delivery charges if applicable, will be credited as soon as possible.

		 			<h2>Price policy</h2>
		  			<p>The prices indicated on the www.oliveolives.com site are those established for consumer sales.
					<p>The applicable sales taxes are not included in the prices displayed. They will be added to the amount of the order at the moment of check out.
					<p>www.oliveolives.com reserves the right to modify the prices of products available on its site at any time, without prior notice.
					<p>All prices quoted on the www.oliveolives.com site are in Canadian dollars.
					<p>The prices of products available on www.oliveolives.com may differ from those posted in Olive &amp; Olives outlets.

		  			<h2>Shipping (available only in Canada)*</h2>
		  			<p>All Olive &amp; Olives orders are shipped within Canada only by Canada Post. Orders are processed within 24 to 48 hrs, Monday through Friday.
					<p>Once your order is confirmed, Olive &amp; Olives prepares your order with the utmost care.
					<p>Packages will be shipped by Canada Post and will be delivered within 2 to 10 business days depending on the delivery address. The shipping charges vary depending on the postal code, the order’s exact weight before packaging and the package size. A number of factors influence the weight of an order, such as the type of bottle (shape, thickness of glass, etc.). The weight of gift sets includes gift packaging (e.g., metal box) and accessories (e.g., glasses) when applicable.
					<p>*Free delivery is applicable on all orders of $60.00 or more (before taxes and excluding subscriptions to classes and workshops).

		  			<h2>Terms of payment and transaction security</h2>
		  			<p>All banking transactions are processed and secured through PAYPAL. Trading is done on the PAYPAL website; therefore, and for your own security, we have no access to banking information.

		  			<h2>Incomplete order</h2>
		  			<p>In the event that we are unable to deliver a product, Olive &amp; Olives refunds the purchase price of the product as well as applicable delivery charges for that product (including taxes) if applicable.

		  			<h2>Product in Good Condition</h2>
					<p>A product ordered by mistake, provided it was in good condition at the time of receipt, may be exchanged or refunded. However, the product must be sealed and resent with a proof of purchase (copy of e-mailed confirmation or invoice printed through the site) within 30 days. If no proof of purchase is presented, the product may only be exchanged.
					<p>Olive &amp; Olives does not refund delivery charges billed by www.oliveolives.com for a product delivered in good condition that is exchanged or reimbursed.
					<p>To obtain a list of all Olive &amp; Olives outlets and their addresses, <a href="<?=Util::baseUrl('stores','en')?>">click here</a>.

		  			<h2>Defective Product</h2>
		  			<p>A defective product may be exchanged. the product must be resent with a proof of purchase (copy of e-mailed confirmation or invoice printed through the site). It must be at least three-quarters full and bought within the 30 days.

		  			<h2>Missing Product in the Delivered Order</h2>
		  			<p>A client must contact Olive &amp; Olives Customer Service if there is a missing product in the order. Two options are available:
				  	<ul>
					  	<li>If the client wishes to replace the missing product, www.oliveolives.com sends the product and covers all delivery charges relative to the new shipment.</li>
					 	<li>If the client no longer wants the product or if there aren't any left in stock, www.oliveolives.com reimburses the purchase price of the product as well as all the delivery charges for the entire order (including taxes).</li>
				  	</ul>

				  	<h2>Ethics</h2>
					<p>A written message can be added to the package when you order a gift. Olive &amp; Olives reserves the right to check and refuse any message that does not comply with ethical values.

		 			<h2>About the operator</h2>
		 			<p>The www.oliveolives.com website is operated by Distributions Olive &amp; Olives inc. The sales conditions mentioned above act as a contract between the customer and Olive &amp; Olives. Olive &amp; Olives reserves the right to change this policy at any time with immediate effect upon posting on the site, without liability to you or any other person.

		  			<h2>Legal operating address:</h2>
					<p>Distributions Olive &amp; Olives inc.<br>
					8262 Pie IX blvd.<br>
					Montreal (Quebec)<br>
					H1Z 3T6</p>

					<p>Phone number:<br>
					514 381-4020<br>
					E-mail:<a href="mailto:info@oliveolives.com"><b>info@oliveolives.com</b></a>.<p>

		  			<h2>Beware of fraudulent e-mails</h2>

		  			<p>The distribution of fraudulent e-mails (phishing*) is a technique used with the intention to reroute funds by usurping well-established company identities. To do so, authentic looking e-mails are sent by counterfeiters to retrieve personal information from the recipients.
					<p>To protect customers, Olive &amp; Olives insists on informing you that no such e-mail will ever be sent to you requesting personal information such as your address, date of birth or credit card number.
					<p>If you ever encounter an e-mail that seems to originate from Olive &amp; Olives and asks for personal information, do not answer! Immediately contact customer service to report the incident at 514 381-4020 or at info@oliveolives.com
					<p style="font-size:0.85em;">*In computing, phishing is a criminal activity using social engineering techniques. Phishers attempt to fraudulently acquire sensitive information, such as usernames, passwords and credit card details, by masquerading as a trustworthy entity in an electronic communication. Phishing is typically carried out by email or instant messaging, and often directs users to give details at a Web site, although phone contact has been used as well. Source: Wikipedia, the free encyclopedia.</p>
			<?php
					break;

				// 2: CANCELLATION
				case 2:
			?>
					<h1>Our Cancellation Policy</h1>
					<hr/>
					<p class="intro">Olive &amp; Olives reserves the right to cancel a course due to insufficient enrolment or due to factors out of their control. Participants will be notified ahead of time by telephone. If the workshop or class cannot be moved to a later date that is convenient to the client, the class will be fully refunded. </p>
					<p>If a participant wishes to cancel a workshop or class, Olive &amp; Olives must be informed by telephone or email seven days prior to the event in order to substitute for another date or to be eligible for a store credit. </p>
					<p>If cancellation notices are received less than seven days before the event, Olive &amp; Olives is unable to issue a store credit or to substitute with another class. No refund.</p>

			<?php
					break;

				// 3: SHIPPING
				case 3:
			?>
					<h1>Shipping Information</h1>
					<hr>

		 			<h2>Shipping (available only in Canada)</h2>
					<!-- 
					<div class="shipping-alert">
						<p>Madam, Sir,</p>
						<p>During the Canada Post strike all services will be interrupted. To ensure a good service, your order will be delivered by FedEx.</p>
						<p>We thank you for your understanding and hope that the situation will soon be resolved.</p>
						<p>Olive &amp; Olives team</p>
					</div> 
					-->
					<p>All Olive &amp; Olives orders are shipped within Canada only by Canada Post. Orders are processed within 24 to 48 hrs, Monday through Friday.
					<p>Once your order is confirmed, Olive &amp; Olives prepares your order with the utmost care.
					<p>Packages will be shipped by Canada Post and will be delivered within 2 to 10 business days depending on the delivery address. The shipping charges vary depending on the postal code, the order’s exact weight before packaging and the package size. A number of factors influence the weight of an order, such as the type of bottle (shape, thickness of glass, etc.). The weight of gift sets includes gift packaging (e.g., metal box) and accessories (e.g., glasses) when applicable.
					<p><b>Free delivery is applicable on all orders of $60.00 or more</b> (before taxes and excluding subscriptions to classes and workshops).

			<?php
					break;
			?>

		<?php
			endswitch;
		?>
	</div>
</div>
