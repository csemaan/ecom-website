<div class="secondary_nav">
	<h2>Stores • Retail Outlets • Head Office</h2>
</div>
<div class="content_container clearfix">
	<div class="address_block header_address">
		<h2>Olive &amp; Olives</h2>
		<h1>Montreal</h1>
		<div class="address">
			<p class="street">3127 Masson St<br>Montreal (Quebec)  H1Y 1X8</p>
			<p class="contact">T  514 526-8989</p>
			<p class="contact">E  <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>						
			<a href="http://goo.gl/K5cdZX" class="map_btn" target="_blank">LOCATE</a>
		</div>					
		<table class="hours">
			<tr>
				<td class="day">Monday</td>
				<td>10am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Tuesday</td>
				<td>10am to 6 pm</td>
			</tr>
			<tr>
				<td class="day">Wednesday</td>
				<td>10am to 6 pm</td>
			</tr>
			<tr>
				<td class="day">Thursday</td>
				<td>10am to 8pm</td>
			</tr>
			<tr>
				<td class="day">Friday</td>
				<td>10am to 8pm</td>
			</tr>
			<tr>
				<td class="day">Saturday</td>
				<td>10am to 5pm</td>
			</tr>
			<tr>
				<td class="day">Sunday</td>
				<td>10am to 5pm</td>
			</tr>
		</table>			
	</div>
	<div class="address_block header_address last_element">
		<h2>Olives &amp; Épices</h2>
		<h1>Montreal, Jean-Talon Market</h1>
		<div class="address">					
			<p class="street">7070 Henri-Julien Ave.<br>Montreal (Quebec) H2R 1T1</p>
			<p class="contact">T  514 271-0001</p>
			<p class="contact">E <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>	
			<a href="http://goo.gl/maps/QZSA0" class="map_btn" target="_blank">LOCATE</a>
		</div>					
		<table class="hours">
			<tr>
				<td class="day">Monday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Tuesday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Wednesday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Thursday</td>
				<td>9am to 8pm</td>
			</tr>
			<tr>
				<td class="day">Friday</td>
				<td>9am to 8pm</td>
			</tr>
			<tr>
				<td class="day">Saturday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Sunday</td>
				<td>9am to 5pm</td>
			</tr>
		</table>			
	</div>
	<div class="address_block">
		<h2>Olive &amp; Olives</h2>
		<h1>Laval, Marché Gourmand</h1>
		<div class="address">
			<p class="street">2888 Cosmodôme Ave.<br>Laval (Quebec)  H7T 2X1</p>
			<p class="contact">T  450 687-8222</p>
			<p class="contact">E <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>							
			<a href="http://goo.gl/maps/k6Seu" class="map_btn" target="_blank">LOCATE</a>
		</div>					
		<table class="hours">
			<tr>
				<td class="day">Monday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Tuesday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Wednesday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Thursday</td>
				<td>9am to 9pm</td>
			</tr>
			<tr>
				<td class="day">Friday</td>
				<td>9am to 9pm</td>
			</tr>
			<tr>
				<td class="day">Saturday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Sunday</td>
				<td>9am to 6pm</td>
			</tr>
		</table>			
	</div>
	<div class="address_block last_element">
		<h2>Olive &amp; Olives</h2>
		<h1>St-Lambert</h1>
		<div class="address">					
			<p class="street">428b Victoria Ave.<br>St-Lambert (Quebec)  J4P 2H9</p>
			<p class="contact">T  450 923-2424</p>
			<p class="contact">E <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>	
			<a href="http://goo.gl/maps/WmS0N" class="map_btn" target="_blank">LOCATE</a>
		</div>					
		<table class="hours" style="width:200px;">
			<tr>
				<td class="day">Monday</td>
				<td>10am to 7 pm</td>
			</tr>
			<tr>
				<td class="day">Tuesday</td>
				<td>10am to 7 pm</td>
			</tr>
			<tr>
				<td class="day">Wednesday</td>
				<td>10am to 7 pm</td>
			</tr>
			<tr>
				<td class="day">Thursday</td>
				<td>10am to 8pm</td>
			</tr>
			<tr>
				<td class="day">Friday</td>
				<td>10am to 8pm</td>
			</tr>
			<tr>
				<td class="day">Saturday</td>
				<td>10am to 5pm</td>
			</tr>
			<tr>
				<td class="day">Sunday</td>
				<td>10am to 5pm</td>
			</tr>
			<tr>	
				<td class="day" nowrap>December 24</td>
				<td>10am to 4pm</td>
			</tr>
			<tr>
				<td class="day" nowrap>December 31</td>
				<td>10am to 4pm</td>
			</tr>
			<tr>
				<td class="day" colspan="2">CLOSED <span style="font-weight:400;">December 25-26<br>and January 1-2</span></td>
			</tr>				
		</table>			
	</div>
	<div class="address_block">
		<h2>Olive &amp; Olives</h2>
		<h1>Toronto</h1>
		<div class="address">
			<p class="street">779 Queen St East<br>Toronto (Ontario) M4M 1H5</p>
			<p class="contact">T  416 551-8181</p>
			<p class="contact">E <a href="mailto:contact@oliveolives.com" target="_blank" class="mail_link">contact@oliveolives.com</a></p>								
			<a href="http://goo.gl/maps/eQSFd" class="map_btn" target="_blank">LOCATE</a>
		</div>					
		<table class="hours">
			<tr>
				<td class="day">Monday</td>
				<td>11am to 7pm</td>
			</tr>
			<tr>
				<td class="day">Tuesday</td>
				<td>11am to 7pm</td>
			</tr>
			<tr>
				<td class="day">Wednesday</td>
				<td>11am to 7pm</td>
			</tr>
			<tr>
				<td class="day">Thursday</td>
				<td>11am to 7pm</td>
			</tr>
			<tr>
				<td class="day">Friday</td>
				<td>11am to 7pm</td>
			</tr>
			<tr>
				<td class="day">Saturday</td>
				<td>10am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Sunday</td>
				<td>11am to 5pm</td>
			</tr>
		</table>			
	</div>
	<div class="address_block last_element">
		<h2>Olive &amp; Olives</h2>
		<h1>Toronto</h1>
		<div class="address">					
			<p class="street">20 Market Street<br>Toronto (Ontario) M5E 1M6</p>
			<p class="contact">T  416 815-8181</p>
			<p class="contact">E <a href="mailto:contact@oliveolives.com" target="_blank" class="mail_link">contact@oliveolives.com</a></p>	
			<a href="http://goo.gl/maps/oMeKk" class="map_btn" target="_blank">LOCATE</a>
		</div>					
		<table class="hours">
			<tr>
				<td class="day">Monday</td>
				<td>Closed</td>
			</tr>
			<tr>
				<td class="day">Tuesday</td>
				<td>10am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Wednesday</td>
				<td>10am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Thursday</td>
				<td>10am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Friday</td>
				<td>10am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Saturday</td>
				<td>9am to 6pm</td>
			</tr>
			<tr>
				<td class="day">Sunday</td>
				<td>11am to 5pm</td>
			</tr>
		</table>
	</div>
	<div class="address_block">
		<h1 style="margin-top:23px;">Olive &amp; Olives Booths</h1>
		<div>
			<?php foreach ($costcoAddresses as $costco): ?>
				<h2><?=str_replace("Québec","Quebec",$costco[2]);?>:</h2>
				<p class="street cs_street"><?=Util::makeDateSpan($costco[0],$costco[1],$lan);?></p>
			<?php endforeach ?>
		</div>								
	</div>

	<div class="address_block last_element">
		<h2>Distribution Olive &amp; Olives inc.</h2>
		<h1 style="letter-spacing:-0.04em;">Montreal, Head Office/Warehouse</h1>
		<div class="address">					
			<p class="street">8262 Pie IX blvd.<br>Montreal (Quebec)  H1Z 3T6</p>
			<p class="contact">T  514 381-4020</p>
			<p class="contact">E <a href="mailto:info@oliveolives.com" target="_blank" class="mail_link">info@oliveolives.com</a></p>	
			<a href="http://goo.gl/maps/Jasyv" class="map_btn" target="_blank">LOCATE</a>
		</div>
	</div>
</div>