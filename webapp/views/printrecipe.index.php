<!DOCTYPE html>
<!--[if lt IE 9]><html class="bad-ie"><![endif]-->
<!--[if gt IE 8]><!--><html><!--<![endif]-->
	
	<head>
		<base href="<?=BASE_URL?>"></base>
		<title><?=$recipe['rcd_title']?></title>
		<meta name="robots" content="noindex,nofollow" />
		<link rel="shortcut icon" href="favicon.ico?v=2" />
		<meta name="viewport" content="width=1380">
		<meta name="description" content="<?=$meta_desc?>">
		<meta charset="utf-8">
		<!--[if lt IE 9]>
			<script src="assets/js/lib/html5shiv.js"></script>
		<![endif]-->
		<script type="text/javascript" src="//use.typekit.net/pox7xcr.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
		<link rel="stylesheet" href="assets/styles/css/normalize.css">
		<link rel="stylesheet" href="assets/styles/css/main.css">
		<link rel="stylesheet" href="assets/styles/css/additions_cs.css">
	</head>

	<body class="print">
		<div class="content_container clearfix">
			<div class="print_share_container clearfix">
				<img src="assets/images/logo_print.png" height="25" width="222" alt="" class="print_logo">
				<img src="data/images/recipe/<?=$recipe['rcp_img_large']?>" height="298" width="300" alt="" class="recipe_large postcard">
				<?php if(isset($relatedProducts)): ?>
					<span class="related_products">				
						<p class="title"><?=$rcd_related_products?></p>
						<?php 
							foreach ($relatedProducts as $relatedProd): 
						?>
								<p class="sub_title"><?=$relatedProd['catName']?><?=(($lan=='fr')?' ':'')?>:</p>
						<?php 
								foreach ($relatedProd['products'] as $product):
						?>
									<a href="javascript:void(0);"><?=$product["pdd_name"]?></a>
						<?php
								endforeach;
							endforeach;
						?>
					</span>
				<?php endif ?>	
			</div>			
			<div id="recipe_details" class="clearfix print_details">
				<h1><?=$recipe['rcd_title']?></h1>
				<h2></h2>
				<span class="recipe_desc">
					<h3><?=$rcd_ingredients_label?></h3>
					<p>
						<?=$recipe['rcd_ingredients'];?>
					</p>
					<h3><?=$rcd_preparation_label?></h3>
					<p>
						<?=str_replace("<br>",'<br/><br/>',$recipe['rcd_preparation']);?>
					</p>
				</span>
			</div>
		</div>
		<script type="text/javascript">window.setTimeout(function() {window.print()},500)</script>
	</body>
</html>