<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
    body,td { color:#2f2f2f; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; }
</style>

<div style="font:11px/1.35em Verdana, Arial, Helvetica, sans-serif;">
	<table cellspacing="0" cellpadding="0" border="0" width="98%" style="margin-top:10px; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; margin-bottom:10px;">
		<tr>
			<td align="center" valign="top">
				<!-- [ header starts here] -->
				<table cellspacing="0" cellpadding="0" border="0" width="650">
					<tr>
						<td valign="top"><a href="<?=BASE_URL?>"><img src="<?=BASE_URL?>assets/images/logo_email.gif" alt=""  style="margin-bottom:10px;" border="0"/></a></td>
					</tr>
				</table>
				<!-- [ middle starts here] -->
				<table cellspacing="0" cellpadding="0" border="0" width="650">
					<tr>
						<td valign="top">
							<p>
								<strong>Bonjour <?=$order[0]["cli_firstname"]?> <?=$order[0]["cli_lastname"]?></strong>,
								<br/>Merci d&rsquo;avoir utilis&eacute; la boutique en ligne d&rsquo;Olive &amp; Olives.
								Si vous avez des questions concernant votre achat, veuillez nous &eacute;crire &agrave; <a href="mailto:commande@oliveolives.com" style="color:#1E7EC8;">commande@oliveolives.com</a>
								ou nous appeler au <span class="nobr">(514) 381-4020</span> du lundi au vendredi, de 9&nbsp;h &agrave; 17&nbsp;h HNE.
							</p>
							<p>Voici la confirmation de votre commande. Merci.</p>
							<h3 style="border-bottom:2px solid #eee; font-size:1.05em; padding-bottom:1px; ">
								Votre numéro de commande Olive &amp; Olives : <?=$order[0]["ord_number"]?> <small>(complétée <?=lcfirst(Util::makeDate($order[0]["ord_date"],'fr'))?>)</small>
							</h3>
							<table cellspacing="0" cellpadding="0" border="0" width="100%">
								<thead>
									<tr>
										<th align="left" width="48.5%" bgcolor="#eaf1d3" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;">
											Adresse de facturation&nbsp;:
										</th>
										<th width="3%"></th>
										<th align="left" width="48.5%" bgcolor="#eaf1d3" style="padding:5px 9px 6px 9px; border:1px solid #bebcb7; border-bottom:none; line-height:1em;">
											Adresse de livraison&nbsp;:
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<?php
											$billingAddressId = $order[0]["ord_billing_adr_id_fk"];
											$billingOrderLine = ($order[0]["adr_id"]==$billingAddressId)?0:1;
											$shippingOrderLine = 1-$billingOrderLine;
											$ship = $order[$shippingOrderLine];
											$bill = $order[$billingOrderLine];
										?>
										<td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;">
											<?=$bill["adr_firstname"]." ".$bill["adr_lastname"]?><br/>
											<?=$bill["adr_street1"]?><br/>
											<?php if (isset($bill["adr_street2"])&&$bill["adr_street2"]!=""): ?>
												<?=$bill["adr_street2"]?><br/>
											<?php endif ?>
											<?=($bill["adr_city"].", ".strtoupper($bill["adr_state"]))?><br/>
											<?=$bill["adr_zip"]?><br/>
											<?=(($bill["adr_country"]=="ca")?"Canada":"États-Unis d'Amérique")?><br/>
											T: <?=$bill["adr_phone"]?><br />
											<?=$bill["adr_email"]?>
										</td>
										<td>&nbsp;</td>
										<td valign="top" style="padding:7px 9px 9px 9px; border:1px solid #bebcb7; border-top:0; background:#f8f7f5;">
											<?php if (isset($product) || (isset($order[0]["ord_giftmsg"])&&$order[0]["ord_giftmsg"]!="")): ?>
												<?=$ship["adr_firstname"]." ".$ship["adr_lastname"]?><br/>
												<?=$ship["adr_street1"]?><br/>
												<?php if (isset($ship["adr_street2"])&&$ship["adr_street2"]!=""): ?>
													<?=$ship["adr_street2"]?><br/>
												<?php endif ?>
												<?=($ship["adr_city"].", ".strtoupper($ship["adr_state"]))?><br/>
												<?=$ship["adr_zip"]?><br/>
												<?=(($ship["adr_country"]=="ca")?"Canada":"États-Unis d'Amérique")?><br/>
												T: <?=$ship["adr_phone"]?><br />
												<?=$ship["adr_email"]?>
												<!-- <p>Méthode d'expédition : Postes Canada - Colis standard</p> -->
											<?php else: ?>
												S/O
											<?php endif ?>
										</td>
									</tr>
								</tbody>
							</table>
							<br/>
							<?php if (isset($order[0]["ord_giftmsg"])&&$order[0]["ord_giftmsg"]!=""): ?>
								<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border:1px solid #bebcb7; background:#f8f7f5;">
									<thead>
										<tr>
											<th align="left" bgcolor="#eaf1d3" style="padding:3px 9px">Message cadeau</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td align="left" valign="top" style="padding:3px 9px">
												<p>
													Vous avez demandé qu'une carte cadeau avec le message suivant soit ajoutée
													à votre coli :
												</p>
											</td>
										</tr>
										<tr>
											<td align="left" valign="top" style="padding:3px 20px 10px 20px" bgcolor="#FFFFFF">
												<i><?=$order[0]["ord_giftmsg"]?></i>
											</td>
										</tr>
									</tbody>
								</table>
								<br/>
							<?php endif ?>
							<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border:1px solid #bebcb7; background:#f8f7f5;">
								<thead>
									<tr>
										<th align="left" bgcolor="#eaf1d3" style="padding:3px 9px">Article</th>
										<th align="left" bgcolor="#eaf1d3" style="padding:3px 9px">Réf.</th>
										<th align="center" bgcolor="#eaf1d3" style="padding:3px 9px">Quantité</th>
										<th align="right" bgcolor="#eaf1d3" style="padding:3px 9px">Prix</th>
										<th align="right" bgcolor="#eaf1d3" style="padding:3px 9px">Sous-total</th>
									</tr>
								</thead>
								<?php
									$counter = 0;
									if(isset($product)) :
										foreach ($product as $prd):
								?>
											<tbody<?=(($counter%2==0)?' bgcolor="#eeeded"':'')?>>
												<tr>
													<td align="left" valign="top" style="padding:3px 9px">
														<strong><?=$prd["pdd_name"]?></strong>
													</td>
					    							<td align="left" valign="top" style="padding:3px 9px"><?=$prd["prd_sku"]?></td>
												    <td align="center" valign="top" style="padding:3px 9px"><?=$prd["odd_quantity"]?></td>
												    <td align="right" valign="top" style="padding:3px 9px">
												    	<span class="price"><?=Util::formatPrice($prd['odd_price'],'fr')?></span>
												    </td>
												    <td align="right" valign="top" style="padding:3px 9px">
												    	<span class="price"><?=Util::formatPrice($prd['odd_price']*$prd["odd_quantity"],'fr')?></span>
												    </td>
												</tr>
											</tbody>
								<?php
											$counter++;
										endforeach;
									endif;
								?>
								<?php
									if(isset($class)) :
										foreach ($class as $cls):
								?>
											<tbody<?=(($counter%2==0)?' bgcolor="#eeeded"':'')?>>
												<tr>
													<td align="left" valign="top" style="padding:3px 9px">
														<strong>
															Cours : <?=($cls["cls_title"]." ".lcfirst($cls["cls_subtitle"]))?><br/>
															<?=Util::makeDateTime($cls["sch_date"],$cls["sch_time"],"fr");?> -
															<?=$cls['sch_location']?>
														</strong>
													</td>
					    							<td align="left" valign="top" style="padding:3px 9px"><?=$cls["cls_id"]."S".$cls["sch_id"]?></td>
												    <td align="center" valign="top" style="padding:3px 9px"><?=$cls["odd_quantity"]?></td>
												    <td align="right" valign="top" style="padding:3px 9px">
												    	<span class="price"><?=Util::formatPrice($cls['odd_price'],'fr')?></span>
												    </td>
												    <td align="right" valign="top" style="padding:3px 9px">
												    	<span class="price"><?=Util::formatPrice($cls['odd_price']*$cls["odd_quantity"],'fr')?></span>
												    </td>
												</tr>
											</tbody>
								<?php
										endforeach;
									endif;
								?>
								<tfoot>
									<tr>
										<td colspan="3" align="right" style="padding:3px 9px">Sous-total</td>
		            					<td colspan="2" align="right" style="padding:3px 9px"><span class="price"><?=Util::formatPrice($order[0]["ord_total"]-$order[0]["ord_taxes"]-$order[0]["ord_shippingfees"],"fr")?></span></td>
		        					</tr>
		                        	<tr>
			            				<td colspan="3" align="right" style="padding:3px 9px">Livraison et traitement</td>
			            				<td colspan="2" align="right" style="padding:3px 9px"><span class="price"><?=Util::formatPrice($order[0]["ord_shippingfees"],"fr")?></span></td>
			            			</tr>
			                    	<tr>
							            <td colspan="3" align="right" style="padding:3px 9px">Taxes</td>
							            <td colspan="2" align="right" style="padding:3px 9px"><span class="price"><?=Util::formatPrice($order[0]["ord_taxes"],"fr")?></span></td>
			        				</tr>
							        <tr bgcolor="#eaf1d3">
							        	<td colspan="3" align="right" style="padding:3px 9px"><strong><big>TOTAL</big></strong></td>
							        	<td colspan="2" align="right" style="padding:6px 9px"><strong><big><span class="price"><?=Util::formatPrice($order[0]["ord_total"],"fr")?></span></big></strong></td>
							        </tr>
		    					</tfoot>
		    				</table>
	    					<br/>
	                    	<p>Nous vous remercions,<br/><strong>Olive &amp; Olives</strong></p>
	                	</td>
	            	</tr>
	        	</table>
	    	</td>
		</tr>
	</table>
</div>
