<!DOCTYPE html>
<!--[if lt IE 9]><html class="bad-ie"><![endif]-->
<!--[if gt IE 8]><!--><html><!--<![endif]-->
	
	<head>
		<base href="<?=BASE_URL?>"></base>
		<title><?=$product['pdd_name']?></title>
		<meta name="robots" content="noindex,nofollow" />
		<link rel="shortcut icon" href="favicon.ico?v=2" />
		<meta name="viewport" content="width=1380">
		<meta name="description" content="<?=$meta_desc?>">
		<meta charset="utf-8">
		<!--[if lt IE 9]>
			<script src="assets/js/lib/html5shiv.js"></script>
		<![endif]-->
		<script type="text/javascript" src="//use.typekit.net/pox7xcr.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
		<link rel="stylesheet" href="assets/styles/css/normalize.css">
		<link rel="stylesheet" href="assets/styles/css/main.css">
		<link rel="stylesheet" href="assets/styles/css/additions_cs.css">
	</head>

	<body class="print">
		<div class="content_container clearfix">
			<div id="product_details" class="clearfix">
				<img src="assets/images/logo_print.png" height="25" width="222" alt="" class="print_logo">
				<p><b>www.oliveolives.com</b><br>
				<b><?=$pdp_hq_label ?></b> <?=INFO_PHONE ?></p>	
				<h3><?=($product['prd_typ_id_fk'])?$availableTypes[$product['prd_typ_id_fk']][$lan]:''?></h3>
				<h1><?=$product['pdd_name']?></h1>
				<h2><?=$product['pdd_origin']?></h2>
				<span class="pr_desc">
					<?php if ($product['pdd_desc1_text']): ?>
						<h3><?=$product['pdd_desc1_title']?></h3>
						<p><?=$product['pdd_desc1_text']?></p>
					<?php endif ?>
					<?php if ($product['pdd_desc2_text']): ?>
						<h3><?=$product['pdd_desc2_title']?></h3>
						<p><?=$product['pdd_desc2_text']?></p>				
					<?php endif ?>
					<?php if ($product['pdd_desc3_text']): ?>
						<h3><?=$product['pdd_desc3_title']?></h3>
						<p><?=$product['pdd_desc3_text']?></p>
					<?php endif ?>
				</span>
				<span class="pr_details">
					<?php if($product['pdd_format']):?>
					<p class="pr_size"><?=$product['pdd_format']?></p>
					<?php endif; ?>
					<?php if($product['prd_saleprice']): ?>
						<p class="pr_sale_price"><?=Util::formatPrice($product['prd_saleprice'],$lan)?></p>
						<p class="pr_prev_price"><?=Util::formatPrice($product['prd_price'],$lan)?></p>
					<?php else: ?>
						<p class="pr_sale_price"><?=Util::formatPrice($product['prd_price'],$lan)?></p>
					<?php endif; ?>
					<p class="pr_delivery"><?=$pdd_free_delivery?><br><i class="cs_smallertext"><?=$pdd_free_delivery_notice?></i></p>
				</span>
				<?php if (isset($recipes) && count($recipes)>0): ?>
					<div id="recipe_ideas">
						<h3><?=$pdd_recipe_ideas?></h3>
						<ul class="pr_recipes clearfix">
							<?php
								$counter = 1; 
								foreach ($recipes as $recipe): 
							?>
								<li<?=($counter%4==0)?" class='last_element'":""?>>
									<a href="javascript:void(0)"><img src="data/images/recipe/<?=$recipe['rcp_img_small']?>" height="148" width="148" alt="" class="postcard"><?=$recipe['rcd_title']?></a>
								</li>
							<?php 
								$counter++;
								endforeach; 
							?>
						</ul>
					</div>
				<?php endif; ?>
			</div>
			<img src="data/images/product/<?=$product['prd_img_large']?>" alt="" class="product_large">
		</div>
		<script type="text/javascript">window.setTimeout(function() {window.print()},500)</script>
	</body>
</html>