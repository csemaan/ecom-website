<?php
Class StoresModel extends CommonModel {
	function getCostcoAddresses($fileDataSource) {
		$data = file($fileDataSource);
		array_shift($data);
		$dataArray = array();
		foreach ($data as $line) {
			$line = str_replace('"', '', $line); // Last field can contain commas, and then Google will enclose its values in quotes.
			$lineArray = explode(",",$line,3); // Last field can contain commas and should not be exploded.
			if(strtotime($lineArray[0])<=time() && strtotime($lineArray[1])>=time()-(24*3600)) {	// We only want stores for which the FROM field is in the past or present and the TO field is in the future.
				$dataArray[] = $lineArray;
			}
		}
		usort($dataArray, function($a, $b) {
			if (strpos(strtolower($a[2]), 'costco')===0) {
				return -1;
			}
			else if(strpos(strtolower($b[2]), 'costco')!==0) {
				return (mb_strtolower($a[2])<=mb_strtolower($b[2]))?0:1;
			}
			return 1;
		});
		return $dataArray;
	}

	function getOpeningHours($fileDataSource,$lan) {
		$days = array(
				"en"	=>	array("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"),
				"fr"	=>	array("Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche")	
			);
		$closedStr = array("en"=>"Closed","fr"=>"Fermé");
		$toStr = array("en"=>"to","fr"=>"à");
		$data = file($fileDataSource);
		array_shift($data); // Remove first line (headers) ...
		array_shift($data); // ... and second line (more headers).
		$dataArray = array();
		foreach ($data as $line) {
			$line = str_replace('"', '', $line); // if field contains commas, then Google will enclose its values in quotes.
			$lineArray = explode(",",$line); // Last field can contain commas and should not be exploded.
			$store = array_shift($lineArray); // First column contains store names
			$hours = array();
			for($i=0, $j=0; $i<14; $i = $i+2, $j++) {
				if($lineArray[$i] != "") {
					if($lan == "fr") {
						$hoursStr = $lineArray[$i]." h ".$toStr[$lan]." ".$lineArray[$i+1]." h ";
					}
					else if($lan == "en") {
						$hoursStr = ($lineArray[$i]<13)?$lineArray[$i]."am":($lineArray[$i]-12)."pm";
						$hoursStr .= " ".$toStr[$lan]." ";
						$hoursStr .= ($lineArray[$i+1]<13)?$lineArray[$i+1]."am":($lineArray[$i+1]-12)."pm";
					}
				}
				else {
					$hoursStr = $closedStr[$lan];
				}
				$hours[$days[$lan][$j]] = $hoursStr;
			}

			$dataArray[$store] = $hours;
		}
		return $dataArray;
	}
}
?>