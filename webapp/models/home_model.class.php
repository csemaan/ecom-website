<?php
	Class HomeModel extends CommonModel {
		function getSlideshowData() {
			$this->connect();
			$q = "SELECT * FROM slide 
	        				JOIN slide_detail 
	                		ON sld_sli_id_fk = sli_id 
	                		WHERE sli_active=1
	                		AND sld_lang='".$this->_lan."'";
	        $q .= " ORDER BY FIELD(sli_id,".$this->getSortOrder('slideshow').")";
	        $r = $this->select($q);
			$this->close();
			if($r) {
				return $r;
			}
		}
	}
?>