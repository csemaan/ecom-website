<?php
Class RecipeModel extends CommonModel {
	function get($id) {
		$this->connect();
		$id = $this->clean($id,false);
		$q = "SELECT * FROM recipe 
        				JOIN recipe_detail 
                		ON rcd_rcp_id_fk = rcp_id 
                		WHERE rcp_id = $id 
                		AND rcp_active=1
                		AND rcd_lang='".$this->_lan."'";
        $r = $this->select($q);
		$this->close();
		if($r) {
			return $r;
		}
	}

	function getRelatedProducts($prodSkus) {
		$prodSkus = preg_replace("/([^)(,]+)/i","'$1'",$prodSkus);
		$this->connect();
		$q = "SELECT prd_id,prd_sku,prd_cat_id_fk,pdd_name,pdd_slug,cat_name_fr,cat_name_en FROM product 
        				JOIN product_detail 
                		ON pdd_prd_id_fk = prd_id 
                		JOIN category
                		ON prd_cat_id_fk = cat_id
                		WHERE prd_sku IN ($prodSkus) 
                		AND prd_active=1
                		AND pdd_lang='".$this->_lan."'
                		ORDER BY prd_cat_id_fk ASC";
        $r = $this->select($q);
		$this->close();
		if($r) {
			// Group products by category.
			$prodByCat = array();
			foreach ($r as $product) {
				if(!isset($prodByCat[$product['prd_cat_id_fk']])) {
					$prodByCat[$product['prd_cat_id_fk']] = array(
							'catName' 	=> $product['cat_name_'.$this->_lan],
							'products' 	=> array()
					);
				}
				array_push($prodByCat[$product['prd_cat_id_fk']]['products'],$product);
			}
			return $prodByCat;
		}
	}
}
?>