<?php
// This model is used in the base Controller class.
Class CommonModel extends Sql {
	function getCategories() {
		$this->connect();
		$q = "SELECT * FROM category";
        $q .= " ORDER BY cat_id ASC";
		$r = $this->select($q);
		$this->close();
		if($r) {
			return $r;
		}
	}

	function getTypes($category) {
		$this->connect();
		$category = $this->clean($category,false);
		$q = "SELECT * FROM type 
        				WHERE typ_cat_id_fk = $category 
                		AND typ_active=1";
        $q .= " ORDER BY typ_sortorder ASC";
		$r = $this->select($q);
		$this->close();
		if($r) {
			return $r;
		}
	}

	function getAllCategoriesAndTypes() {
		$bfa = array();
		$cats = $this->getCategories();
		foreach ($cats as $cat) {
			$cattype = array();
			$cattype['catinfo'] = $cat;
			$cattype['types'] = $this->getTypes($cat['cat_id']);
			$bfa[] = $cattype;
		}
		return $bfa;
	}

	function getAllCatTypesSlugs($allCatsAndTypes) {
		$extraSlugs = array();
		foreach($allCatsAndTypes as $catTypes) {
			$extraSlugs[Util::slugify($catTypes['catinfo']['cat_name_en'])] = Util::slugify($catTypes['catinfo']['cat_name_fr']);
			foreach ($catTypes['types'] as $type) {
				$extraSlugs[Util::slugify($type['typ_name_en'])] = Util::slugify($type['typ_name_fr']);
			}
		}
		return $extraSlugs;
	}

	function getAvailableTypes() {
		$this->connect();
		$q = "SELECT typ_id,typ_name_fr,typ_name_en FROM type";
        $r = $this->select($q);
		$this->close();
		if($r) {
			$t = array();
			foreach ($r as $elt) {
				$t[$elt['typ_id']] = array('en'=>$elt['typ_name_en'],"fr"=>$elt['typ_name_fr']);
			}
			return $t;
		}
	}

	protected function getSortOrder($catalogue,$category=0) {
		$r = $this->select("SELECT srt_idseries FROM sort 
						WHERE srt_catalogue='$catalogue'
						AND srt_prd_cat_id=$category");
		if($r) {
			return trim($r[0]["srt_idseries"],",");
		}
		else {
			return 0;
		}
	}

	function createCart($cartNumber) {
		$this->connect();
		$q = "INSERT INTO cart VALUES (0,'$cartNumber',CURDATE())";
		$r = $this->insert($q);
		$this->close();
		if($r) {
			return $r;
		}
	}

	function updateCartSerial($cartNumber) {
		$this->connect();
		$q = "UPDATE cart SET crt_id=LAST_INSERT_ID(crt_id),crt_date=CURDATE() WHERE crt_serial='$cartNumber'";
		$this->update($q);
		$r = $this->db->insert_id;
		$this->close();
		return $r;
	}

	function cartSummary($cartId){
		$this->connect();
		
		// Check and update cart for quantities
		// Delete from cart products and classes that are no more currently available
		$qdu = "DELETE FROM cart_detail WHERE (crd_prd_id_fk IN (SELECT prd_id FROM product WHERE prd_stock=0) OR crd_sch_id_fk IN (SELECT sch_id FROM class_schedule WHERE sch_stock=0)) AND crd_crt_id_fk=$cartId";
		$rdu = $this->delete($qdu);
		// Update cart quantities for products and classes based on current availability
		$quqp = "UPDATE cart_detail SET crd_quantity = LEAST(crd_quantity,(SELECT prd_stock FROM product WHERE prd_id=crd_prd_id_fk LIMIT 1)) WHERE crd_crt_id_fk=$cartId AND crd_prd_id_fk IS NOT NULL";
		$ruqp = $this->update($quqp);	
		$quqc = "UPDATE cart_detail SET crd_quantity = LEAST(crd_quantity,(SELECT sch_stock FROM class_schedule WHERE sch_id=crd_sch_id_fk LIMIT 1)) WHERE crd_crt_id_fk=$cartId AND crd_sch_id_fk IS NOT NULL";
		$ruqc = $this->update($quqc);	


        $qp = "SELECT COUNT(crd_id) AS itemcount,SUM(crd_quantity) AS itemtotal,SUM(crd_quantity*(IFNULL(prd_saleprice,prd_price))) AS subtotal FROM 
        				cart_detail 
        				JOIN product
        				ON crd_prd_id_fk = prd_id
                		WHERE crd_crt_id_fk = $cartId";
		$rp = $this->select($qp);

		$qc = "SELECT COUNT(crd_id) AS itemcount,SUM(crd_quantity) AS itemtotal,SUM(crd_quantity*cls_price) AS subtotal FROM
        				cart_detail 
        				JOIN class_schedule
        				ON crd_sch_id_fk = sch_id
        				JOIN class
        				ON sch_cls_id_fk = cls_id 
                		WHERE crd_crt_id_fk = $cartId";
		$rc = $this->select($qc);
		$this->close();
		$result = array();
		if($rp) {
			$result["product"] = $rp;
		}
		if($rc) {
			$result["class"] = $rc;
		}
		return $result;
	}
}
?>