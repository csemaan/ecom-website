<?php
Class ConfirmationModel extends CommonModel {
	function preProcessOrder($order_uniqid,$cartSummary,$address,$fees,$cartId) {
		$clientId = $this->addClient($address);
		$shipAddr = $this->addAddress($clientId,$address,"ship");
		$billAddr = $this->addAddress($clientId,$address,"bill");
		$orderId = $this->createPreOrder($order_uniqid, $clientId,$shipAddr,$billAddr,$address,$cartId);
		return $orderId;
	}

	function processOrder($ppData) {
		if($ppData['payment_status']=="Completed" && ($ppData['receiver_email']==PP_RECEIVER_EMAIL || $ppData['receiver_id'] == PP_RECEIVER_ID)) {
			$orderId = $this->updateOrder($ppData);
			return $orderId;
		}
		else return false;
	}

	function addClient($address) {
		$this->connect();
		$p = $this->clean($address);
		extract($p);
		$q = "INSERT INTO client VALUES (0,'$bill_fname','$bill_lname','$bill_email','$bill_tel',CURDATE())";
		$r = $this->insert($q);
		$this->close();
		if($r) {
			return $r;
		}
	}
	
	function addAddress($clientId,$address,$type) {
		$this->connect();
		$p = $this->clean($address);
		extract($p);
		if($type=="ship") {
			$q = "INSERT INTO client_address VALUES (0,'$ship_fname','$ship_lname','$ship_street1','$ship_street2','$ship_city','$ship_province','ca','$ship_postalcode','$ship_email','$ship_tel',$clientId)";
		}
		else {
			if(isset($bill_state) && $bill_state!="") {
				$state = $bill_state;
				$zip = $bill_zip;
			}
			else { 
				$state = $bill_province;
				$zip = $bill_postalcode;
			}
			$q = "INSERT INTO client_address VALUES (0,'$bill_fname','$bill_lname','$bill_street1','$bill_street2','$bill_city','$state','$bill_country','$zip','$bill_email','$bill_tel',$clientId)";	
		}
		$r = $this->insert($q);
		$this->close();
		if($r) {
			return $r;
		}
	}
	
	function createPreOrder($order_uniqid, $clientId,$shipAddr,$billAddr,$address,$cartId) {
		$this->connect();
		$p = $this->clean($address);
		extract($p);
		$q = "INSERT INTO `order` VALUES (0,'$order_uniqid','','pp','',0,0,0,'$gift_message','initiated',CURDATE(),$clientId,$shipAddr,$billAddr,'".$this->_lan."')";
		$orderId = $this->insert($q);
		$response = false;
		if($orderId) {
			$orderNumber = "NNC2-".str_pad($orderId,5,'0',STR_PAD_LEFT);
			$this->update("UPDATE `order` SET ord_number='$orderNumber' WHERE ord_id=$orderId");
			$response = $orderId;
		}
		$this->close();
		return $response;
	}

	function updateOrder($ppData) {
		$this->connect();
		$orderStatus = "";
		if(isset($ppData["ipn_track_id"])) {
			$orderStatus = ",ord_status='paid'";
		}

		// Following two lines introduced to fix a bug discovered on August 9, 2016
		// where some orders where having an order total of 0.
		// The problem was traced back to differences in PP IPN variables, some IPN
		// messages not having the variables mc_handling and tax (don't know why as 
		// of today: research did not return a valid answer).
		// Hence following two lines checking first if these variables exist before
		// using them.
		$pp_ipn_mc_handling = 0;
		// Code adjusted AGAIN because it was breaking as of October 13, 2016 !
		// In IPN message we have "mc_handling" ...
		if(isset($ppData['mc_handling'])) {
			$pp_ipn_mc_handling = $ppData['mc_handling'];
		}
		// ... but in PDT POST we have "handling_amount" ! Go figure!
		elseif(isset($ppData['handling_amount'])) {
			$pp_ipn_mc_handling = $ppData['handling_amount'];
		}
		$pp_ipn_tax = (isset($ppData['tax']))?$ppData['tax']:0;

		$q = "UPDATE `order` SET ord_pptransactionid='".$ppData['txn_id']."',ord_total=".$ppData['mc_gross'].",ord_shippingfees=".$pp_ipn_mc_handling.",ord_taxes = ".$pp_ipn_tax."$orderStatus WHERE ord_signature='".$ppData['custom']."'";
		$this->update($q);
		$orderIdQueryResponse = $this->select("SELECT ord_id FROM `order` WHERE ord_signature='".$ppData['custom']."'");
		if ($orderIdQueryResponse) {
			$orderId = $orderIdQueryResponse[0]["ord_id"];
		}
		$response = false;
		if($orderId) {
			// October 14, 2016 : this unfortunately ceased to work, as PP PDT variables have changed without prior notice : the POST contains only the last item in a series of items in a cart, so we cannot rely on this to offer the user a summary of his order on the confirmation page. 
			// Openned a ticket with PP, waiting for reply to see what can be done...
			for($i=1; $i<=$ppData['num_cart_items']; $i++) {
				$qty = $ppData['quantity'.$i];
				$price = $ppData['mc_gross_'.$i]/$qty;
				$prdId = ((strpos($ppData['item_number'.$i],".")===false)?$ppData['item_number'.$i]:0);
				$clsId = ((strpos($ppData['item_number'.$i],".")!==false)?(floor($ppData['item_number'.$i])):0);
				$currentInsertId = $this->insert("INSERT INTO order_detail VALUES (0,$qty,$price,$orderId,$prdId,$clsId)");
				if($currentInsertId && $prdId!=0) {
					$this->update("UPDATE product SET prd_stock=GREATEST(0,prd_stock-$qty) WHERE prd_id=$prdId");
					// Uncomment below if we want to make product with unavailable stock inactive
					//$this->update("UPDATE product SET prd_active=0 WHERE prd_id=$prdId AND prd_stock=0");	
				}
				if($currentInsertId && $clsId!=0) {
					$this->update("UPDATE class_schedule SET sch_stock=GREATEST(0,sch_stock-$qty) WHERE sch_id=$clsId");
				}	
			}
			$response = $orderId;
		}
		$this->close();
		return $response;
	}
	
	function removeCart($cartId) {
		$this->connect();
		$q = "DELETE FROM `cart` WHERE crt_id=$cartId";
		$this->delete($q);
		$this->close();
	}

	function getOrderDetail($orderId) {	
		$this->connect();
		$qo = "SELECT * FROM client_address
        				JOIN client
        				ON adr_cli_id_fk = cli_id
        				JOIN `order` 
        				ON ord_cli_id_fk = cli_id
        				WHERE ord_id = $orderId";
		$ro = $this->select($qo);

        $qp = "SELECT * FROM order_detail 
        				JOIN product
        				ON odd_prd_id_fk = prd_id
        				JOIN product_detail
        				ON pdd_prd_id_fk = prd_id AND pdd_lang='".$ro[0]['ord_lang']."'
                		WHERE odd_ord_id_fk = $orderId";
		$rp = $this->select($qp);

		$qc = "SELECT * FROM order_detail 
        				JOIN class_schedule
        				ON odd_sch_id_fk = sch_id
        				JOIN class
        				ON sch_cls_id_fk = cls_id
                		WHERE odd_ord_id_fk = $orderId";
		$rc = $this->select($qc);
		$this->close();
		$result = array();
		$result["order"] = $ro;
		if($rp) {
			$result["product"] = $rp;
		}
		if($rc) {
			$result["class"] = $rc;
		}
		return $result;
	}

	function getGAeComTrackingJS($orderDetail) {
		$jsStr = "<script> _gaq.push([
								'_addTrans',
								'".$orderDetail["order"][0]["ord_number"]."',
								'Olive & Olives',
								'".$orderDetail["order"][0]["ord_total"]."',
								'".$orderDetail["order"][0]["ord_taxes"]."',
		    					'".$orderDetail["order"][0]["ord_shippingfees"]."',
		    					'".strtoupper($orderDetail["order"][0]["adr_city"])."',
		    					'".strtoupper($orderDetail["order"][0]["adr_state"])."',
		    					'".strtoupper($orderDetail["order"][0]["adr_country"])."'
		  		]);";
		if (isset($orderDetail['product'])):
			foreach ($orderDetail['product'] as $product):
				$jsStr .= "_gaq.push([
										'_addItem',
									    '".$orderDetail["order"][0]["ord_number"]."',
									    '".$product['prd_sku']."',
									    '".$product['pdd_name']."',
									    'Produit',
									    '".$product['odd_price']."',
									    '".$product['odd_quantity']."'
									]);";
			endforeach;
		endif;
		if (isset($orderDetail['class'])):
			foreach ($orderDetail['class'] as $class):
				$jsStr .= "_gaq.push([
										'_addItem',
									    '".$orderDetail["order"][0]["ord_number"]."',
									    '".$class['cls_id']."',
									    '".$class['cls_title']." (".$class['sch_date'].")',
									    'Cours',
									    '".$class['odd_price']."',
									    '".$class['odd_quantity']."'
									]);";
			endforeach;
		endif;
		$jsStr .= "_gaq.push(['_trackTrans']); </script>";
		return $jsStr;
	}
}
?>