<?php
Class CartModel extends CommonModel {
	function add($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		$q = "INSERT INTO cart_detail VALUES (0,$crd_quantity,NOW(),$crt_id,".((isset($prd_id))?$prd_id:'NULL').",".((isset($sch_id))?$sch_id:'NULL').")";
		$r = $this->insert($q);
		$this->close();
		if($r) {
			return $r;
		}
	}

	function get($cartId) {	
		$this->connect();
        $qp = "SELECT crd_id,crd_quantity,prd_id,prd_sku,IFNULL(prd_saleprice,prd_price) AS prd_price,prd_stock,prd_img_small,pdd_lang,pdd_name,pdd_format,prd_taxable,(crd_quantity*prd_weight) AS weight FROM 
        				cart_detail 
        				JOIN product
        				ON crd_prd_id_fk = prd_id
        				JOIN product_detail
        				ON pdd_prd_id_fk = prd_id AND pdd_lang='".$this->_lan."'
                		WHERE crd_crt_id_fk = $cartId
                		ORDER BY crd_datetime ASC";
		$rp = $this->select($qp);

		$qc = "SELECT crd_id,crd_quantity,cls_lang,cls_price,cls_img_course,cls_title,sch_id,sch_date,sch_stock,sch_time,cls_taxable FROM 
        				cart_detail 
        				JOIN class_schedule
        				ON crd_sch_id_fk = sch_id
        				JOIN class
        				ON sch_cls_id_fk = cls_id 
                		WHERE crd_crt_id_fk = $cartId
                		ORDER BY crd_datetime ASC";
		$rc = $this->select($qc);
		$this->close();
		$result = array();
		if($rp) {
			$result["product"] = $rp;
		}
		if($rc) {
			$result["class"] = $rc;
		}
		return $result;
	}

	function modify($p) {
		$this->connect();
		$p = $this->clean($p);
		extract($p);
		if(isset($remove)) {
			for($i=count($remove)-1;$i>=0; $i--) {
				$this->removeInUpdate($remove[$i]);
			}
		}
		if(isset($crd_id)) {
			for($j=count($crd_id)-1;$j>=0; $j--) {
				if($crd_quantity[$j]==0) {
					$this->removeInUpdate($crd_id[$j]);
					continue;
				} 
				$q = "UPDATE cart_detail SET crd_quantity=".$crd_quantity[$j]." WHERE crd_id=".$crd_id[$j];
				$this->update($q);
			}
		}
		$this->close();
	}

	function removeInUpdate($cartItemId) {
		$q = "DELETE FROM cart_detail WHERE crd_id=$cartItemId";
		$r = $this->delete($q);
		return $r;
	}

	function remove($cartItemId) {
		$this->connect();
		$q = "DELETE FROM cart_detail WHERE crd_id=$cartItemId";
		$r = $this->delete($q);
		$this->close();
		return $r;
	}

	function calculateWeightAndTaxes($cart,$province) {
		global $taxRates;
		$totalProductsWeight = 0;
		$taxableProductsAmount = 0;
		$taxableClassesAmount = 0;
		if (isset($cart['product'])) {
			foreach($cart["product"] as $product) {
				if($product['weight']) {
					$totalProductsWeight += (($product['weight'])/1000); // DO NOT MULTIPLY BY $product['crd_quantity'] because $product['weight'] already has quantity factored-in in the SQL request above. 
				}
				if($product["prd_taxable"]) {
					$taxableProductsAmount += ($product["prd_price"]*$product['crd_quantity']);
				}
			}
		}
		if (isset($cart['class'])) {
			foreach($cart["class"] as $class) {
				if($class["cls_taxable"]) {
					$taxableClassesAmount += ($class["cls_price"]*$class["crd_quantity"]);
				}
			}
		}
		$response = array(
				"weight" 	=>	$totalProductsWeight,
				"taxes"		=>	($taxableProductsAmount*$taxRates[$province]/100) + ($taxableClassesAmount*$taxRates['qc']/100)
			);
		return $response;
	}

	public function getShippingRateAndTaxes($order_uniqid,$addressDetail,$cartDetail,$cartSummary,$lan) {
		$shippingPostalCode = strtoupper(str_replace(" ", "", $addressDetail["ship_postalcode"]));
		$shippingProvince = $addressDetail["ship_province"];

		$weightAndTaxes = $this->calculateWeightAndTaxes($cartDetail,$shippingProvince);
		$productsSubtotalAmount = $cartSummary['product'][0]['subtotal'];		
		
		$taxesAmount_raw = number_format($weightAndTaxes['taxes'],2);
		$taxesAmount = Util::formatPrice($weightAndTaxes['taxes'],$lan);
		$shippingAmount_raw = 0;
		$shippingAmount = 0;
		$transitTime = 0;
		$error = false;
		$errorCode = 0;
		$errorMsg = "";

		$response = array(
			"taxesAmount"			=>	$taxesAmount,
			"taxesAmount_raw"		=>	$taxesAmount_raw,
			"shippingAmount"		=>	$shippingAmount,
			"shippingAmount_raw"	=>	$shippingAmount_raw,
			"transitTime"			=>	$transitTime,
			"error"					=>	$error,
			"errorCode"				=>	$errorCode,
			"errorMsg"				=>	$errorMsg,
			"pcErrorFreeDelivery"	=> 	false
		);

		if(isset($cartDetail["product"])) {
			$xmlRequest = '<?xml version="1.0" encoding="UTF-8"?>
							<mailing-scenario xmlns="http://www.canadapost.ca/ws/ship/rate-v2">
							  <customer-number>'.CP_CUSTOMERNUMBER.'</customer-number>
							  <contract-id>'.CP_CONTRACTID.'</contract-id>
							  <options>
							  	<option>
							  		<option-code>SO</option-code>
							  	</option>
							  </options>
							  <parcel-characteristics>
							    <weight>'.$weightAndTaxes['weight'].'</weight>
							  </parcel-characteristics>
							  <services>
							  	<service-code>DOM.RP</service-code>
							  </services>
							  <origin-postal-code>'.CP_ORIGIN_POSTALCODE.'</origin-postal-code>
							  <destination>
							    <domestic>
							      <postal-code>'.$shippingPostalCode.'</postal-code>
							    </domestic>
							  </destination>
							</mailing-scenario>';

			$curl = curl_init(CP_ENDPOINT);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($curl, CURLOPT_CAINFO, CP_CERTAUTH_LIST);
			
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlRequest);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, CP_API_CREDENTIALS);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
														'Content-Type: application/vnd.cpc.ship.rate-v2+xml', 
														'Accept: application/vnd.cpc.ship.rate-v2+xml', 
														'Accept-Language: '.$lan.'-CA'
						));
			$curl_response = curl_exec($curl);
			if(curl_errno($curl)){
				return json_encode($response);
			}
			curl_close($curl);

			// Debug
			$response["pc_response_payload"] = $curl_response;

			$doc = new DomDocument();
			$doc->loadXML($curl_response);
			$dueElt = $doc->getElementsByTagName("due"); // Full amount including taxes, gas surcharge, other fees, etc.
			$gstElt = $doc->getElementsByTagName("gst"); // GST amount (0 if not applied)
			$pstElt = $doc->getElementsByTagName("pst"); // PST
			$hstElt = $doc->getElementsByTagName("hst"); // HST
			
			$errorElt = $doc->getElementsByTagName("message");
			if($dueElt->length>0) {
				$transitTime = $doc->getElementsByTagName("expected-transit-time")->item(0)->nodeValue;
				if($productsSubtotalAmount<60 && $productsSubtotalAmount>0) {
					// Full taxes amount on delivery fees (charged by Post Canada)
					$taxesIncludedInDueElt = $gstElt->item(0)->nodeValue + $pstElt->item(0)->nodeValue + $hstElt->item(0)->nodeValue;
					// Delivery fees before taxes
					$dueMinusTaxes = $dueElt->item(0)->nodeValue - $taxesIncludedInDueElt;
					$shippingAmount = $dueMinusTaxes;
					
					// Add delivery taxes to the current taxes amount and update the response array.
					$taxesAmount_raw += $taxesIncludedInDueElt; 
					$taxesAmount = Util::formatPrice($taxesAmount_raw,$lan);
					$response["taxesAmount"] = $taxesAmount;
					$response["taxesAmount_raw"] = $taxesAmount_raw;
				}
			}
			else if($errorElt->length>0) {
				$error = true;
				$errorCode = $doc->getElementsByTagName("code")->item(0)->nodeValue;
				$errorMsg = "";
				if($errorCode=="1740") {
					$errorMsg = $doc->getElementsByTagName("description")->item(0)->nodeValue;
				}
				else if($productsSubtotalAmount>=60) {
					$response["pcErrorFreeDelivery"] = true;
				}
			}
		}
		$response["parcelWeight"] = $weightAndTaxes['weight'];
		//$response["cartJSON"] = $weightAndTaxes['cart'];
		
		$response["error"] = $error;
		$response["errorCode"] = $errorCode;
		$response["errorMsg"] = $errorMsg;
		$response["shippingAmount"] = Util::formatPrice($shippingAmount,$lan);
		$response["shippingAmount_raw"] = $shippingAmount;
		$response["transitTime"] = $transitTime;
		
		// Added OR condition to let the PayPal cart encryption process complete even if Canada Post reports an error (probably due to exceeding weight) when cart amount is higher than threshold and delivery is free anyways.
		if(!$error || (isset($response["pcErrorFreeDelivery"]) && $response["pcErrorFreeDelivery"])) {
			//$response["ppEncryptedCart"] = $this->makePaypalEncryptedCart($order_uniqid,$addressDetail,$cartDetail,$shippingAmount,$weightAndTaxes['taxes'],$lan);
			// One line updated for delivery tax separation
			$response["ppEncryptedCart"] = $this->makePaypalEncryptedCart($order_uniqid,$addressDetail,$cartDetail,$shippingAmount,$taxesAmount_raw,$lan);
		}
		// Two lines updated for delivery tax separation
		//$response["totalAmount"] = Util::formatPrice($cartSummary['product'][0]['subtotal'] + $cartSummary['class'][0]['subtotal'] + $weightAndTaxes['taxes'] + $shippingAmount,$lan);
		$response["totalAmount"] = Util::formatPrice($cartSummary['product'][0]['subtotal'] + $cartSummary['class'][0]['subtotal'] + $taxesAmount_raw + $shippingAmount,$lan);
		//$response["totalAmount_raw"] = $cartSummary['product'][0]['subtotal'] + $cartSummary['class'][0]['subtotal'] + number_format($weightAndTaxes['taxes'],2) + $shippingAmount;	
		$response["totalAmount_raw"] = $cartSummary['product'][0]['subtotal'] + $cartSummary['class'][0]['subtotal'] + number_format($taxesAmount_raw,2) + $shippingAmount;	
		return $response;
	}

	function makePaypalEncryptedCart($order_uniqid,$addressDetail,$cartDetail,$shippingAmount,$taxesAmount,$lan) {
		$ppStates = array(
						"ab"	=>	"Alberta",
						"bc"	=>	"British Columbia",
						"mb"	=>	"Manitoba",
						"nb"	=>	"New Brunswick",
						"nl"	=>	"Newfoundland",
						"ns"	=>	"Nova Scotia",
						"nu"	=>	"Nunavut",
						"nt"	=>	"Northwest Territories",
						"on"	=>	"Ontario",
						"pe"	=>	"Prince Edward Island",
						"qc"	=>	"Quebec",
						"sk"	=>	"Saskatchewan",
						"yt"	=>	"Yukon"	
					);
		$ppBasePayload = array(
				"cmd"				=>	"_cart",
				"cert_id"			=>	PPWPS_CERTID,
				"bn"				=>	PP_BN,
				"business"			=>	PP_ACCOUNT_ID,
				"_cart"				=>	"upload",
				"upload"			=>	"1",
				"paymentaction"		=>	"sale",
				"currency_code"		=>	"CAD",
				"lc"				=>	$lan."_CA",
				"charset"			=>	"UTF-8",
				"no_note"			=>	"1",
				"custom"			=>	$order_uniqid,
				"no_shipping"		=>	1,
				"notify_url"		=>	PP_IPN_URL,
				"return"			=>	BASE_SURL.$lan."/confirmation",
				"cancel_return"		=>	BASE_SURL.$lan."/confirmation",
				"rm"				=>	2,
				"cbt"				=>	constant(strtoupper("PP_RETURN_LABEL_$lan"))
			);

		$ppAddressPayload = array(
				"first_name"		=>	$addressDetail['bill_fname'],
				"last_name"			=>	$addressDetail['bill_lname'],
				"email"				=>	$addressDetail['bill_email'],
				"night_phone_a"		=>	substr($addressDetail['bill_tel'],0,3),
				"night_phone_b"		=>	substr($addressDetail['bill_tel'],3,3),
				"night_phone_c"		=>	substr($addressDetail['bill_tel'],6,4),
				"address1"			=>	$addressDetail['bill_street1'],
				"address2"			=>	$addressDetail['bill_street2'],
				"city"				=>	$addressDetail['bill_city'],
				"state"				=>	(strtolower($addressDetail['bill_country'])=="ca")?$ppStates[$addressDetail['bill_province']]:$addressDetail['bill_state'],
				"zip"				=>	(strtolower($addressDetail['bill_country'])=="ca")?$addressDetail['bill_postalcode']:$addressDetail['bill_zip'],
				"country"			=>	strtoupper($addressDetail['bill_country'])
			);

		$ppCartPayload = array();
		$counter = 1;
		if(isset($cartDetail["product"])) {
			foreach ($cartDetail["product"] as $product) {
				$ppCartPayload["item_name_$counter"] = $product["pdd_name"];
				$ppCartPayload["quantity_$counter"] = $product["crd_quantity"];
				$ppCartPayload["amount_$counter"] = $product["prd_price"];
				$ppCartPayload["item_number_$counter"] = $product["prd_id"];
				$counter++;	
			}
		}
		if(isset($cartDetail["class"])) {
			foreach ($cartDetail["class"] as $class) {
				$ppCartPayload["item_name_$counter"] = $class["cls_title"];
				$ppCartPayload["quantity_$counter"] = $class["crd_quantity"];
				$ppCartPayload["amount_$counter"] = $class["cls_price"];
				$ppCartPayload["item_number_$counter"] = $class["sch_id"].".0";
				$counter++;	
			}
		}

		$ppCartPayload["tax_cart"] = number_format($taxesAmount,2);
		$ppCartPayload["handling_cart"] = $shippingAmount;

		$ppPayload = array_merge($ppBasePayload,$ppAddressPayload,$ppCartPayload);

		$ppPayloadString = "";
		foreach ($ppPayload as $key => $value) {
			if ($value != "") {
				$ppPayloadString .= (DEV)?"<input type='hidden' name='$key' value=\"$value\"/>\n":"$key=$value\n";
			}
		}
		if(DEV)
			return $ppPayloadString;
		// Testing purposes only.
		//file_put_contents("pp_frm_tests/payload_encrypted_".time().".txt", $ppPayloadString);
		//file_put_contents("pp_frm_tests/payload_elements_".time().".txt", $ppPayloadString);
		// Added "-md SHA256" option on 20160930.
		$openssl_cmd = "(".OPENSSL." smime -sign -md SHA256 -signer ".PPWPS_OO_PUBLIC_KEY." -inkey ".PPWPS_OO_PRIVATE_KEY." -outform der -nodetach -binary <<_EOF_\n$ppPayloadString\n_EOF_\n) | " .OPENSSL." smime -encrypt -des3 -binary -outform pem ".PPWPS_PP_PUBLIC_KEY;
		exec($openssl_cmd, $output, $error);
		// Testing only (20160930)
		// file_put_contents("pp_frm_tests/cart_encrypted_".time().".txt", $output."Error: $error|||\n\n****************\n\nOutput: \n".var_export(implode("\n",$output),true));
		return implode("\n",$output);
	}
}
?>