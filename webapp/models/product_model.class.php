<?php
Class ProductModel extends CommonModel {
	function get($id) {
		$this->connect();
		$id = $this->clean($id,false);
		$q = "SELECT * FROM product 
        				JOIN product_detail 
                		ON pdd_prd_id_fk = prd_id 
                		WHERE prd_id = $id 
                		AND prd_active=1
                		AND pdd_lang='".$this->_lan."'";
        $r = $this->select($q);
		$this->close();
		if($r) {
			return $r;
		}
	}

	function getLinkedRecipes($sku) {
		$this->connect();
		$sku = $this->clean($sku,false);
		$q = "SELECT rcp_id,rcp_img_small,rcd_title,rcd_slug FROM recipe 
        				JOIN recipe_detail 
                		ON rcd_rcp_id_fk = rcp_id 
                		WHERE FIND_IN_SET('$sku',rcp_product_links) 
                		AND rcp_active=1
                		AND rcd_lang='".$this->_lan."'";
        $r = $this->select($q);
		$this->close();
		if($r) {
			return $r;
		}
	}
}
?>