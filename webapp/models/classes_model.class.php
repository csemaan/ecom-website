<?php
Class ClassesModel extends CommonModel {
	function get($instructor) {
		/**/
		$this->connect();
		$instructor = $this->clean($instructor,false);
        $q = "SELECT * FROM class 
        				JOIN class_schedule 
                		ON sch_cls_id_fk = cls_id 
                		WHERE cls_active=1
                		AND sch_active = 1
                		AND CONCAT(sch_date,' ',sch_time) >= DATE_SUB(NOW(), INTERVAL 7 DAY)
                		AND cls_lang='".$this->_lan."'";
        $q .= (isset($instructor)&&$instructor!="0")?" AND cls_instructor_slug='$instructor'":"";
        $q .= " ORDER BY sch_date ASC, FIELD(cls_id,".$this->getSortOrder('class').")";
		$r = $this->select($q);
		$this->close();
		if($r) {
			return $r;
		}
		
	}

	function getAvailableInstructors() {
		$this->connect();
		$q = "SELECT DISTINCT cls_instructor,cls_instructor_slug  FROM class 
        				JOIN class_schedule 
                		ON sch_cls_id_fk = cls_id 
                		WHERE cls_active = 1
                		AND sch_active = 1
                		AND CONCAT(sch_date,' ',sch_time) >= DATE_SUB(NOW(), INTERVAL 7 DAY)
                		AND cls_lang='".$this->_lan."'";
        $q .= " ORDER BY FIELD(cls_id,".$this->getSortOrder('class').")";
		$r = $this->select($q);
		$this->close();
		if($r) {
			return $r;
		}
	}
}
?>