<?php
Class ProductsModel extends CommonModel {
	/*
	 * 
	 * Get data in specified language for all products satisfying filters: 
	 *					category, type, status, keywords.
	 * 
	 */
	function get($category,$type) {
		$this->connect();
		$category = $this->clean($category,false);
		$type = $this->clean($type,false);
        $q = "SELECT * FROM product 
        				JOIN product_detail 
                		ON pdd_prd_id_fk = prd_id 
                		WHERE prd_cat_id_fk = $category 
                		AND prd_active=1
                		AND pdd_lang='".$this->_lan."'";
        $q .= (isset($type)&&$type!="0")?" AND prd_typ_id_fk=$type":"";
        $q .= " ORDER BY FIELD(prd_id,".$this->getSortOrder('product',$category).")";
		$r = $this->select($q);
		$this->close();
		if($r) {
			return $r;
		}
	}
}
?>