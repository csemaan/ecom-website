<?php
Class RecipesModel extends CommonModel {
	function get($kw) {
		$this->connect();
		if(isset($kw) && $kw) {
			$kw = $this->clean($kw,false);
			$re = preg_replace("/\s+/", "|", trim($kw));
		}
		$q = "SELECT * FROM recipe 
        				JOIN recipe_detail 
                		ON rcd_rcp_id_fk = rcp_id 
                		WHERE rcp_active=1
                		AND rcd_lang='".$this->_lan."'";
        $q .= (isset($re) && $re!="")?" AND (rcd_title REGEXP '$re' OR rcd_ingredients REGEXP '$re' OR rcd_preparation REGEXP '$re' OR rcd_keywords REGEXP '$re')":"";
        $q .= " ORDER BY FIELD(rcp_id,".$this->getSortOrder('recipe').")";
		$r = $this->select($q);
		$this->close();
		if($r) {
			return $r;
		}
	}
}
?>