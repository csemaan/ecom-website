<?php 
class PoliciesController extends Controller {
	function index($p=0) {
		// Array order determines display order
		$policies = array(
						array('privacy','foo_privacy'),
						array('use','foo_usepolicy'),
						array('cancellation','foo_cancellationpolicy'),
						array('shipping','foo_delivery_info')
		);
		$section = isset($p[0])?$p[0]:$p;
		if(!is_numeric($section) || $section<0 || !isset($policies[$section])) {
			$section = 0;
		}
		$this->set('staticView',true);
		$this->set("section",$section);
		$this->set("policies",$policies);
	}
}
?>