<?php 
class AboutController extends Controller {
	function index($p=0) {
		// Array order determines display order
		$about = array(
						array('about','abt_about'),
						array('news','abt_news')
		);
		$section = isset($p[0])?$p[0]:$p;
		if(!is_numeric($section) || $section<0 || !isset($about[$section])) {
			$section = 0;
		}
		$this->set('staticView',true);
		$this->set("section",$section);
		$this->set("about",$about);
	}
}
?>