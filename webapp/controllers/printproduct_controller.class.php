<?php 
class PrintproductController extends Controller {
	function index($p) {
		$id = (isset($p[0]))?$p[0]:0;

		if(!is_numeric($id) || $id==0) {
			Util::http_redirect(isset($_SESSION['currentProductsRoute'])?$_SESSION['currentProductsRoute']:'products/',$this->_lan);
		} 
		
		$this->set('printView',true);

		$availableTypes = $_SESSION['availableTypes'];
		$this->set('availableTypes',$availableTypes);

		$product = $this->_model->get($id);
		if(!isset($product[0])) {
			Util::http_redirect('error/'.join($p,'/'),$this->_lan);
		} 
		$this->set('product',$product[0]);

		$recipes = $this->_model->getLinkedRecipes($product[0]['prd_sku']);
		if(count($recipes)>0) {
			$this->set('recipes',$recipes);
		}		
	}
}
?>