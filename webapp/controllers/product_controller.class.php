<?php 
class ProductController extends Controller {
	function index($p) {
		$id = (isset($p[0]))?$p[0]:0;

		if(!is_numeric($id) || $id==0) {
			Util::http_redirect(isset($_SESSION['currentProductsRoute'])?$_SESSION['currentProductsRoute']:'products/',$this->_lan);
		} 
		
		$product = $this->_model->get($id);
		if(!isset($product[0])) {
			Util::http_redirect('error/'.join($p,'/'),$this->_lan);
		} 
		$this->set('product',$product[0]);
		$this->set('meta_title_override',$product[0]['pdd_name']);
		$this->set('meta_desc_override',($product[0]['pdd_desc1_text']||$product[0]['pdd_desc2_text']||$product[0]['pdd_desc3_text']));
		$this->set('fb_og_image',BASE_URL."data/images/product/".$product[0]['prd_img_large']);
		
		$recipes = $this->_model->getLinkedRecipes($product[0]['prd_sku']);
		if(count($recipes)>0) {
			$this->set('recipes',$recipes);
		}

		if(isset($_SESSION['categories'])) {
			$categories = $_SESSION['categories'];
			$this->set('categories',$categories);
		}
		else {
			$categories = $this->_model->getCategories();
			$this->set('categories',$categories);
			$_SESSION['categories'] = $categories;
		}
		/*
		if(isset($_SESSION['currentCategory'])) {
			$currentCategory = $_SESSION['currentCategory'];
			$this->set('currentCategory',$_SESSION['currentCategory']);
		}
		else {
			$currentCategory = 1;
			$this->set('currentCategory',$currentCategory);
			$_SESSION['currentCategory'] = $currentCategory;
		}
		*/
		$currentCategory = $product[0]["prd_cat_id_fk"];
		$this->set('currentCategory',$currentCategory);
		$_SESSION['currentCategory'] = $currentCategory;

		if(isset($_SESSION['availableTypes'])) {
			$availableTypes = $_SESSION['availableTypes'];
			$this->set('availableTypes',$availableTypes);
		}
		else {
			$availableTypes = $this->_model->getAvailableTypes();
			$this->set('availableTypes',$availableTypes);
			$_SESSION['availableTypes'] = $availableTypes;
		}
		if(isset($_SESSION['currentProductsRoute'])) {
			$currentProductsRoute = $_SESSION['currentProductsRoute'];
			$this->set('currentProductsRoute',$currentProductsRoute);
		}
		else {
			$currentProductsRoute = Util::baseUrl('products/'.$currentCategory,$this->_lan);
			$this->set('currentProductsRoute',$currentProductsRoute);
			$_SESSION['currentProductsRoute'] = $currentProductsRoute;
		}
		if(isset($_SESSION['productsIdSet'])) {
			$productsIdSet = $_SESSION['productsIdSet'];
			$this->set('productsIdSet',$productsIdSet);
		}
	}
}
?>