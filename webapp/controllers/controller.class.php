<?php 
class Controller {
	protected $_model;
    protected $_controller;
    protected $_action;
    protected $_template;
    protected $_lan;
    protected $_ruri;
    protected $cartId = 0; 
 
    function __construct($model, $controller, $action, $lan, $requestURI) {
        $this->_controller = $controller;
        $this->_action = $action;
        $this->_model = new $model($lan);
        $this->_template = new Template($controller,$action,$lan,$requestURI);
        $this->_lan = $lan;
        $this->_ruri = $requestURI;

        if(isset($_SESSION['extraSlugs']) && isset($_SESSION['allCatsAndTypes'])) { 
            $this->set('allCatsAndTypes',$_SESSION['allCatsAndTypes']);   
            $this->set('extraSlugs',$_SESSION['extraSlugs']);
        }
        else {
            $allCatsAndTypes = $this->_model->getAllCategoriesAndTypes();
            $extraSlugs = $this->_model->getAllCatTypesSlugs($allCatsAndTypes);
            $this->set('allCatsAndTypes',$allCatsAndTypes);
            $this->set('extraSlugs',$extraSlugs);
            $_SESSION['allCatsAndTypes'] = $allCatsAndTypes; 
            $_SESSION['extraSlugs'] = $extraSlugs; 
        }
        // Cart handling
        $this->cartId = $this->createCart();
        $this->set("cartSummary",$this->_model->cartSummary($this->cartId));
    }
 
    function set($name,$value) {
        $this->_template->set($name,$value);
    }

    function createCart(){
        if(isset($_COOKIE['oocart'])) {
            $cartId = $this->_model->updateCartSerial($_COOKIE['oocart']);
        }
        if(!isset($_COOKIE['oocart']) || $cartId==false) {
            $cartSerial = md5(uniqid('',true));
            setcookie('oocart',$cartSerial,time()+1500*24*3600,'/');
            $cartId = $this->_model->createCart($cartSerial);
        }
        return $cartId;
    }
 
    function __destruct() {
        $this->_template->render();
    }
}
?>