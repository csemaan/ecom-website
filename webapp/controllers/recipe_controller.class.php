<?php 
class RecipeController extends Controller {
	function index($p) {
		$id = (isset($p[0]))?$p[0]:0;

		if(!is_numeric($id) || $id==0) {
			Util::http_redirect('recipes/',$this->_lan);
		} 
		
		$recipe = $this->_model->get($id);
		if(!isset($recipe[0])) {
			Util::http_redirect('error/'.join($p,'/'),$this->_lan);
		} 
		$this->set('recipe',$recipe[0]);
		$this->set('meta_title_override',$recipe[0]['rcd_title']);
		$this->set('fb_og_image',BASE_URL."data/images/recipe/".$recipe[0]['rcp_img_large']);

		if(isset($recipe[0]['rcp_product_links']) && $recipe[0]['rcp_product_links'] != "") { 
			// This is important to reset the back to list button in the product view page.
			unset($_SESSION['currentProductsRoute']);
			$this->set('relatedProducts',$this->_model->getRelatedProducts($recipe[0]['rcp_product_links']));
		}
		
		if(isset($_SESSION['recipesIdSet'])) {
			$recipesIdSet = $_SESSION['recipesIdSet'];
			$this->set('recipesIdSet',$recipesIdSet);
		}
	}
}
?>