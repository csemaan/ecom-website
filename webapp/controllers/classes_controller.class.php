<?php 
class ClassesController extends Controller {
	function index($p='1') {
		$this->get($p);
	}

	function get($p) {
		$instructor = (isset($p[0]))?$p[0]:'0';

		$this->set('currentInstructor',$instructor);

		$classes = $this->_model->get($instructor);
		$this->set('classes',$classes);

/*
	DISABLE CACHE DURING DEV
*/
		/*
		if(isset($_SESSION['availableInstructors'][$this->_lan])) {
			$availableInstructors = $_SESSION['availableInstructors'][$this->_lan];
		}
		else {
			$availableInstructors = $this->_model->getAvailableInstructors();
			$_SESSION['availableInstructors'][$this->_lan] = $availableInstructors;
		}
		$this->set('availableInstructors',$availableInstructors);
		*/
		$availableInstructors = $this->_model->getAvailableInstructors();
		$this->set('availableInstructors',$availableInstructors);
	}
}
?>