<?php 
class PrintrecipeController extends Controller {
	function index($p) {
		$id = (isset($p[0]))?$p[0]:0;

		if(!is_numeric($id) || $id==0) {
			Util::http_redirect('recipes/',$this->_lan);
		} 
		
		$this->set('printView',true);

		$recipe = $this->_model->get($id);
		if(!isset($recipe[0])) {
			Util::http_redirect('error/'.join($p,'/'),$this->_lan);
		} 
		$this->set('recipe',$recipe[0]);

		if(isset($recipe[0]['rcp_product_links']) && $recipe[0]['rcp_product_links'] != "") { 
			// This is important to reset the back to list button in the product view page.
			$this->set('relatedProducts',$this->_model->getRelatedProducts($recipe[0]['rcp_product_links']));
		}
	}
}
?>