<?php 
class StoresController extends Controller {
	function index() {
		$this->set('staticView',true);
		$this->set('costcoAddresses',$this->_model->getCostcoAddresses(GD_COSTCO_TIMETABLE));
		// Uncomment following line to activate Google Drive Opening Hours
		//$this->set('openingHours',$this->_model->getOpeningHours(GD_OPENING_HOURS,$this->_lan));
	}
}
?>