<?php 
class ErrorController extends Controller {
	function _404() {
		$this->set('staticView',true);
		header('HTTP/1.0 404 Not Found');
	}

	function _503() {
		$this->set('staticView',true);
		header('HTTP/1.0 503 Service Unavailable');
	}

	function index() {
		$this->_404();
	}
}
?>