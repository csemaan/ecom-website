<?php 
class CartController extends Controller {
	function index() {
		$this->set('nocache',true);
		if(isset($_POST["action"])){
			$_POST['crt_id'] = $this->cartId;
			switch ($_POST["action"]) {
				case 'add':
					$this->add($_POST);
					break;
				case 'remove':
					$this->remove();
					break;
				case 'update':
					$this->update($_POST);
					break;
				case 'shippingfees':
					$this->set('ajaxRequest',true);
					$_SESSION["address_detail"] = $_POST;
					if(!isset($_SESSION["order_uniqid"])) {
						$order_uniqid = uniqid("oo",true);
						$_SESSION["order_uniqid"] = $order_uniqid;
					}
					else {
						$order_uniqid = $_SESSION["order_uniqid"];
					}
					$this->calculateShippingAndTaxes($order_uniqid,$_POST,$_SESSION["cart_detail"],$_SESSION["cart_summary"],$this->_lan);
				default:
					break;
			}
		}
		$this->get();
		if(isset($_SESSION['currentProductsRoute'])) {
			$this->set('currentProductsRoute',$_SESSION['currentProductsRoute']);
		}
	}

	function get() {
		$cartDetail = $this->_model->get($this->cartId);
		$this->set('cartContent',$cartDetail);
		$_SESSION["cart_detail"] = $cartDetail;
		$cartSummary = $this->_model->cartSummary($this->cartId);
		$this->set('cartSummary',$cartSummary);
		$_SESSION["cart_summary"] = $cartSummary;
	}

	function add($p) {
		$this->_model->add($p);
	}

	function remove() {

	}

	function update($p) {
		$this->_model->modify($p);
	}

	function calculateShippingAndTaxes($order_uniqid,$addressDetail,$cartDetail,$cartSummary,$lan) {
		$shippingRateAndTaxes = $this->_model->getShippingRateAndTaxes($order_uniqid,$addressDetail,$cartDetail,$cartSummary,$lan);
		$_SESSION["shipping_taxes_detail"] = $shippingRateAndTaxes;
		$_SESSION["pcShippingTransitTime"] = $shippingRateAndTaxes["transitTime"];
		$this->set('shippingRateAndTaxes',json_encode($shippingRateAndTaxes));
	}
}
?>