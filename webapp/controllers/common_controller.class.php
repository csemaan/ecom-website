<?php 
class CommonController extends Controller {
	function index() {
		$allCatsAndTypes = $this->_model->getAllCategoriesAndTypes();
		$this->set('allCatsAndTypes',$allCatsAndTypes);
		$this->set('extraSlugs',$this->_model->getAllCatTypesSlugs($allCatsAndTypes));
	}
}
?>