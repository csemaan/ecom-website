<?php 
class ProductsController extends Controller {
	function index($p='1') {
		$this->get($p);
	}

	function get($p) {
		$category = (isset($p[0]))?$p[0]:'1';
		$type = (isset($p[2]))?$p[2]:'0';

		if(isset($_SESSION['availableTypes'])) {
			$availableTypes = $_SESSION['availableTypes'];
			$this->set('availableTypes',$availableTypes);
		}
		else {
			$availableTypes = $this->_model->getAvailableTypes();
			$this->set('availableTypes',$availableTypes);
			$_SESSION['availableTypes'] = $availableTypes;
		}
		
		if(isset($_SESSION['categories'])) {
			$categories = $_SESSION['categories'];
			$this->set('categories',$categories);
		}
		else {
			$categories = $this->_model->getCategories();
			$this->set('categories',$categories);
			$_SESSION['categories'] = $categories;
		}

		if(!in_array($category, Util::pluck($categories,'cat_id'))) {
			$_SESSION['currentProductsRoute'] = Util::baseUrl('products',$this->_lan);
			Util::http_redirect('products',$this->_lan);
		}
		$types = $this->_model->getTypes($category);
		if($type != 0 && !in_array($type, Util::pluck($types,'typ_id'))) {
			$_SESSION['currentProductsRoute'] = Util::baseUrl('products/'.$category,$this->_lan);
			Util::http_redirect('products/'.$category,$this->_lan);
		} 
		$this->set('types',$types);
		$products = $this->_model->get($category,$type);
		$this->set('products',$products);
		if($products && count($products)>0) {
			$productsIdSet = Util::pluck($products,'prd_id');
			$this->set('productsIdSet',$productsIdSet);
			$_SESSION['productsIdSet'] = $productsIdSet;
		}
		$this->set('currentCategory',$category);
		$this->set('currentType',$type);
		$_SESSION['currentCategory'] = $category;
		$_SESSION['currentProductsRoute'] = Util::baseUrl('products/'.join($p,'/'),$this->_lan);
	}
}
?>