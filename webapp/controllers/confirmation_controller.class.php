<?php 
class ConfirmationController extends Controller {
	function index() {
		$txDate = date("Ymd");
		$txTimestamp = time();
		if(isset($_POST["action"]) && $_POST["action"]=="prepare_order") {
			$this->set('ajaxRequest',true);
			$response = true;
			if(!isset($_SESSION["order_uniqid"]) || !isset($_SESSION["shipping_taxes_detail"]) || !isset($_SESSION["address_detail"]) || !isset($_SESSION["cart_summary"]) || !isset($_SESSION["cart_detail"])) {
				$response = false;
			}
			else {
				// Add gift message if it is changed after Shipping Calculation and before Payment.
				$_SESSION["address_detail"]["gift_message"] = $_POST["gift_message"];
				$orderId = $this->preProcessOrder($_SESSION["order_uniqid"], $_SESSION["cart_summary"],$_SESSION["address_detail"],$_SESSION["shipping_taxes_detail"]);
				if($orderId) {
					//$_SESSION["temp_order_id"] = $orderId;
					Util::subscribeToNewsletter($this->_lan,$_SESSION["address_detail"]["bill_email"]);
					$this->removeCart();
					$response = true;
				}
				else {
					$response = false;
				}
			}
			$this->set('prepareOrder',json_encode($response));
		}
		else {
			$status = isset($_POST)?$_POST:0;
			if(!$status) {
				header("Location:".BASE_URL);
			}
			if(isset($_POST["ipn_track_id"])) {
				$ipnStatus = $this->doIPNAuthentication();
				if($ipnStatus && $ipnStatus=="verified") {
					// Set the template to no-view mode
					$this->set('noView',true);
					$orderId = $this->processOrder($_POST);
					// If order process is complete ...
					if($orderId) { 
						// Save transaction to file for reference.
						file_put_contents(PRIVATE_PATH."pp_ipn/ipn_".$_POST['txn_id']."_".$txDate.".txt", var_export($_POST,true));
						
						// Send confirmation email.
						$orderDetail = $this->getOrderDetail($orderId);
						$lan = $orderDetail["order"][0]["ord_lang"];
						$this -> sendConfirmationEmail($lan,$orderDetail);
					}
					else {
						// Save error situation to file for reference.
						file_put_contents(PRIVATE_PATH."pp_ipn/errors/error_ipn_verified_".$txDate."_".$txTimestamp.".txt", var_export($_POST,true));
					}
				}
				else {
					// Save error situation to file for reference.
					file_put_contents(PRIVATE_PATH."pp_ipn/errors/error_ipn_not_verified_".$txDate."_".$txTimestamp.".txt", var_export($_POST,true));
				}
			}
			else if(isset($_POST["txn_id"])) {
				$this->set('staticView',true);
				$orderId = $this->processOrder($_POST);
				file_put_contents(PRIVATE_PATH."pp_pdt/pdt_".$txDate."_".$_POST['txn_id'].".txt", var_export($_POST,true));
				if($orderId) { 
					$orderDetail = $this->getOrderDetail($orderId);
					$this->set('orderDetail',$orderDetail);
					$this->set('gaTrackingString',$this->_model->getGAeComTrackingJS($orderDetail));
				}
			}
		}
	}

	function processOrder($ppData) {
		return $this->_model->processOrder($ppData);
	}

	function preProcessOrder($order_uniqid,$cartSummary,$address,$fees) {
		return $this->_model->preProcessOrder($order_uniqid,$cartSummary,$address,$fees,$this->cartId);
	}

	function removeCart() {
		$this->_model->removeCart($this->cartId);
		if(isset($_SESSION["shipping_taxes_detail"])) {
			unset($_SESSION["shipping_taxes_detail"]);
		}
		if(isset($_SESSION["address_detail"])) {
			unset($_SESSION["address_detail"]);
		}
		if(isset($_SESSION["cart_summary"])) {
			unset($_SESSION["cart_summary"]);
		}
		if(isset($_SESSION["cart_detail"])) {
			unset($_SESSION["cart_detail"]);
		}
		if(isset($_SESSION["order_uniqid"])) {
			unset($_SESSION["order_uniqid"]);
		}
		$this->set("cartSummary",array());
	}

	function getOrderDetail($orderId) {
		return $this->_model->getOrderDetail($orderId);
	}

	function renderEmailString($emailTemplate, $vars){
		if (is_array($vars) && !empty($vars)) {
			extract($vars);
		}
		ob_start();
		include $emailTemplate;
		return ob_get_clean();
	}

	function sendConfirmationEmail($lan,$data) {
		$to  = $data["order"][0]["cli_email"];
		$subject = 'Olive & Olives'.(($lan=="fr")?" : Commande # ":": Order # ").$data["order"][0]["ord_number"];
		$message = $this->renderEmailString("webapp/views/orderemail.$lan.php",$data);
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$headers .= 'From: '.EMAIL_FROM. "\r\n";
		$headers .= 'Bcc: '.EMAIL_BCC.",".EMAIL_DEBUG."\r\n";
		//$headers .= 'Bcc: '.EMAIL_BCC."\r\n";
		$sendmailReturnPath = "-f".EMAIL_RETURNPATH; // Required to set the return-path. Otherwise return-path is set to MT main username.
		mail($to, $subject, $message, $headers,$sendmailReturnPath);
	}

	function doIPNAuthentication() {
		$raw_post_data = file_get_contents('php://input');
		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
		foreach ($raw_post_array as $keyval) {
			$keyval = explode ('=', $keyval);
			if (count($keyval) == 2)
				$myPost[$keyval[0]] = urldecode($keyval[1]);
		}
		$req = 'cmd=_notify-validate';
		if(function_exists('get_magic_quotes_gpc')) {
			$get_magic_quotes_exists = true;
		}
		foreach ($myPost as $key => $value) {
			if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
				$value = urlencode(stripslashes($value));
			} else {
				$value = urlencode($value);
			}
			$req .= "&$key=$value";
		}

		$ch = curl_init(PP_PAYMENT_ENDPOINT);
		if ($ch == FALSE) {
			return FALSE;
		}
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

		// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
		// of the certificate as shown below. Ensure the file is readable by the webserver.
		// This is mandatory for some environments.

		//$cert = __DIR__ . "./cacert.pem";
		//curl_setopt($ch, CURLOPT_CAINFO, $cert);

		$res = curl_exec($ch);
		curl_close($ch);
		
		// Inspect IPN validation result and act accordingly

		if (strcmp ($res, "VERIFIED") == 0) {
			return "verified";
			// check whether the payment_status is Completed
			// check that txn_id has not been previously processed
			// check that receiver_email is your PayPal email
			// check that payment_amount/payment_currency are correct
			// process payment and mark item as paid.
		} 
		else if (strcmp ($res, "INVALID") == 0) {
			return "invalid";
		}

	}
}
?>