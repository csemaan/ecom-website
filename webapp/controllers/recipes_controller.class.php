<?php 
class RecipesController extends Controller {
	function index($p=false) {
		if(isset($_POST['kw'])) {
			$this->set('ajaxRequest',true);
			$this->get($_POST['kw']);
		}
		else {
			if(!$p) {
				setcookie('oo_rcp_kw','',time()-1,'/');
			}
			$this->get();
		}
	}

	function get($kw=false) {
		$recipes = $this->_model->get($kw);
		$this->set('recipes',$recipes);
		if($recipes && count($recipes)>0) {
			$recipesIdSet = Util::pluck($recipes,'rcp_id');
			$this->set('recipesIdSet',$recipesIdSet);
			$_SESSION['recipesIdSet'] = $recipesIdSet;
		}

	}
}
?>