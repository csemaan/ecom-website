<?php
class PrintTemplate {
	protected $variables = array();
	protected $_controller;
	protected $_action;
	protected $_lan;
	protected $_ruri;

	function __construct($controller,$action,$lan,$ruri) {
		$this->_controller = $controller;
		$this->_action = $action;
		$this->_lan = $lan;
		$this->_ruri = $ruri;
	}

	function set($name,$value) {
		$this->variables[$name] = $value;
	}

    function render() {
		extract($this->variables);
		$lan = $this->_lan;
		$ruri = $this->_ruri;
		$pageMenu = $this->_controller;
		include(ROOT.'webapp/texts/info.txt.php');
		include(ROOT.'webapp/texts/'.$this->_lan.'/common.txt.php');
		include(ROOT.'webapp/texts/'.$this->_lan.'/printheader.txt.php');
		include(ROOT.'webapp/texts/'.$this->_lan.'/'.$this->_controller.'.txt.php');
		include(ROOT.'webapp/texts/'.$this->_lan.'/printfooter.txt.php');
		
		if(isset($ajaxRequest) && $ajaxRequest) {
			include(ROOT.'webapp/ajax_views/'.$this->_controller.'.'.$this->_action.'.php');
		}
		else {
			include(ROOT.'webapp/views/header.inc.php');
			if(isset($staticView) && $staticView==true) {
				include(ROOT.'webapp/views/'.$this->_controller.'.'.$this->_action.'.'.$lan.'.php');	
			}
			else {
				include(ROOT.'webapp/views/'.$this->_controller.'.'.$this->_action.'.php');
			}
			include(ROOT.'webapp/views/footer.inc.php');
		}
    }
}