<?php 
class Sql {
	protected $db;
	protected $result;
	protected $_lan;
	
	function __construct($lan) {
		$this->_lan = $lan;
	}

	function connect() {
		$this->db = new mysqli(MY_HOST,MY_USER,MY_PASS,MY_DBNAME);
		$this->db->query("SET NAMES 'UTF8'");
	}
	
	function close() {
		$this->db->close();
	}
	
	function clean($data,$isArray=true) {
		// Incompatible with PHP<5.4
		if($isArray) {
			array_walk($data,
				function(&$v,$k) {
					if(is_numeric($v)) {
						$v = (double)$v;
					}
					else {
						if(!is_array($v)) {
							$v = trim($this->db->real_escape_string($v));
						}
						else{
							$v = $this->clean($v);
						}
					}
				}
			);
		}
		else {
			if(is_numeric($data)) {
				$data = (double)$data;
			}
			else {
				$data = trim($this->db->real_escape_string($data));
			}
		}
		return $data;
	}
	
	function setNull($v) {
		if(!$v && $v!=="0" && $v!==0) return "NULL";
		return $v;
	}

	function doQuery($q) {
		$this->result = $this->db->query($q);
	}
	
	function select($q) {
		$this->doQuery($q);
		$resultArray = array();
		while($record = $this->result->fetch_assoc()) {
			$resultArray[] = $record;
		}
		return $resultArray;
	}
	
	function update($q) {
		$this->doQuery($q);
		return $this->db->affected_rows;
	}
	
	function insert($q) {
		$this->doQuery($q);
		return $this->db->insert_id;
	}
	
	function delete($q) {
		$this->doQuery($q);
		return $this->db->affected_rows;
	}
}
?>