<?php 
class Util {
	public static function getLan() {
		$lan = 'fr';
		if(isset($_COOKIE['oolan'])) {
			$lan = $_COOKIE['oolan'];
		}
		return $lan;
	}

	public static function setLan($lan='fr') {
		global $availableLanguages;
		if(!in_array($lan, $availableLanguages)) {
			return false;
		}
		setcookie('oolan',$lan,time()+365*24*3600,'/');
		return true;
	}

	public static function baseUrl($node,$lan) {
		global $urlNodes;
		$nodeArray = explode('/',trim($node,'/'));
		array_walk($nodeArray,function(&$val,$key,$p) {
						$val = ($p[1]=='fr')?(isset($p[0][$val])?$p[0][$val]:$val):$val;
					},array($urlNodes,$lan));
		// return BASE_URL.$lan.'/'.implode('/',$nodeArray);
		// Updated on September 29, 2014 to allow for HTTPS browsing of the whole site.
		return ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] = 'on')?BASE_SURL:BASE_URL).$lan.'/'.implode('/',$nodeArray);
	}

	public static function toggleRouteLan($node,$lan,$extraSlugs) {
		global $urlNodes;
		$nodeArray = explode('/',trim($node,'/'));
		array_walk($nodeArray,function(&$val,$key,$p) {
						if(!is_numeric($val)) {
							$val = ($p[1]=='fr')?(isset($p[0][$val])?$p[0][$val]:''):array_search($val,$p[0]);
						}
					},array(array_merge($urlNodes,$extraSlugs),$lan));
		return BASE_URL.trim(implode('/',$nodeArray),'/');
	}

	public static function toggleLan($requestURI,$extraSlugs) {
		if(stripos($requestURI, '/fr')!==false) { 
			self::setLan('fr');
			return self::toggleRouteLan($requestURI,'en',$extraSlugs);
		}
		else {
			self::setLan('en');
			return self::toggleRouteLan($requestURI,'fr',$extraSlugs);
		}
	}

	public static function slugify($text){
		setlocale(LC_CTYPE, 'en_US.utf8');
		$text = preg_replace('#[^\\pL\d]+#u', '-', $text);
		$text = trim($text, '-');
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		$text = strtolower($text);
		$text = preg_replace('#[^-\w]+#', '', $text);
		return $text;
	}

	public static function pluck($array,$eltIndex){
		$plucked = array();
		foreach($array as $elt) {
			$plucked[] = $elt[$eltIndex];
		}
		return $plucked;
	}

	public static function upluck($array,$eltIndex){
		$plucked = array();
		foreach($array as $elt) {
			if(!in_array($elt[$eltIndex], $plucked))
				$plucked[] = $elt[$eltIndex];
		}
		return $plucked;
	}	

	public static function http_redirect($route,$lan) {
		header('Location: '.self::baseUrl($route,$lan));
	}

	public static function makeMailtoUrl($subject,$body) {
		return "mailto:?subject=".str_replace("+"," ",urlencode($subject))
					."&amp;body=".str_replace("+"," ",urlencode(sprintf($body,"http://"
						.$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"])));
	}

	public static function makeClassesInquiryMailtoUrl($to,$subject,$body) {
		return "mailto:$to?subject=".str_replace("+"," ",urlencode($subject))
					."&amp;body=".str_replace("+"," ",urlencode($body));
	}

	public function formatPrice($amount,$lan) {
		$amount = number_format($amount,2);
		if($lan=='fr') {
			setlocale(LC_MONETARY, 'fr_CA');
			//return money_format('%!.2n $', $amount);
			$amount = str_replace(',',' ',$amount);
			return str_replace('.', ',', $amount).' $';
		}
		else {
			return '$'.$amount;
		}
	}

	public function isNew($dateStr) {
		$date = new DateTime($dateStr);
		$today = new DateTime();
		$today->sub(new DateInterval('P30D'));
		return ($today<$date)?true:false;
	}

	public function makeDateTime($date,$time,$lan) {
		setlocale(LC_TIME, $lan."_".strtoupper($lan));
		$format = ($lan=="en")?"%A, %B %e, %G at %l:%M%p":"le %A %e %B %G, &agrave; %k h %M";
		return utf8_encode(str_replace("h 00","h",ucfirst(strftime($format,strtotime($date." ".$time)))));
	}
	public function makeDate($date,$lan) {
		setlocale(LC_TIME, $lan."_".strtoupper($lan));
		$format = ($lan=="en")?"%A, %B %e, %G":"le %A %e %B %G";
		return utf8_encode(ucfirst(strftime($format,strtotime($date))));
	}
	public function makeDateSpan($dateFrom,$dateTo,$lan) {
		setlocale(LC_TIME, $lan."_".strtoupper($lan));
		$timeFrom = strtotime($dateFrom);
		$timeTo = strtotime($dateTo);
		
		if($dateFrom) {
			$sameMonth = (date("n",$timeFrom)==date("n",$timeTo));
			$sameYear = (date("Y",$timeFrom)==date("Y",$timeTo));
			
			$formatFrom = ($lan=="en")?"From %B %e ".($sameYear?"":"%Y")."  to ":"Du %e ".($sameMonth?"":"%B ")."  ".($sameYear?"":"%Y")." au ";
		}
		else {
			$formatFrom = ($lan=="en")?"Until ":"Jusqu'au ";	
		}

		$formatTo = ($lan=="en")?"%B %e %G":"%e %B %G";
		return utf8_encode(strftime($formatFrom,$timeFrom).strftime($formatTo,$timeTo));
	}

	public static function subscribeToNewsletter($lan,$emailAddress) {
		$listIds = array("en"=>"19a192cbf3","fr"=>"bec6acadbc");
		$result = file_get_contents("http://oliveolives.us2.list-manage1.com/subscribe/post-json?u=b5677d839a79ad7c42449c495&id=".$listIds[$lan]."&EMAIL=".$emailAddress."&c=?");
		return trim($result,"?()");
	}
}
?>