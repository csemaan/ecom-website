if(!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g,'');
  };
}

function checkValidity(f) {
	var elts = f.elements;
	var val = "";
	var re = null;
	var frmIsValid = true;
	for(var i=0; i<elts.length; i++) {
		val = $(elts[i]).val().trim();
		if(elts[i].disabled==false && ($(elts[i]).attr("required") || ($(elts[i]).attr("pattern") && val != ""))) {
			re = new RegExp('^' + $(elts[i]).attr("pattern") + '$',"i");
			if(val.search(re) == -1) {
				$(elts[i]).addClass("invalid");
				frmIsValid = false;
			}
			else if($(elts[i]).attr("maxvalue")) {
				if(val>$(elts[i]).attr("maxvalue")*1) {
					$(elts[i]).removeClass("invalid");
					$(elts[i]).addClass("invalid-maxvalue");
					frmIsValid = false;	
				}
				else {
					$(elts[i]).removeClass("invalid-maxvalue");
				}
			}
			else {
				$(elts[i]).removeClass("invalid");
				$(elts[i]).removeClass("invalid-maxvalue");
			}
		}
	}
	return frmIsValid;
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = escape(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return unescape(c.substring(nameEQ.length, c.length));
    }
    return null;
}


function eraseCookie(name) {
    createCookie(name, "", -1);
}

// Default Namespace.
(function() {
	// Config that can be changed
	var slideshowAutoStart = true;
	var slideshowSlideWidth = 1380;
	var slideshowSpeed = 300
	var slideshowDelay = 8000; // 8 seconds
	var slideshowStopAfter = 60000; // 60 seconds
	
	// Globals.
	var ssDownscaleTimer = null;
	var ssAutoStartTimer = null;
	var labelTBC = "";

	// Document Ready
	$(function() {
		setScrollToTop($('#top-link'));
		ssDownscaleWidth();
		if(slideshowAutoStart) {
			ssStart();
		}
		$(".ssc_dot").on('click',ssMoveToSlide);
		$(".ssc_left").on('click',ssMoveToPreviousSlide);
		$(".ssc_right").on('click',ssMoveToNextSlide);

		// Shopping Cart
		labelTBC = $(".cs_shippingAmount").html();
		$("td",$(".cs_receiptColumnProductList tr").last()).addClass("last");
		$(".add_to_cart,.cs_cartupdatebtn").on('click',function(event){
			if(!$(this).hasClass("cs_no_action")) {
				event.preventDefault();
				$form = $(this).closest('form');
				$form.submit();
			}
		});
		$("form").on('submit',function(event){
			$form = $(this);
			if(checkValidity(this)) {
				if(!$form.hasClass("cs_newsletter_form")) {
					return true;
				}
				else {
					subscribeNewsletter($form);
					return false;
				}
			}
			else {
				$("input.invalid",$form).each(function(index,elt){
					$(elt).addClass("invalid-pattern");
					$(elt).removeClass("invalid-maxvalue");
					var $tooltip = $('<div class="cs_tooltip">'+$form.data('invalid-msg')+'</div>');
					$(elt).after($tooltip);
					$tooltip.fadeIn();
					setTimeout(function() {$tooltip.remove();$(elt).removeClass("invalid-pattern");},3000);
					$(elt).attr("title",$form.data('invalid-msg'));
				});
				$("input.invalid-maxvalue",$form).each(function(index,elt){
					$(elt).addClass("invalid-maxvalue");
					$(elt).removeClass("invalid-pattern");
					var $tooltip = $('<div class="cs_tooltip">'+$form.data('invalid-maxvalue-msg')+' (max:&nbsp;'+ $(elt).attr('maxvalue') +')</div>').css("top",$(elt).outerHeight()+5+'px').css("left",-15+'px');
					$(elt).after($tooltip);
					$tooltip.fadeIn();
					setTimeout(function() {$tooltip.remove();$(elt).removeClass("invalid-maxvalue");},3000);
					$(elt).attr("title",$form.data('invalid-maxvalue-msg'));
				});
				return false;
			}
		});
		$(".cs_proceedToCheckout").on("click",checkoutStep1);
		$(".cs_countryChoice").on("change",changeBillingCountry)
		$(".cs_sameBillShip,.cs_diffBillShip").on('click',copyBillingAddress);
		$('.cs_shippingbilling_btn').on('click',calculateShipping);
		$(".cs_updateAddressBtn").on("click",cartDisablePayment);
		$(".cs_payWithPaypal").on("click",payWithPaypal);

		// Recipe Search
		//$(".searchBox").on('keyup',getRecipeByKeywords);
		$("input.searchBox").on("keyup", getRecipeByKeywords).on("keydown",function(){
									clearTimeout(window.instantSearchTimoutId);
								});
		$(".cs_recipeList").on("click",".cs_recipeLink",function(event) {
			createCookie("oo_rcp_kw",$("input.searchBox").val(),false);
			console.log($("input.searchBox").val());
		});
		if($('.cs_recipeList').eq(0)) {
			if(document.location.href.search(/\/kw/)!=-1) {
				$("input.searchBox").val(readCookie("oo_rcp_kw")).focus().keyup();
			}
		}

		// Handle Placeholders 
		$('.cs_placeholder').each(function () {
	        var placeholder = $(this).attr('data-placeholder');
	        $(this).val(placeholder);
	    });
	    $('.cs_placeholder').focus(function () {
	        if($(this).val() == $(this).attr('data-placeholder')){
	            $(this).val('');
	        }
	        $(this).addClass("cs_black");
	    }).blur(function(){
	        if($(this).val() == ''){
	          $(this).val($(this).attr('data-placeholder'));
	          $(this).removeClass("cs_black");
	        }
	    });

	    // handle popup window links
	    $('.cs_popuplink').on('click',openPopupWindow);

	    // Handle Newsletter Subscribe Button
	    $(".cs_newsletter_btn").on("click", function() {
	    	$(this).closest("form").submit();
	    });
	});

	/*
		Jeff's scroll-to-top.
	*/
	function setScrollToTop($elt) {
		$(window).scroll(function() {
			($(window).scrollTop() >= 500)
				?$elt.fadeIn(1000)
				:$elt.fadeOut(1000);
		});
		$elt.on('click',function(e) {
		    	$("html,body").animate({scrollTop:0},300);
		    	e.preventDefault();
		});
	}

	// Slideshow - Downscale on Window resize
	function ssDownscaleWidth() {
		if(ssDownscaleTimer==null) {
			$(window).on("resize",function() {
			    clearTimeout(ssDownscaleTimer);
			    ssDownscaleTimer = setTimeout(ssDownscaleWidth, 10);
			});
		}
		var w = $(window).width();
		if(w<slideshowSlideWidth) {
			var margin = (w-slideshowSlideWidth)/2;
			var m = Math.max(margin,-200);
	    	$('.slideshow').css('marginLeft',m+'px');
    	}
    	else if(w>slideshowSlideWidth) {
    		$('.slideshow').css('marginLeft','auto');	
    	}
	};
	

	// Slideshow Animation
	function ssMoveToSlide(e,slidePos,cancelAutoPlay) {
		if(!cancelAutoPlay) {
			clearTimeout(ssAutoStartTimer);
		}
		if(typeof slidePos=='undefined') slidePos = $(this).index()-1;
		var delta = -slideshowSlideWidth*slidePos;
		$('.slides').animate({marginLeft:delta},slideshowSpeed);
		$('.ssc_dot').removeClass('ssc_on');
		$('.ssc_dot').eq(slidePos).addClass('ssc_on');
	}

	function ssMoveToPreviousSlide(event) {
		clearTimeout(ssAutoStartTimer);
		var prevPos = $('.ssc_on').index()-2;
		var pos = (prevPos>0)?prevPos:0;
		ssMoveToSlide(event,pos,false);
	}

	function ssMoveToNextSlide(event) {
		clearTimeout(ssAutoStartTimer);
		var nextPos = $('.ssc_on').index();
		var maxPos = $('.ssc_dot').size()-1;
		var pos = (nextPos<maxPos)?nextPos:maxPos;
		ssMoveToSlide(event,pos,false);
	}
	
	function ssMoveToNextSlideRepeat(event) {
		var maxPos = $('.ssc_dot').size();
		var pos = $('.ssc_on').index() % maxPos;
		ssMoveToSlide(event,pos,true);
	}

	function ssStart() {
		clearTimeout(ssAutoStartTimer);
		ssAutoStartTimer = setInterval(ssMoveToNextSlideRepeat, slideshowDelay);
		setTimeout(function() {clearInterval(ssAutoStartTimer)}, slideshowStopAfter);
	}

	// Recipe Search
	function getRecipeByKeywords(event) {
		var $kw = $(this).val().trim();
		window.instantSearchTimoutId = 
				setTimeout(function() {
					$("#main_content").load($("body").attr('lang')+'/recipes',{'kw':$kw});
				},100);
	}

	// Shopping Cart
	function checkCartForm() {
		$form = $("form[name='frm_cart']");
		if(checkValidity(document.frm_cart)) {
			return true;
		}
		else {
			$("input.invalid",$form).each(function(index,elt){
				$(elt).addClass("invalid-pattern");
				$(elt).removeClass("invalid-maxvalue");
				var $tooltip = $('<div class="cs_tooltip">'+$form.data('invalid-msg')+'</div>');
				$(elt).after($tooltip);
				$tooltip.fadeIn();
				setTimeout(function() {$tooltip.remove();$(elt).removeClass("invalid-pattern");},3000);
				//$(elt).attr("title",$form.data('invalid-msg'));
			});
			$("input.invalid-maxvalue",$form).each(function(index,elt){
				$(elt).addClass("invalid-maxvalue");
				$(elt).removeClass("invalid-pattern");
				var $tooltip = $('<div class="cs_tooltip">'+$form.data('invalid-maxvalue-msg')+' (max:&nbsp;'+ $(elt).attr('maxvalue') +')</div>').css("top",$(elt).outerHeight()+5+'px').css("left",-15+'px');
				$(elt).after($tooltip);
				$tooltip.fadeIn();
				setTimeout(function() {$tooltip.remove();$(elt).removeClass("invalid-maxvalue");},3000);
				//$(elt).attr("title",$form.data('invalid-maxvalue-msg'));
			});
			return false;
		}
	}

	function checkoutStep1(event) {
		if($(this).hasClass("btn_off")) {
			return false;
		}
		event.preventDefault();
		if(checkCartForm()) {
			$(".cs_sc_step1").slideUp();
			$(".cs_sc_step2").slideDown();
		}
	}

	function changeBillingCountry() {
		var countryCode = $(this).val();
		$(".cs_statesBlock").hide();
		$(".cs_statesBlock_" + countryCode).show();
		if(countryCode=="us") {
			$(".cs_sameBillShip").get(0).checked = false;
			$(".cs_sameBillShip").get(0).disabled = true;
			$(".cs_sameBillShipBlock").attr("title",$(".cs_sameBillShipBlock").attr("warningmsg"));
			$(".cs_diffBillShip").click();
			$('input[name="bill_postalcode"]').removeAttr("required");
			$('select[name="bill_province"]').removeAttr("required");
			$('select[name="bill_state"]').attr("required");
			$('input[name="bill_zip"]').attr("required");
		}
		else {
			$(".cs_sameBillShip").get(0).disabled = false;
			$(".cs_sameBillShipBlock").attr("title","");
			$('input[name="bill_postalcode"]').attr("required");
			$('select[name="bill_province"]').attr("required");
			$('select[name="bill_state"]').removeAttr("required");
			$('input[name="bill_zip"]').removeAttr("required");
		}
	}

	function copyBillingAddress(){
		$(".cs_shippingbilling_btn").removeClass("btn_off").attr("title","");
		if($(this).val()=="same"){
			$(".cs_shippingAddressForm").slideUp();
		}else{
			$(".cs_shippingAddressForm").slideDown();
			$(".cs_shippingAddressForm input, .cs_shippingAddressForm select").each(function(index,elt){
				$(elt).val("");
			});
		}
	}

	function calculateShipping(event) {
		event.preventDefault();
		if(!$(this).hasClass("btn_off")) {
			$form = $(this).closest("form");

			// Fill Shipping Address Form
			if($(".cs_sameBillShip",$form).get(0).checked){
				$(".cs_billingAddressForm input, .cs_billingAddressForm select",$form).each(function(index,elt){
					$billingName = $(elt).attr("name");
					$billingVal = $(elt).val();
					$shippingName = $billingName.replace(/bill_/,'ship_');
					$(".cs_shippingAddressForm input[name='"+$shippingName+"'],.cs_shippingAddressForm select[name='"+$shippingName+"']").val($billingVal);
				});
			}
			
			// Validation
			$(".cs_frmInvalidMsg").hide();
			
			$("input,select",$form).each(function(index,elt){
				$(elt).closest("div").removeClass("invalid");
			});
			
			if(!checkValidity($form.get(0))) {
				$(".cs_frmInvalidMsg").fadeIn();
				$("input.invalid,select.invalid",$form).each(function(index,elt){
					$(elt).closest("div").addClass("invalid");
				});
				return false;
			}
			
			// Get Shipping & Taxes
			if($('input[name="ship_postalcode"]').val() != "") {
				$(".cs_shippingbilling_btn").addClass("btn_off");
				$(".ajaxloader",$(this)).show();
				var postData = 'action=shippingfees&' + $form.serialize();
				$.ajax({url:$("body").attr('lang')+'/cart',type:'POST',dataType:'json',data:postData,success:handleCalculateShipping});
			}
		}
	}

	function handleCalculateShipping(data) {
		$(".ajaxloader").hide();
		if(data) {
			if(data['errorCode']=="1740") {
				if($(".cs_sameBillShip").get(0).checked){
					$("input[name='bill_postalcode']").closest("div").addClass("invalid");
				}
				else {
					$("input[name='ship_postalcode']").closest("div").addClass("invalid");	
				}
				$(".cs_updateAddressBtn").click();
				$(".cs_canadaPostProblemMsg").hide();
				$(".cs_frmInvalidMsg").fadeIn();
				//cartDisablePayment();
			}
			else if(data['error'] && !data['pcErrorFreeDelivery']) {
				$(".cs_updateAddressBtn").click();
				$(".cs_frmInvalidMsg").hide();
				$(".cs_canadaPostProblemMsg").fadeIn();
			}
			else {
				cartEnablePayment();
				// Show Popover Message
				$("html,body").animate({scrollTop:200},300);
				$(".cs_shippingAmount").html(data['shippingAmount']);
				$(".cs_taxesAmount").html(data['taxesAmount']);
				$(".cs_totalAmount").html(data['totalAmount']);
				var mode = $('.cs_ppPaymentForm').data('mode');
				if(mode=='dev') {
					$(".cs_payWithPaypal").before(data['ppEncryptedCart']);
				}
				else {
					$(".cs_ppwps_encryptedCart").val(data['ppEncryptedCart']);
				}
			}
		}
	}

	function cartDisablePayment(event) {
		event.preventDefault();
		$(".cs_payWithPaypal").popover('hide');
		formDisable(document.forms["frm_checkout"],false);
		$(".cs_tbc_label").html(labelTBC);
		//$(".cs_updateAddressBtnRow").hide();
		$(".cs_frmEnableMsg").fadeOut();
		$(".cs_payWithPaypal").addClass("btn_off");
		$(".cs_shippingbilling_btn").removeClass("btn_off");
		//$(".cs_checkoutFormZone").removeClass("mask");
	}

	function cartEnablePayment() {
		formDisable(document.forms["frm_checkout"],true);
		$(".cs_shippingbilling_btn").addClass("btn_off");
		$(".cs_payWithPaypal").removeClass("btn_off");
		//$(".cs_updateAddressBtnRow").show();
		$(".cs_frmEnableMsg").fadeIn();
		$(".cs_payWithPaypal").popover('show');
		//$(".cs_checkoutFormZone").addClass("mask");
	}

	function payWithPaypal(event) {
		if(!$(".cs_payWithPaypal").hasClass("btn_off")) {
			event.preventDefault();
			$(".ajaxloader",$(this)).show();
			$(this).addClass("btn_off");
			$(".cs_sameBillShip,.cs_diffBillShip").removeAttr("checked");
			var postData = 'action=prepare_order&gift_message=' + $("textarea[name='gift_message']").val();
			var $form = $(this).closest("form");
			$.ajax({url:$("body").attr('lang')+'/confirmation',type:'POST',dataType:'json',data:postData,success:function(data) {
																									if(data) {
																										$form.submit();
																									}
																								}
			});
		}
	}

	function formDisable(f,isDisable) {
		for(var i=0; i<f.elements.length; i++) {
			if(!$(f.elements[i]).data("donotdisable")) {
				f.elements[i].disabled = isDisable;
			}
		}
	}

	function openPopupWindow() {
		var w = 1024;
		var h = 600;
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	    var left = ((screen.width / 2) - (w / 2)) + dualScreenLeft;
	    var top = ((screen.height / 2) - (h / 2)) + dualScreenTop;
	    var newWindow = window.open($(this).data('popupurl'), 'printwindow', 'scrollbars=yes,resizable, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

	    // Puts focus on the newWindow
	    if (window.focus) {
	        newWindow.focus();
	    }
	}

	function subscribeNewsletter($f) {
		if(typeof $("input",$f).attr("disabled") == "undefined" || $("input",$f).attr("disabled") == false) {
			var postData = "e=" + $("input",$f).val();
			$(".ajaxloader",$f).show();
			$.ajax({url:'ajax/nlsubscribe.php',type:'POST',dataType:'json',data:postData,success:handleSubscribeNewsletter,error:handleSubscribeNewsletterError});
			$("input",$f).attr("disabled","disabled");
		}
	}

	function handleSubscribeNewsletter(data) {
		$(".ajaxloader",$(".cs_newsletter_form")).hide();
		// New API method
		/*
		if(data) {
			if(data.email) {
				var $tooltip = $('<div class="cs_nltooltip cs_success">'+data.email+' est maintenant abonné / '+data.email+' is now subscribed</div>');
				$(".cs_newsletter_btn").after($tooltip);
				$tooltip.fadeIn();
				setTimeout(function() {$tooltip.remove();$("input",$(".cs_newsletter_form")).removeAttr("disabled");},15000);
			}
		}
		*/
		// Old API method
		if(data) {
			if(data.result) {
				var $tooltip = $('<div class="cs_nltooltip cs_'+data.result+'">'+data.msg+'</div>');
				$(".cs_newsletter_btn").after($tooltip);
				$tooltip.fadeIn();
				setTimeout(function() {$tooltip.remove();$("input",$(".cs_newsletter_form")).removeAttr("disabled");},15000);
			}
		}
	}

	function handleSubscribeNewsletterError(xhr, status, error) {
		$(".ajaxloader",$(".cs_newsletter_form")).hide();
		console.log(error);
		console.log(status);
		if(error) {
			var $tooltip = $('<div class="cs_nltooltip cs_error">Service non-disponible : SVP réessayez plus tard<br>Service unavailable: Please try again later</div>');
			$(".cs_newsletter_btn").after($tooltip);
			$tooltip.fadeIn();
			setTimeout(function() {$tooltip.remove();/*$("input",$(".cs_newsletter_form")).removeAttr("disabled");*/},15000);
		}
	}
})();