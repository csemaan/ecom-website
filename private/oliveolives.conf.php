<?php 
define('DEV',false);

define('ROOT','');
define('DEFAULT_MODULE','home');
define('DEFAULT_ACTION','index');

/******************* DB *************************/
define('MY_HOST','xxxxxxxxxxxxxx-xxxxxxxxxxxxxx.xxxxxxxxxxxx.com');
define('MY_USER','xxxxxxxxxx');
define('MY_PASS','#xxxxxxxxxxxxxxx!'); 
define('MY_DBNAME','xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');

/**************** Google Drive ******************/
define('GD_COSTCO_TIMETABLE','http://docs.google.com/spreadsheet/pubxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
define('GD_OPENING_HOURS','http://docs.google.com/spreadsheet/pub?xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx=0&output=csv');


//define('IMAGE_UPLOAD_DIR','/var/www/vhosts/g8qd-lzlw.accessdomain.com/httpdocs/data/images/');
define('IMAGE_UPLOAD_DIR','/var/www/vhosts/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.com/httpdocs/data/images/');
define('BASE_URL','http://xxxxxxxxxxxxx.com/');
define('BASE_SURL','https://xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.com/');
//define('PRIVATE_PATH','/var/www/vhosts/g8qd-lzlw.accessdomain.com/private/');
define('PRIVATE_PATH','/var/www/vhosts/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.com/private/');

$availableLanguages = array('fr','en');	

$urlNodes = array(
			'en'			=> 'fr',
			'home'			=> 'accueil',
			'products'		=> 'produits',
			'product'		=> 'produit',
			'printproduct'	=> 'impproduit',
			'recipes'		=> 'recettes',
			'recipe'		=> 'recette',
			'printrecipe'	=> 'imprecette',
			'classes'		=> 'ateliers',
			'stores'		=> 'magasins', 
			'about'			=> 'a-propos',
			'news'			=> 'presse',
			'cart'			=> 'panier',
			'all'			=> 'tous',
			'policies'		=> 'politiques',
			'privacy'		=> 'confidentialite',
			'use'			=> 'utilisation',
			'cancellation'	=> 'annulation',
			'shipping'		=> 'livraison',
			'confirmation'	=> 'confirmation'	
	);

// Email settings
define("EMAIL_FROM","Olive & Olives <commande@xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.com>");
define("EMAIL_BCC","commande@xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.com");
define("EMAIL_RETURNPATH","commande@xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.com");
define("EMAIL_DEBUG","xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx@xvitae.com");

// Canada Post
define("CP_CERTAUTH_LIST",PRIVATE_PATH."canadapost/cacert.pem");
define("CP_ENDPOINT","https://soa-gw.canadapost.ca/rs/ship/price");
define("CP_API_CREDENTIALS","xxxxx:xxxxxxxxxxxxxxxxxxx");
define("CP_CUSTOMERNUMBER","xxxxxx");
define("CP_CONTRACTID","xxxxxx");
define("CP_ORIGIN_POSTALCODE","xxxxxx");

// Paypal Payment Standard
define('PP_PAYMENT_ENDPOINT',"https://www".(DEV?'.sandbox':'').".paypal.com/cgi-bin/webscr");
define("PP_IPN_URL",BASE_URL."fr/confirmation");
define('PP_ACCOUNT_ID','info'.(DEV?'_test':'').'@xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.com'); 
define('PP_RECEIVER_EMAIL','info'.(DEV?'_test':'').'@xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.com'); 
define('PP_PDT_IDENTITYTOKEN',(DEV?"xxxxxxxxxxxxxxxxxxxxxxxxxxxx-xxxxxxxxxxxxxxxxx":"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"));
define("PP_BN","xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_ShoppingCart_WPS_CA");
define("PP_RETURN_LABEL_FR","Retourner au site d'Olive &amp; Olives");
define("PP_RETURN_LABEL_EN","Back to Olive &amp; Olives' Web Site");
define("PP_CUSTOM_SIGNATURE","6e637a4a132ccae1ed7948458cf497a4d52d8681615fba368b0d7eae6ca587e9");

// Paypal Encrypted Payments
define('PPWPS_OO_PRIVATE_KEY',PRIVATE_PATH."paypal/oo_prvkey.pem");
define('PPWPS_OO_PUBLIC_KEY',PRIVATE_PATH."paypal/oo_pubkey.pem");
define('PPWPS_PP_PUBLIC_KEY',PRIVATE_PATH."paypal/paypal_pubkey.pem");
define('PPWPS_CERTID','xxxxxxxxxxxxxxxxx');

# Path to the openssl binary
define('OPENSSL',"/usr/bin/openssl");

// Tax Rates
$taxRates = array(
		"qc"	=>	14.975,
		"ab"	=>	5,
		"bc"	=>	5,
		"mb"	=>	5,
		"nb"	=>	15,
		"nl"	=>	15,
		"ns"	=>	15,
		"on"	=>	13,
		"pe"	=>	14,
		"sk"	=>	5,
		"nt"	=>	5,
		"nu"	=>	5,
		"yt"	=>	5
	);
?>