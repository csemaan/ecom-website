# Olive & Olives Website and CMS

Things to do on production server:

* Review .htaccess and correct any URL errors.
* Correct CONFIG_FILE_PATH in CMS and Website router code
* Correct ALL paths in the config file
* Configure Google Analytics
* Add Google Webmaster Tools
* Configure Sitemap and Ask Google to crawl site
* If time permits, cache slugs in file once a day (or even statically) instead of generating them on each request.